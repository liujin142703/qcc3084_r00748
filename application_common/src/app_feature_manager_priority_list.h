/*!
\copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      API for the feature manager priority list for the application.
*/

#ifndef APP_FEATURE_MANAGER_PRIORITY_LIST_H
#define APP_FEATURE_MANAGER_PRIORITY_LIST_H

#include "feature_manager.h"

const feature_manager_priority_lists_t * App_GetFeatureManagerPriorityLists(void);

#endif // APP_FEATURE_MANAGER_PRIORITY_LIST_H
