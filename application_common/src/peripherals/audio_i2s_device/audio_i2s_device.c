/*!
\copyright  Copyright (c) 2020 - 2023 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file       audio_i2s_device.c
\brief      Support for I2S audio output, implementing the 'Device' API defined
            in audio_i2s_common.h.
*/

#ifndef INCLUDE_SSM2518_I2S_AMP

#include <audio_i2s_common.h>
#include <pio_common.h>
#include <logging.h>
#include "adi1787.h"


/*! Default I2S configuration data for this chip */
const i2s_config_t device_i2s_config =
{
    .master_mode = 1,       /*!< Default to master mode for pure I2S output */
    .data_format = 0,       /*!< I2S left justified */
    .bit_delay = 1,         /*!< I2S delay 1 bit */
#ifdef ENABLE_I2S_OUTPUT_24BIT
    .bits_per_sample = 24,  /*!< 24-bit data words per channel */
#else
    .bits_per_sample = 16,  /*!< 16-bit data words per channel */
#endif
    .bclk_scaling = 64,     /*!< 32 bit clocks per data word per channel */
    .mclk_scaling = 0,      /*!< External master clock disabled */
    .enable_pio = PIN_INVALID
};


/*! \brief Power on and configure the I2S device. */
bool AudioI2SDeviceInitialise(uint32 sample_rate)
{
    // static uint32 cur_sample_rate = 48000;
    // uint8 spt0_master_value = 0x11;
    // DEBUG_LOG_INFO("AudioI2SDeviceInitialise, sample_rate:%d", sample_rate);
    // if (sample_rate != cur_sample_rate)
    // {
    //     if (sample_rate == 48000)
    //     {
    //         spt0_master_value = 0x11;
    //     }
    //     else if (sample_rate == 96000)
    //     {
    //         spt0_master_value = 0x22;
    //     }
    //     else if (sample_rate == 192000)
    //     {
    //         spt0_master_value = 0x33;
    //     }
    //     else if (sample_rate == 384000)
    //     {
    //         spt0_master_value = 0x64;
    //     }else
    //     {
    //         DEBUG_LOG_ERROR("AudioI2SDeviceInitialise: set sample_rate error!:%d", sample_rate);
    //     }
    //     cur_sample_rate = sample_rate;
    //     i2c_reg_write(i2c_addr_1787_8bit, ADI1787_SPT0_CTRL2, 1, &spt0_master_value);
    // }
    UNUSED(sample_rate);
    if(anc_state != state_anc_off)
    {
        uint8 value = 0x00;
        /*!< V2.5版本，在这里关闭一下Fint规避ANC时钟内部同步后I2S带来的pop */
        i2c_reg_write(i2c_addr_1787_8bit, 0xC00A, 1, &value);
        MessageCancelAll(AncGetTask(), EventMyAdiSpt0SetUp);
        MessageSendLater(AncGetTask(), EventMyAdiSpt0SetUp, 0, 300);
    }
    else
    {
        MessageSendLater(AncGetTask(), EventMyAdiSpt0SetUp, 0, 0);
    }
    DEBUG_LOG_INFO("AudioI2SDeviceInitialise!");
    return TRUE;
}

/*! \brief Shut down and power off the I2S device. */
bool AudioI2SDeviceShutdown(void)
{
    uint8 value = 0x00;
    /*!< V2.5版本，在这里关闭Fint*/
    i2c_reg_write(i2c_addr_1787_8bit, 0xC00A, 1, &value);
    DEBUG_LOG_INFO("AudioI2SDeviceShutdown!");
    return TRUE;
}

/*! \brief Set the initial hardware gain of the I2S device, per channel. */
bool AudioI2SDeviceSetChannelGain(i2s_out_t channel, int16 gain)
{
    UNUSED(channel);
    UNUSED(gain);
    return TRUE;
}

/*! \brief Set the initial hardware gain of all I2S channels to the same level. */
bool AudioI2SDeviceSetGain(int16 gain)
{
    UNUSED(gain);
    return TRUE;
}

/*! \brief Get the overall delay of the I2S hardware, for TTP adjustment. */
uint16 AudioI2SDeviceGetDelay(void)
{
    return 0;
}

#endif /* INCLUDE_SSM2518_I2S_AMP */
