/*!
\copyright  Copyright (c) 2020 - 2023 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file       app_setup_unexpected_message.h
\brief      Unexpected message handlers.
 
*/

#ifndef APP_SETUP_UNEXPECTED_MESSAGE_H_
#define APP_SETUP_UNEXPECTED_MESSAGE_H_


/*@{*/

/*! \brief Installs unexpected message handlers.
*/
void App_SetupUnexpectedMessage(void);

/*@}*/

#endif /* APP_SETUP_UNEXPECTED_MESSAGE_H_ */
