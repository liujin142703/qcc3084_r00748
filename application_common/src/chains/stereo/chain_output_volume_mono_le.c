/*!
    \copyright Copyright (c) 2024 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_output_volume_mono_le.c
    \brief The chain_output_volume_mono_le chain.

    This file is generated by F:\qcc3084\3084_F05_r00784\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#include <chain_output_volume_mono_le.h>
#include <cap_id_prim.h>
#include <opmsg_prim.h>
#include <hydra_macros.h>
#include <app_chain_config.h>
#include <kymera_chain_roles.h>
static const operator_config_t operators[] =
{
    MAKE_OPERATOR_CONFIG_PRIORITY_MEDIUM(CAP_ID_OUTPUT_VOL_CTRL, OPR_VOLUME_CONTROL),
} ;

static const operator_endpoint_t inputs[] =
{
    {OPR_VOLUME_CONTROL, EPR_SINK_MIXER_MAIN_IN, 0},
    {OPR_VOLUME_CONTROL, EPR_VOLUME_AUX, 1},
} ;

static const operator_endpoint_t outputs[] =
{
    {OPR_VOLUME_CONTROL, EPR_SOURCE_MIXER_OUT, 0},
} ;

const chain_config_t chain_output_volume_mono_le_config = {0, 0, operators, 1, inputs, 2, outputs, 1, NULL, 0};

