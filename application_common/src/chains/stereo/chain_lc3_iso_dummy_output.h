/*!
    \copyright Copyright (c) 2024 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_lc3_iso_dummy_output.h
    \brief The chain_lc3_iso_dummy_output chain.

    This file is generated by F:\qcc3084\3084_F05_r00784\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#ifndef _CHAIN_LC3_ISO_DUMMY_OUTPUT_H__
#define _CHAIN_LC3_ISO_DUMMY_OUTPUT_H__

/*!
\page chain_lc3_iso_dummy_output
    @startuml
        object OPR_SREC_OUTPUT_BASIC_PASS
        OPR_SREC_OUTPUT_BASIC_PASS : id = CAP_ID_BASIC_PASS
        object EPR_SOURCE_ENCODE_OUT #lightblue
        EPR_SOURCE_ENCODE_OUT <-- "OUT1(0)" OPR_SREC_OUTPUT_BASIC_PASS
    @enduml
*/

#include <chain.h>

extern const chain_config_t chain_lc3_iso_dummy_output_config;

#endif /* _CHAIN_LC3_ISO_DUMMY_OUTPUT_H__ */

