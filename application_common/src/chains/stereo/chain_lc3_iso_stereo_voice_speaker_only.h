/*!
    \copyright Copyright (c) 2024 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_lc3_iso_stereo_voice_speaker_only.h
    \brief The chain_lc3_iso_stereo_voice_speaker_only chain.

    This file is generated by F:\qcc3084\3084_F05_r00784\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#ifndef _CHAIN_LC3_ISO_STEREO_VOICE_SPEAKER_ONLY_H__
#define _CHAIN_LC3_ISO_STEREO_VOICE_SPEAKER_ONLY_H__

/*!
\page chain_lc3_iso_stereo_voice_speaker_only
    @startuml
        object OPR_LC3_DECODE_SCO_ISO
        OPR_LC3_DECODE_SCO_ISO : id = CAP_ID_LC3_SCO_ISO_DEC
        object OPR_LC3_DECODE_SCO_ISO_RIGHT
        OPR_LC3_DECODE_SCO_ISO_RIGHT : id = CAP_ID_LC3_SCO_ISO_DEC
        object EPR_ISO_FROM_AIR_LEFT #lightgreen
        OPR_LC3_DECODE_SCO_ISO "IN(0)" <-- EPR_ISO_FROM_AIR_LEFT
        object EPR_ISO_FROM_AIR_RIGHT #lightgreen
        OPR_LC3_DECODE_SCO_ISO_RIGHT "IN(0)" <-- EPR_ISO_FROM_AIR_RIGHT
        object EPR_SCO_SPEAKER #lightblue
        EPR_SCO_SPEAKER <-- "OUT_L(0)" OPR_LC3_DECODE_SCO_ISO
        object EPR_SCO_SPEAKER_RIGHT #lightblue
        EPR_SCO_SPEAKER_RIGHT <-- "OUT_R(0)" OPR_LC3_DECODE_SCO_ISO_RIGHT
    @enduml
*/

#include <chain.h>

extern const chain_config_t chain_lc3_iso_stereo_voice_speaker_only_config;

#endif /* _CHAIN_LC3_ISO_STEREO_VOICE_SPEAKER_ONLY_H__ */

