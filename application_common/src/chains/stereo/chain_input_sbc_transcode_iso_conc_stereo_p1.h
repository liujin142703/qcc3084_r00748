/*!
    \copyright Copyright (c) 2024 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_input_sbc_transcode_iso_conc_stereo_p1.h
    \brief The chain_input_sbc_transcode_iso_conc_stereo_p1 chain.

    This file is generated by F:\qcc3084\3084_F05_r00784\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#ifndef _CHAIN_INPUT_SBC_TRANSCODE_ISO_CONC_STEREO_P1_H__
#define _CHAIN_INPUT_SBC_TRANSCODE_ISO_CONC_STEREO_P1_H__

/*!
\page chain_input_sbc_transcode_iso_conc_stereo_p1
    @startuml
        object OPR_RTP_DECODER
        OPR_RTP_DECODER : id = CAP_ID_RTP_DECODE
        object OPR_SBC_DECODER
        OPR_SBC_DECODER : id = CAP_ID_SBC_DECODER
        object OPR_SPEAKER_RESAMPLER
        OPR_SPEAKER_RESAMPLER : id = CAP_ID_IIR_RESAMPLER
        object OPR_LEA_SPLT_ISO_TX
        OPR_LEA_SPLT_ISO_TX : id = CAP_ID_SPLITTER
        object OPR_LC3_ENCODE_SCO_ISO
        OPR_LC3_ENCODE_SCO_ISO : id = CAP_ID_LC3_SCO_ISO_ENC
        object OPR_LC3_ENCODE_SCO_ISO_RIGHT
        OPR_LC3_ENCODE_SCO_ISO_RIGHT : id = CAP_ID_LC3_SCO_ISO_ENC
        object OPR_LC3_DECODE_SCO_ISO
        OPR_LC3_DECODE_SCO_ISO : id = CAP_ID_LC3_SCO_ISO_DEC
        object OPR_LC3_DECODE_SCO_ISO_RIGHT
        OPR_LC3_DECODE_SCO_ISO_RIGHT : id = CAP_ID_LC3_SCO_ISO_DEC
        OPR_SBC_DECODER "IN(0)"<-- "OUT(0)" OPR_RTP_DECODER
        OPR_SPEAKER_RESAMPLER "IN_0(0)"<-- "OUT_L(0)" OPR_SBC_DECODER
        OPR_SPEAKER_RESAMPLER "IN_1(1)"<-- "OUT_R(1)" OPR_SBC_DECODER
        OPR_LEA_SPLT_ISO_TX "IN_0(0)"<-- "OUT_0(0)" OPR_SPEAKER_RESAMPLER
        OPR_LEA_SPLT_ISO_TX "IN_1(1)"<-- "OUT_1(1)" OPR_SPEAKER_RESAMPLER
        OPR_LC3_ENCODE_SCO_ISO "IN_L(0)"<-- "OUT_0(0)" OPR_LEA_SPLT_ISO_TX
        OPR_LC3_ENCODE_SCO_ISO_RIGHT "IN_R(0)"<-- "OUT_2(2)" OPR_LEA_SPLT_ISO_TX
        OPR_LC3_DECODE_SCO_ISO "IN_L(0)"<-- "ISO_OUT_L(0)" OPR_LC3_ENCODE_SCO_ISO
        OPR_LC3_DECODE_SCO_ISO_RIGHT "IN_R(0)"<-- "ISO_OUT_R(0)" OPR_LC3_ENCODE_SCO_ISO_RIGHT
        object EPR_SINK_MEDIA #lightgreen
        OPR_RTP_DECODER "IN(0)" <-- EPR_SINK_MEDIA
        object EPR_LEA_CONC_SPLT_ISO_LEFT #lightblue
        EPR_LEA_CONC_SPLT_ISO_LEFT <-- "OUT_1(1)" OPR_LEA_SPLT_ISO_TX
        object EPR_LEA_CONC_SPLT_ISO_RIGHT #lightblue
        EPR_LEA_CONC_SPLT_ISO_RIGHT <-- "OUT_3(3)" OPR_LEA_SPLT_ISO_TX
        object EPR_SOURCE_DECODED_PCM #lightblue
        EPR_SOURCE_DECODED_PCM <-- "OUT_L(0)" OPR_LC3_DECODE_SCO_ISO
        object EPR_SOURCE_DECODED_PCM_RIGHT #lightblue
        EPR_SOURCE_DECODED_PCM_RIGHT <-- "OUT_R(0)" OPR_LC3_DECODE_SCO_ISO_RIGHT
    @enduml
*/

#include <chain.h>

extern const chain_config_t chain_input_sbc_transcode_iso_conc_stereo_p1_config_p0;

extern const chain_config_t chain_input_sbc_transcode_iso_conc_stereo_p1_config_p1;

#endif /* _CHAIN_INPUT_SBC_TRANSCODE_ISO_CONC_STEREO_P1_H__ */

