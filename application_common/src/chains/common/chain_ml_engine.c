/*!
    \copyright Copyright (c) 2024 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_ml_engine.c
    \brief The chain_ml_engine chain.

    This file is generated by F:\qcc3084\3084_F05_r00784\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#include <chain_ml_engine.h>
#include <cap_id_prim.h>
#include <opmsg_prim.h>
#include <hydra_macros.h>
#include <app_chain_config.h>
#include <kymera_chain_roles.h>
static const operator_config_t operators_p1[] =
{
    MAKE_OPERATOR_CONFIG_P1(CAP_ID_DOWNLOAD_ML_ENGINE, OPR_ML_ENGINE),
} ;

const chain_config_t chain_ml_engine_config_p1 = {0, 0, operators_p1, 1, NULL, 0, NULL, 0, NULL, 0};

