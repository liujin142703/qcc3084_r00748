/*!
    \copyright Copyright (c) 2024 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_ml_engine.h
    \brief The chain_ml_engine chain.

    This file is generated by F:\qcc3084\3084_F05_r00784\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#ifndef _CHAIN_ML_ENGINE_H__
#define _CHAIN_ML_ENGINE_H__

/*!
\page chain_ml_engine
    @startuml
        object OPR_ML_ENGINE
        OPR_ML_ENGINE : id = CAP_ID_DOWNLOAD_ML_ENGINE
    @enduml
*/

#include <chain.h>

extern const chain_config_t chain_ml_engine_config_p1;

#endif /* _CHAIN_ML_ENGINE_H__ */

