/*!
\copyright  Copyright (c) 2022 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\ingroup    charger_case
\brief      Unexpected message handlers.
 
*/

#ifndef CHARGER_CASE_SETUP_UNEXPECTED_MESSAGE_H_
#define CHARGER_CASE_SETUP_UNEXPECTED_MESSAGE_H_


/*@{*/

/*! \brief Installs unexpected message handlers.
*/
void ChargerCase_SetupUnexpectedMessage(void);

/*@}*/

#endif /* EARBUD_SETUP_UNEXPECTED_MESSAGE_H_ */
