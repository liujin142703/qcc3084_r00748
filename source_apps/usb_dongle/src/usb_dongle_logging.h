/*!
\copyright  Copyright (c) 2021 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Logging group for application.
*/

#ifndef USB_DONGLE_LOGGING_H
#define USB_DONGLE_LOGGING_H

#define DEBUG_LOG_MODULE_NAME application
#include <logging.h>

#endif // USB_DONGLE_LOGGING_H
