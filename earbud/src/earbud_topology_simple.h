#ifndef EARBUD_TOPOLOGY_SIMPLE_H
#define EARBUD_TOPOLOGY_SIMPLE_H

#include "tws_topology.h"

const tws_topology_product_behaviour_t* EarbudTopologySimple_GetConfig(void);

#endif // EARBUD_TOPOLOGY_SIMPLE_H
