/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup @@@cap_name^U@@@
 * \ingroup capabilities
 * \file  @@@cap_name@@@.h
 * \ingroup @@@cap_name^U@@@
 *
 * ML_base template public header file. 
 *
 */

#ifndef _@@@cap_name^U@@@_H
#define _@@@cap_name^U@@@_H

#include "capabilities.h"

/****************************************************************************
Public Variable Definitions
*/

/** The capability data structure for @@@cap_name@@@ */
#ifdef INSTALL_OPERATOR_@@@cap_name^U@@@
extern const CAPABILITY_DATA @@@cap_name@@@_cap_data;
#endif



#endif /* _@@@cap_name^U@@@_H */
