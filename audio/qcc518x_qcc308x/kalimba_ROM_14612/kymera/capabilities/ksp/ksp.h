/****************************************************************************
 * Copyright (c) 2014 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file  ksp.h
 * \ingroup capabilities
 *
 * KSP Capability public header file. <br>
 *
 */

#ifndef KSP_H
#define KSP_H


/** The capability data structure for KSP capability */
extern const CAPABILITY_DATA ksp_cap_data;

#endif /* KSP_H */
