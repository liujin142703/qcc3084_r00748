/****************************************************************************
 * Copyright (c) 2014 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file  swbs.c
 * \ingroup  operators
 *
 *  SWBS_ENC/SWBS_DEC operator common code
 *
 */

/****************************************************************************
Include Files
*/
#include "swbs_private.h"

/***************************** SWBS shared tables ******************************/
/** memory shared between enc and dec */
/// Currently aptX doesn't use shared memory or scratch

