/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file cap_download_mgr_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef CAP_DOWNLOAD_MGR_SCHED_H
#define CAP_DOWNLOAD_MGR_SCHED_H

extern void cap_download_mgr_task_init(void **);
extern void cap_download_mgr_task_handler(void **);
#endif
