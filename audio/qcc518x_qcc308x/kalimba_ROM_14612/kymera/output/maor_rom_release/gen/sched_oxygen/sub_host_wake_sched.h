/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file sub_host_wake_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef SUB_HOST_WAKE_SCHED_H
#define SUB_HOST_WAKE_SCHED_H

extern void sub_host_wake_bg_int_handler(void **);
#endif
