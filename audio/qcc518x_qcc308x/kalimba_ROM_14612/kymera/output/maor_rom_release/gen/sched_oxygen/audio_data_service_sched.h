/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file audio_data_service_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef AUDIO_DATA_SERVICE_SCHED_H
#define AUDIO_DATA_SERVICE_SCHED_H

extern void audio_data_service_task_init(void **);
extern void audio_data_service_task_handler(void **);
#endif
