/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file ps_sar_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef PS_SAR_SCHED_H
#define PS_SAR_SCHED_H

extern void ps_sar_task_init(void **);
extern void ps_sar_handler(void **);
#endif
