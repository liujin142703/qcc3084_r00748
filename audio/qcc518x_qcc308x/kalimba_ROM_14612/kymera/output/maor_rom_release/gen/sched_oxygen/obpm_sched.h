/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file obpm_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef OBPM_SCHED_H
#define OBPM_SCHED_H

extern void obpm_adaptor_init(void **);
extern void obpm_adaptor_msg_handler(void **);
#endif
