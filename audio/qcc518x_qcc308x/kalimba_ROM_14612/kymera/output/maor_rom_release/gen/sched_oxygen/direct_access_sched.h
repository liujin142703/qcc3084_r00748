/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file direct_access_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef DIRECT_ACCESS_SCHED_H
#define DIRECT_ACCESS_SCHED_H

extern void direct_access_task_init(void **);
extern void direct_access_handler(void **);
#endif
