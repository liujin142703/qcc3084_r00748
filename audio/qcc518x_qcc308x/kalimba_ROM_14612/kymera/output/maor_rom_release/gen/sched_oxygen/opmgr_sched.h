/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file opmgr_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef OPMGR_SCHED_H
#define OPMGR_SCHED_H

extern void opmgr_task_init(void **);
extern void opmgr_task_handler(void **);
#endif
