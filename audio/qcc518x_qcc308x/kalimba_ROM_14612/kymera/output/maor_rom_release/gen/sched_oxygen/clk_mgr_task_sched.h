/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file clk_mgr_task_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef CLK_MGR_TASK_SCHED_H
#define CLK_MGR_TASK_SCHED_H

extern void clk_mgr_task_init(void **);
extern void clk_mgr_task_msg_handler(void **);
#endif
