/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file aov_task_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef AOV_TASK_SCHED_H
#define AOV_TASK_SCHED_H

extern void aov_task_init(void **);
extern void aov_task_msg_handler(void **);
#endif
