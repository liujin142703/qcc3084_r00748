/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file subwd_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef SUBWD_SCHED_H
#define SUBWD_SCHED_H

extern void subwd_bg_int_handler(void **);
#endif
