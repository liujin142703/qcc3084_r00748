/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file ps_router_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef PS_ROUTER_SCHED_H
#define PS_ROUTER_SCHED_H

extern void ps_router_task_init(void **);
extern void ps_router_handler(void **);
#endif
