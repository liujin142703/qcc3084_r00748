#ifndef __MLFE100_INTERNAL__
#define __MLFE100_INTERNAL__
/*============================================================================
@file mlfe100_internal.h

header defining ML constants.

Copyright (c) 2023 QUALCOMM Technologies Incorporated.
All Rights Reserved.
Qualcomm Confidential and Proprietary
============================================================================*/
/* These defines are set based on the intended use case and model.
   For the hybrid cvc using the matrix ns model, we expect the following:*/
#define MAX_MLFE_INPUT_COUNT 1
#define MAX_MLFE_OUTPUT_COUNT 1
#define MAX_MODEL_INPUT_COUNT 8
#define MAX_MODEL_OUTPUT_COUNT 2

#endif//#define __MLFE100_INTERNAL__



