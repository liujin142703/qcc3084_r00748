/**************************************************************************/
/* Copyright (c) 2023 Qualcomm Technologies International, Ltd.           */
/* Autogenerated File, Do Not Edit                                        */
/*                                                                        */
/* This file exports public entry points from patch code                  */
/*                                                                        */
/**************************************************************************/
#ifndef SUBSYS3_PATCH0_FW00003914_MAP_PUBLIC_H
#define SUBSYS3_PATCH0_FW00003914_MAP_PUBLIC_H

#define PATCH_BUILD_ID                           32094

#define ENTRY_POINT_DNLD_ML_ENGINE_LIB_GET_FUNCTION_TABLE 0x0400fcbb /* $_dnld_ml_engine_lib_get_fn_table */
#define ENTRY_POINT_VAGM_MANAGE_CLK              0x040102d5 /* $_vagm_manage_clk */
#define ENTRY_POINT_VOL_CTRL                     0x040104b1 /* $_vol_ctrl_low_event_get */

#endif /* SUBSYS3_PATCH0_FW00003914_MAP_PUBLIC_H */
