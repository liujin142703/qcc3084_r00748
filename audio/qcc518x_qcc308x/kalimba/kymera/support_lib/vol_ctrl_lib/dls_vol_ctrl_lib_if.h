/****************************************************************************
 * Copyright (c) 2022 Qualcomm Technologies International, Ltd
****************************************************************************/
/**
 * \file  dls_vagm_clk_lib_if.h
 * \ingroup support_lib/vagm_clk_lib
 *
 * VA Graph manager support lib interface file. <br>
 *
 */
#ifndef _DLS_VOL_CTRL_LIB_IF_H
#define _DLS_VOL_CTRL_LIB_IF_H

/*****************************************************************************
Include Files
*/
#include "types.h"

/*****************************************************************************
Public functions
*/
extern unsigned vol_ctrl_low_event_get(void);
#endif