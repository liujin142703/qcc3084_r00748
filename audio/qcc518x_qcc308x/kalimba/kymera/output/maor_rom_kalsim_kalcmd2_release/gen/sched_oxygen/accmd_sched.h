/****************************************************************************
 * Copyright (c) 2023 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file accmd_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef ACCMD_SCHED_H
#define ACCMD_SCHED_H

extern void accmd_task_init(void **);
extern void accmd_task(void **);
#endif
