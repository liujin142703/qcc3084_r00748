/****************************************************************************
 * Copyright (c) 2023 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file hydra_sssm_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef HYDRA_SSSM_SCHED_H
#define HYDRA_SSSM_SCHED_H

extern void sssm_audio_task_init(void **);
extern void sssm_audio_task_msg_handler(void **);
#endif
