// frmbuffer.struct 
.CONST 	$cvc_stream.frmbuffer_struct.FRAME_PTR_FIELD	0;
.CONST 	$cvc_stream.frmbuffer_struct.FRAME_SIZE_FIELD	4;
.CONST 	$cvc_stream.frmbuffer_struct.BUFFER_SIZE_FIELD	8;
.CONST 	$cvc_stream.frmbuffer_struct.BUFFER_ADDR_FIELD	12;
.CONST 	$cvc_stream.frmbuffer_struct.PEAK_VALUE_FIELD	16;
.CONST	$cvc_stream.frmbuffer_struct.STRUC_SIZE 	5;
