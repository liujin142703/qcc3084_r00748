// -----------------------------------------------------------------------------
// Copyright (c) 2023                  Qualcomm Technologies International, Ltd.
//
// Generated by /home/svc-audio-dspsw/kymera_builds/builds/2023/kymera_2312060823/kalimba/kymera/../../util/CommonParameters/DerivationEngine.py
// code v3.5, file //depot/dspsw/maor_rom_v20/util/CommonParameters/DerivationEngine.py, revision 2
// namespace {com.csr.cps.4}UnifiedParameterSchema on 2023-12-06 08:59:15 by svc-audio-dspsw
//
// input from EMPTY
// last change  by  on 
// -----------------------------------------------------------------------------
#ifndef __AAH_GEN_C_H__
#define __AAH_GEN_C_H__

#ifndef ParamType
typedef unsigned ParamType;
#endif

// CodeBase IDs
#define AAH_AAH_CAP_ID	0x00EA
#define AAH_AAH_ALT_CAP_ID_0	0x40D0
#define AAH_AAH_SAMPLE_RATE	16000
#define AAH_AAH_VERSION_MAJOR	1

// Constant Definitions


// Runtime Config Parameter Bitfields
#define AAH_CONFIG_ENABLE		0x00000001


// System Mode
#define AAH_SYSMODE_STANDBY		0
#define AAH_SYSMODE_FULL   		1
#define AAH_SYSMODE_MAX_MODES		2

// Statistics Block
typedef struct _tag_AAH_STATISTICS
{
	ParamType OFFSET_CUR_MODE;
	ParamType OFFSET_EC_DELTA_GAIN;
	ParamType OFFSET_FB_DELTA_GAIN;
	ParamType OFFSET_FF_DELTA_GAIN;
	ParamType OFFSET_FLAGS;
	ParamType OFFSET_ACTUAL_DELAY;
	ParamType OFFSET_FRAMES;
	ParamType OFFSET_WATERMARK_IN_BUF;
	ParamType OFFSET_SYNC_LOSS;
}AAH_STATISTICS;

typedef AAH_STATISTICS* LP_AAH_STATISTICS;

// Parameters Block
typedef struct _tag_AAH_PARAMETERS
{
	ParamType OFFSET_AHM_PRIORITY;
	ParamType OFFSET_AHM_ATTACK_TIME;
	ParamType OFFSET_AHM_RELEASE_TIME;
	ParamType OFFSET_AAH_CONFIG;
	ParamType OFFSET_GAIN_SCALE_RX_DB;
	ParamType OFFSET_GAIN_SCALE_FB_MIC_DB;
	ParamType OFFSET_GAIN_SCALE_FF_MIC_DB;
	ParamType OFFSET_GAIN_SCALE_RX_MIX_DB;
	ParamType OFFSET_LIMIT_LEVEL_FB_DB;
	ParamType OFFSET_LIMIT_LEVEL_FF_DB;
	ParamType OFFSET_LIMIT_LEVEL_COMB_DB;
	ParamType OFFSET_AAH_TC_ATTACK;
	ParamType OFFSET_AAH_TC_RELEASE;
	ParamType OFFSET_AAH_TC_HOLD;
	ParamType OFFSET_PXIIR_EC_COEF_0;
	ParamType OFFSET_PXIIR_EC_COEF_1;
	ParamType OFFSET_PXIIR_EC_COEF_2;
	ParamType OFFSET_PXIIR_FB_COEF_0;
	ParamType OFFSET_PXIIR_FB_COEF_1;
	ParamType OFFSET_PXIIR_FB_COEF_2;
	ParamType OFFSET_PXIIR_FF_COEF_0;
	ParamType OFFSET_PXIIR_FF_COEF_1;
	ParamType OFFSET_PXIIR_FF_COEF_2;
	ParamType OFFSET_RX_DELAY;
}AAH_PARAMETERS;

typedef AAH_PARAMETERS* LP_AAH_PARAMETERS;

unsigned *AAH_GetDefaults(unsigned capid);

#endif // __AAH_GEN_C_H__
