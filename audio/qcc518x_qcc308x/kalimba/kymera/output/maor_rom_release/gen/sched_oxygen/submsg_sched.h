/****************************************************************************
 * Copyright (c) 2023 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file submsg_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef SUBMSG_SCHED_H
#define SUBMSG_SCHED_H

extern void submsg_tx_bg(void **);
extern void submsg_rx_bg(void **);
#endif
