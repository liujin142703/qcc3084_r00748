/****************************************************************************
 * Copyright (c) 2023 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file ipc_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef IPC_SCHED_H
#define IPC_SCHED_H

extern void ipc_msg_task_init(void **);
extern void ipc_msg_handler(void **);
#endif
