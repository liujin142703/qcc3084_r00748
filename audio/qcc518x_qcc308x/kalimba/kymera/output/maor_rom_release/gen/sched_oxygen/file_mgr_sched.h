/****************************************************************************
 * Copyright (c) 2023 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file file_mgr_sched.h
 * \ingroup sched_oxygen
 *
 * Declaration of a component's functions used by the scheduler.
 */

#ifndef FILE_MGR_SCHED_H
#define FILE_MGR_SCHED_H

extern void file_mgr_task_init(void **);
extern void file_mgr_task_handler(void **);
#endif
