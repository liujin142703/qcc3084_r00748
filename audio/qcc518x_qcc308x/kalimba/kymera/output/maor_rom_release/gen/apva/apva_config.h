// -----------------------------------------------------------------------------
// Copyright (c) 2023                  Qualcomm Technologies International, Ltd.
//
// Generated by /home/svc-audio-dspsw/kymera_builds/builds/2023/kymera_2312060823/kalimba/kymera/../../util/CommonParameters/DerivationEngine.py
// code v3.5, file //depot/dspsw/maor_rom_v20/util/CommonParameters/DerivationEngine.py, revision 2
// namespace {com.csr.cps.4}UnifiedParameterSchema on 2023-12-06 08:42:23 by svc-audio-dspsw
//
// input from //depot/dspsw/crescendo_dev/kalimba/kymera/capabilities/qva/qva.xml#3
// last change 2721140 by ra22 on 2017/01/31 13:34:30
// -----------------------------------------------------------------------------
#ifndef __APVA_CONFIG_H__
#define __APVA_CONFIG_H__


#endif // __APVA_CONFIG_H__
