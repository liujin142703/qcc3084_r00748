// MLFE100_MASKING_PARAM.struct 
.CONST 	$cvc_noise_sup_prepost.MLFE100_MASKING_PARAM_struct.L2_MLFE_GAIN_FIELD	0;
.CONST 	$cvc_noise_sup_prepost.MLFE100_MASKING_PARAM_struct.MLFE_THRESH_FIELD	4;
.CONST 	$cvc_noise_sup_prepost.MLFE100_MASKING_PARAM_struct.MLFE_AGGR_FIELD	8;
.CONST 	$cvc_noise_sup_prepost.MLFE100_MASKING_PARAM_struct.MLFE_SWB_ATTEN_FIELD	12;
.CONST	$cvc_noise_sup_prepost.MLFE100_MASKING_PARAM_struct.STRUC_SIZE 	4;
// model_5_pre_proc_data.struct 
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.SWB_DELAY_STRUCT_FIELD	0;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.SWB_SCRATCH_FIELD	4;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.DELAY_STRUCT_FIELD	8;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.SCRATCH_TENSOR_FIELD	12;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.MULAW_CMPD100_DATA_FIELD	16;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.IN_TENSOR_CNT_FIELD	20;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.IN_TENSOR_SIZE_FIELD	24;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.IN_TENSOR_ADDR_FIELD	56;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.IN_TENSOR_SCALE_FIELD	88;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.INPUT_NUM_BINS_FIELD	120;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.INPUT_CNT_FIELD	124;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.INPUT_ADDR_FIELD	128;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.PARAM_PTR_FIELD	132;
.CONST 	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.BYPASS_FLAG_PTR_FIELD	136;
.CONST	$cvc_noise_sup_prepost.model_5_pre_proc_data_struct.STRUC_SIZE 	35;
// model_5_post_proc_data_t.struct 
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.MULAW_EXPD100_DATA_FIELD	0;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.OUT_TENSOR_CNT_FIELD	4;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.OUT_TENSOR_SIZE_FIELD	8;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.OUT_TENSOR_ADDR_FIELD	16;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.OUT_TENSOR_SCALE_FIELD	24;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.OUTPUT_NUM_BINS_FIELD	32;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.OUTPUT_CNT_FIELD	36;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.OUTPUT_ADDR_FIELD	40;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.PARAM_PTR_FIELD	44;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.PRE_PROC_DATA_FIELD	48;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.RENORM_HEADROOM_FIELD	52;
.CONST 	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.BYPASS_FLAG_PTR_FIELD	56;
.CONST	$cvc_noise_sup_prepost.model_5_post_proc_data_t_struct.STRUC_SIZE 	15;
// model_5_data_t.struct 
.CONST 	$cvc_noise_sup_prepost.model_5_data_t_struct.IN_TENSOR_SIZE_FIELD	0;
.CONST 	$cvc_noise_sup_prepost.model_5_data_t_struct.IN_TENSOR_ADDR_FIELD	32;
.CONST 	$cvc_noise_sup_prepost.model_5_data_t_struct.IN_TENSOR_CNT_FIELD	64;
.CONST 	$cvc_noise_sup_prepost.model_5_data_t_struct.OUT_TENSOR_SIZE_FIELD	68;
.CONST 	$cvc_noise_sup_prepost.model_5_data_t_struct.OUT_TENSOR_ADDR_FIELD	76;
.CONST 	$cvc_noise_sup_prepost.model_5_data_t_struct.OUT_TENSOR_CNT_FIELD	84;
.CONST	$cvc_noise_sup_prepost.model_5_data_t_struct.STRUC_SIZE 	22;
