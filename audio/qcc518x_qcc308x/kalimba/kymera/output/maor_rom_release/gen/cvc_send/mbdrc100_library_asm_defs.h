// mbdrc100_in_buffer_t.struct 
.CONST 	$mbdrc100_library.mbdrc100_in_buffer_t_struct.CVC_FREQ_BUFFER_FIELD	0;
.CONST 	$mbdrc100_library.mbdrc100_in_buffer_t_struct.NUM_BINS_FIELD	4;
.CONST	$mbdrc100_library.mbdrc100_in_buffer_t_struct.STRUC_SIZE 	2;
// mbdrc_lib_t.struct 
.CONST 	$mbdrc100_library.mbdrc_lib_t_struct.LIB_MEM_PTR_FIELD	0;
.CONST	$mbdrc100_library.mbdrc_lib_t_struct.STRUC_SIZE 	1;
// mbdrc_lib_mem_requirements_t.struct 
.CONST 	$mbdrc100_library.mbdrc_lib_mem_requirements_t_struct.LIB_MEMORY_SIZE_FIELD	0;
.CONST 	$mbdrc100_library.mbdrc_lib_mem_requirements_t_struct.LIB_STACK_SIZE_FIELD	4;
.CONST	$mbdrc100_library.mbdrc_lib_mem_requirements_t_struct.STRUC_SIZE 	2;
// mbdrc100_object_t.struct 
.CONST 	$mbdrc100_library.mbdrc100_object_t_struct.IN_BUFFER_FIELD	0;
.CONST 	$mbdrc100_library.mbdrc100_object_t_struct.IN_BUFFER_COMPLEX_2X16B_FIELD	4;
.CONST 	$mbdrc100_library.mbdrc100_object_t_struct.GAINS_FIELD	8;
.CONST 	$mbdrc100_library.mbdrc100_object_t_struct.PARAM_PTR_FIELD	12;
.CONST 	$mbdrc100_library.mbdrc100_object_t_struct.FFTBIN_FACTOR_FIELD	16;
.CONST 	$mbdrc100_library.mbdrc100_object_t_struct.MBDRC_LIB_FIELD	20;
.CONST 	$mbdrc100_library.mbdrc100_object_t_struct.MBDRC_MEM_REQ_FIELD	24;
.CONST	$mbdrc100_library.mbdrc100_object_t_struct.STRUC_SIZE 	8;
