/****************************************************************************
 * Copyright (c) 2020 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file coredump_table.h
 * \ingroup platform
 *
 * On-chip coredump region descriptor table public header
 */

#ifndef COREDUMP_TABLE_H
#define COREDUMP_TABLE_H

#include "types.h"

extern uint32 *coredump_region_ptr;

#endif /* COREDUMP_TABLE_H */