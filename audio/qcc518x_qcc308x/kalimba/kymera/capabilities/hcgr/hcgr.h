/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 *
 * \file  hcgr.h
 * \ingroup HCGR
 *
 * Howling Control and Gain Recovery operator public header file.
 *
 */

#ifndef _HCGR_H_
#define _HCGR_H_

#include "capabilities.h"
#include "base_aud_cur_op.h"

/****************************************************************************
Public Variable Definitions
*/
/** The capability data structure for HCGR */

extern const CAPABILITY_DATA hcgr_16k_cap_data;

#endif /* _HCGR_H_ */
