/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \file  anc_compander.h
 * \defgroup anc_compander
 *
 * \ingroup capabilities
 *
 * ANC Compander operator public header file.
 *
 */

#ifndef _ANC_COMPANDER_OP_H_
#define _ANC_COMPANDER_OP_H_

#include "capabilities.h"

/****************************************************************************
Public Variable Definitions
*/
/** The capability data structure for ANC COMPANDER*/
extern const CAPABILITY_DATA anc_compander_cap_data;

#endif /* _ANC_COMPANDER_OP_H_ */

