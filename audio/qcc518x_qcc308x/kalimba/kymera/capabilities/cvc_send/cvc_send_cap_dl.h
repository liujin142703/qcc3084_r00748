// *****************************************************************************
// Copyright (c) 2014 - 2017 Qualcomm Technologies International, Ltd.
// %%version
//
// *****************************************************************************

#ifndef _CVC_SEND_DATA_DL_H
#define _CVC_SEND_DATA_DL_H

#define $dms200.STATE_LENGTH              $dms200.STATE_LENGTH.DL
#define $dms200.pbp.STATE_LENGTH          $dms200.pbp.STATE_LENGTH.DL
#define $dms200.pbp.STATE_LENGTH_SWB_FB   $dms200.pbp.STATE_LENGTH_SWB_FB.DL
#define $dms200.STRUC_SIZE                $dms200.STRUC_SIZE.DL

#define $aec530.STRUCT_SIZE.FULL          $aec530.STRUCT_SIZE.FULL.DL

#endif // _CVC_SEND_DATA_DL_H
