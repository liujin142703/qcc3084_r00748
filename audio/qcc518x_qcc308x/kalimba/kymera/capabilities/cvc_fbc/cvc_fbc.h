/*******************************************************************************
 * Copyright (c) 2016 - 2020 Qualcomm Technologies International, Ltd.
*******************************************************************************/
/**
 * \file  cvc_fbc.h
 * \ingroup capabilities
 *
 * Stub Capability public header file. <br>
 *
 */

#ifndef CVC_FBC_H
#define CVC_FBC_H

#include "opmgr/opmgr_for_ops.h"

/** The capability data structure for stub capability */
extern const CAPABILITY_DATA cvc_fbc_cap_data;

#endif /* CVC_FBC_H */
