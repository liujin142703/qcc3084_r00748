/****************************************************************************
 * Copyright (c) 2021 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup ML_EXAMPLE_SVAD
 * \ingroup capabilities
 * \file  ml_example_svad.h
 * \ingroup ML_EXAMPLE_SVAD
 *
 * ML_base template public header file. 
 *
 */

#ifndef _ML_EXAMPLE_SVAD_H
#define _ML_EXAMPLE_SVAD_H

#include "capabilities.h"

/****************************************************************************
Public Variable Definitions
*/

/** The capability data structure for ml_example_svad */
#ifdef INSTALL_OPERATOR_ML_EXAMPLE_SVAD
extern const CAPABILITY_DATA ml_example_svad_cap_data;
#endif



#endif /* _ML_EXAMPLE_SVAD_H */
