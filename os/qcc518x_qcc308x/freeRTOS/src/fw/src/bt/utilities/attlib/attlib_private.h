/*******************************************************************************

Copyright (C) 2010 - 2020 Qualcomm Technologies International, Ltd.
All Rights Reserved.
Qualcomm Technologies International, Ltd. Confidential and Proprietary.

*******************************************************************************/

#ifndef __ATTLIB_PRIVATE_H__
#define __ATTLIB_PRIVATE_H__

#include "qbl_adapter_types.h"
#include "qbl_adapter_scheduler.h"
#include "qbl_adapter_pmalloc.h"
#include "qbl_adapter_vm.h"

#include <string.h>
#include "attlib.h"

#include INC_DIR(tbdaddr,tbdaddr.h)
#include INC_DIR(common,common.h)

#ifdef __cplusplus
extern "C" {
#endif





#ifdef __cplusplus
}
#endif

#endif  /* __ATTLIB_PRIVATE_H__ */
