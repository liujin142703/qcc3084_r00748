/*******************************************************************************

Copyright (C) 2023 Qualcomm Technologies International, Ltd.
All Rights Reserved.
Qualcomm Technologies International, Ltd. Confidential and Proprietary.

*******************************************************************************/

#ifndef __GATT_HIDS_SERVER_UUIDS_H__
#define __GATT_HIDS_SERVER_UUIDS_H__

/* UUIDs for Human Interface Device Service and Characteristics*/
#define UUID_HIDS_SERVICE                 0x1812


#define UUID_HIDS_INFORMATION             0x2A4A
#define UUID_REPORT_MAP                   0x2A4B
#define UUID_HIDS_CONTROL_POINT           0x2A4C
#define UUID_REPORT                       0x2A4D


#endif /* __GATT_HIDS_SERVER_UUIDS_H__ */

