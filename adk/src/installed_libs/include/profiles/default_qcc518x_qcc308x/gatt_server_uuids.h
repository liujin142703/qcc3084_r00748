/*******************************************************************************
Copyright (c) 2015 Qualcomm Technologies International, Ltd.
 %%version
*******************************************************************************/

#ifndef __GATT_SERVER_UUIDS_H__
#define __GATT_SERVER_UUIDS_H__

#define UUID_GATT                                      0x1801
#define UUID_SERVICE_CHANGED                           0x2A05
#define UUID_DATABASE_HASH                             0x2B2A
#define UUID_CLIENT_SUPPORTED_FEATURES                 0x2B29
#define UUID_SERVER_SUPPORTED_FEATURES                 0x2B3A

#endif /* __GATT_SERVER_UUIDS_H__ */
