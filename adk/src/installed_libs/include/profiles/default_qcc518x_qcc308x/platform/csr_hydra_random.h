#ifndef CSR_HYDRA_RANDOM_H__
#define CSR_HYDRA_RANDOM_H__
/******************************************************************************
 Copyright (c) 2019 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#include "csr_hydra_types.h"
#include "csr_synergy.h"

#ifdef __cplusplus
extern "C" {
#endif


extern uint16 UtilRandom(void );

#ifdef __cplusplus
}
#endif

#endif /* CSR_HYDRA_RANDOM_H__ */
