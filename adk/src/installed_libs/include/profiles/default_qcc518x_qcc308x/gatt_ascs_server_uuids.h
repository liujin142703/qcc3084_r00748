/*******************************************************************************
Copyright (c) 2019 Qualcomm Technologies International, Ltd.
 %%version
*******************************************************************************/

#ifndef __GATT_ASCS_SERVER_UUIDS_H__
#define __GATT_ASCS_SERVER_UUIDS_H__

/* UUIDs for Audio Stream Control Service and Characteristics*/


#define UUID_ASCS_SERVICE                     0x184E

#define UUID_ASCS_ASE_SINK_CHAR               0x2BC4
#define UUID_ASCS_ASE_SOURCE_CHAR             0x2BC5 
#define UUID_ASCS_ASE_CONTROL_POINT_CHAR      0x2BC6



#endif /* __GATT_ASCS_SERVER_UUIDS_H__ */

