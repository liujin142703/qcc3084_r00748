/******************************************************************************
 Copyright (c) 2021 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#ifndef __GATT_BATTERY_SERVER_UUIDS_H__
#define __GATT_BATTERY_SERVER_UUIDS_H__

#define UUID_BATTERY_SERVICE                           0x180f
#define UUID_BATTERY_LEVEL                             0x2a19

#endif /* __GATT_BATTERY_SERVER_UUIDS_H__ */
