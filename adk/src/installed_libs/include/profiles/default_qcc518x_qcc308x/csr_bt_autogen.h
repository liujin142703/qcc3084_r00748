#ifndef CSR_BT__AUTOGEN_H__
#define CSR_BT__AUTOGEN_H__
/******************************************************************************
 Copyright (c) 2008-2017 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#include "csr_synergy.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "csr_bt_util.h"

#ifdef __cplusplus
}
#endif

#endif

