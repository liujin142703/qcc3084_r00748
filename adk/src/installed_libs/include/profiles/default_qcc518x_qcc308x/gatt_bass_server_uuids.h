/******************************************************************************
 Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #56 $
******************************************************************************/

#ifndef __GATT_BASS_SERVER_UUIDS_H__
#define __GATT_BASS_SERVER_UUIDS_H__

#define UUID_BASS_SERVICE                           0x184F

#define UUID_BASS_BROADCAST_AUDIO_SCAN_CONTROL_POINT    0x2BC7
#define UUID_BASS_BROADCAST_RECEIVE_STATE               0x2BC8

#endif /* __GATT_BASS_SERVER_UUIDS_H__ */
