/******************************************************************************
 Copyright (c) 2021 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 
 REVISION:      $Revision: #56 $
******************************************************************************/


#ifndef __GATT_TRANSPORT_DISCOVERY_SERVER_UUIDS_H__
#define __GATT_TRANSPORT_DISCOVERY_SERVER_UUIDS_H__

/* UUIDs for TDS and Characteristics*/

#define UUID_TRANSPORT_DISCOVERY_SERVICE                    0x1824

#define UUID_TRANSPORT_DISCOVERY_CONTROL_POINT              0x2ABC
#define UUID_TRANSPORT_DISCOVERY_BREDR_HANDOVER_DATA        0x2B38
#define UUID_TRANSPORT_DISCOVERY_SIG_DATA                   0x2B39
#define UUID_TRANSPORT_DISCOVERY_BREDR_TRANSPORT_BLOCK_DATA 0x290F



#endif /* __GATT_TRANSPORT_DISCOVERY_SERVER_UUIDS_H__ */

