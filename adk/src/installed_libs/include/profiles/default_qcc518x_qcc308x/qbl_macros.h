#ifndef QBL_MACROS_H__
#define QBL_MACROS_H__
/******************************************************************************
 Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MIN
#define MIN CSRMIN
#endif

#ifndef MAX
#define MAX CSRMAX
#endif

#define ENV_PRIM 0xffff

#ifdef __cplusplus
}
#endif

#endif

