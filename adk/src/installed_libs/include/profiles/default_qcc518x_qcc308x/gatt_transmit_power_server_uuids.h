/******************************************************************************
 Copyright (c) 2021 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 
 REVISION:      $Revision: #1 $
******************************************************************************/

#ifndef __GATT_TRANSMIT_POWER_SERVER_UUIDS_H__
#define __GATT_TRANSMIT_POWER_SERVER_UUIDS_H__

#define UUID_TRANSMIT_POWER_SERVICE                           0x1804
#define UUID_TRANSMIT_POWER_LEVEL                             0x2A07

#endif /* __GATT_TRANSMIT_POWER_SERVER_UUIDS_H__ */
