#ifndef CSIP_ENUM_DBG_H__
#define CSIP_ENUM_DBG_H__
/*******************************************************************************
 * File: csip_enum_dbg.h
 * Copyright (c) 2022-2024 Qualcomm Technologies International, Ltd.
 * All Rights Reserved.
 * Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 *
 * DO NOT EDIT
 * Generated from csip.h by enum_generator.py 
 ******************************************************************************/
#include "csip.h"

enum CsipStatus{
    _CSIP_STATUS_SUCCESS = CSIP_STATUS_SUCCESS,
    _CSIP_STATUS_IN_PROGRESS = CSIP_STATUS_IN_PROGRESS,
    _CSIP_STATUS_INVALID_PARAMETER = CSIP_STATUS_INVALID_PARAMETER,
    _CSIP_STATUS_DISCOVERY_ERR = CSIP_STATUS_DISCOVERY_ERR,
    _CSIP_STATUS_FAILED = CSIP_STATUS_FAILED,
    _CSIP_STATUS_INST_PERSISTED = CSIP_STATUS_INST_PERSISTED,
};

enum CsipMessageId{
    _CSIP_INIT_CFM = CSIP_INIT_CFM,
    _CSIP_DESTROY_CFM = CSIP_DESTROY_CFM,
    _CSIP_READ_CS_INFO_CFM = CSIP_READ_CS_INFO_CFM,
    _CSIP_CS_SET_NTF_CFM = CSIP_CS_SET_NTF_CFM,
    _CSIP_SET_LOCK_CFM = CSIP_SET_LOCK_CFM,
    _CSIP_LOCK_STATUS_IND = CSIP_LOCK_STATUS_IND,
    _CSIP_SIZE_CHANGED_IND = CSIP_SIZE_CHANGED_IND,
    _CSIP_SIRK_CHANGED_IND = CSIP_SIRK_CHANGED_IND,
    _CSIP_SIRK_DECRYPT_CFM = CSIP_SIRK_DECRYPT_CFM,
};

enum CsipCsInfo{
    _CSIP_SIRK = CSIP_SIRK,
    _CSIP_SIZE = CSIP_SIZE,
    _CSIP_RANK = CSIP_RANK,
    _CSIP_LOCK = CSIP_LOCK,
};

#endif /*CSIP_ENUM_DBG_H__*/
