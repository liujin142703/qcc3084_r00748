/******************************************************************************
 Copyright (c) 2021-2022 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #56 $
******************************************************************************/

#ifndef __GATT_TMAS_SERVER_UUIDS_H__
#define __GATT_TMAS_SERVER_UUIDS_H__

/* UUIDs for TMAS Service and Characteristics*/
#define UUID_TELEPHONY_MEDIA_AUDIO_SERVICE          0x1855
#define UUID_ROLE                                   0x2B51

#endif /* __GATT_TMAS_SERVER_UUIDS_H__ */

