#ifndef QBL_ADAPTER_TYPES_H__
#define QBL_ADAPTER_TYPES_H__
/******************************************************************************
 Copyright (c) 2020-2022 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #2 $
******************************************************************************/

#include "csr_bt_bluestack_types.h"

#if (defined(HYDRA) || defined(CAA)) && defined(BLUESTACK_LIB)
#include "timed_event_oxygen/timed_event_oxygen.h"
#endif
#endif

