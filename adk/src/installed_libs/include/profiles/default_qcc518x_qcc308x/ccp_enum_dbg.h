#ifndef CCP_ENUM_DBG_H__
#define CCP_ENUM_DBG_H__
/*******************************************************************************
 * File: ccp_enum_dbg.h
 * Copyright (c) 2022-2024 Qualcomm Technologies International, Ltd.
 * All Rights Reserved.
 * Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 *
 * DO NOT EDIT
 * Generated from ccp.h by enum_generator.py 
 ******************************************************************************/
#include "ccp.h"

enum CcpMessageId{
    _CCP_INIT_CFM = CCP_INIT_CFM,
    _CCP_DESTROY_CFM = CCP_DESTROY_CFM,
    _CCP_TBS_TERMINATE_CFM = CCP_TBS_TERMINATE_CFM,
    _CCP_SET_CFM = CCP_SET_CFM,
    _CCP_READ_PROVIDER_NAME_CFM = CCP_READ_PROVIDER_NAME_CFM,
    _CCP_READ_BEARER_UCI_CFM = CCP_READ_BEARER_UCI_CFM,
    _CCP_READ_BEARER_TECHNOLOGY_CFM = CCP_READ_BEARER_TECHNOLOGY_CFM,
    _CCP_READ_BEARER_URI_SCHEMES_SUPPORTED_LIST_CFM = CCP_READ_BEARER_URI_SCHEMES_SUPPORTED_LIST_CFM,
    _CCP_READ_SIGNAL_STRENGTH_CFM = CCP_READ_SIGNAL_STRENGTH_CFM,
    _CCP_READ_SIGNAL_STRENGTH_INTERVAL_CFM = CCP_READ_SIGNAL_STRENGTH_INTERVAL_CFM,
    _CCP_READ_CURRENT_CALLS_LIST_CFM = CCP_READ_CURRENT_CALLS_LIST_CFM,
    _CCP_READ_CONTENT_CONTROL_ID_CFM = CCP_READ_CONTENT_CONTROL_ID_CFM,
    _CCP_READ_FEATURE_AND_STATUS_FLAGS_CFM = CCP_READ_FEATURE_AND_STATUS_FLAGS_CFM,
    _CCP_READ_INCOMING_CALL_TARGET_BEARER_URI_CFM = CCP_READ_INCOMING_CALL_TARGET_BEARER_URI_CFM,
    _CCP_READ_CALL_STATE_CFM = CCP_READ_CALL_STATE_CFM,
    _CCP_READ_INCOMING_CALL_CFM = CCP_READ_INCOMING_CALL_CFM,
    _CCP_READ_CALL_FRIENDLY_NAME_CFM = CCP_READ_CALL_FRIENDLY_NAME_CFM,
    _CCP_READ_CCP_OPTIONAL_OPCODES_CFM = CCP_READ_CCP_OPTIONAL_OPCODES_CFM,
    _CCP_PROVIDER_NAME_SET_NOTIFICATION_CFM = CCP_PROVIDER_NAME_SET_NOTIFICATION_CFM,
    _CCP_TECHNOLOGY_SET_NOTIFICATION_CFM = CCP_TECHNOLOGY_SET_NOTIFICATION_CFM,
    _CCP_SIGNAL_STRENGTH_SET_NOTIFICATION_CFM = CCP_SIGNAL_STRENGTH_SET_NOTIFICATION_CFM,
    _CCP_CURRENT_CALLS_SET_NOTIFICATION_CFM = CCP_CURRENT_CALLS_SET_NOTIFICATION_CFM,
    _CCP_FLAGS_SET_NOTIFICATION_CFM = CCP_FLAGS_SET_NOTIFICATION_CFM,
    _CCP_INCOMING_CALL_TARGET_BEARER_URI_SET_NOTIFICATION_CFM = CCP_INCOMING_CALL_TARGET_BEARER_URI_SET_NOTIFICATION_CFM,
    _CCP_CALL_STATE_SET_NOTIFICATION_CFM = CCP_CALL_STATE_SET_NOTIFICATION_CFM,
    _CCP_CALL_CONTROL_POINT_SET_NOTIFICATION_CFM = CCP_CALL_CONTROL_POINT_SET_NOTIFICATION_CFM,
    _CCP_TERMINATION_REASON_SET_NOTIFICATION_CFM = CCP_TERMINATION_REASON_SET_NOTIFICATION_CFM,
    _CCP_INCOMING_CALL_SET_NOTIFICATION_CFM = CCP_INCOMING_CALL_SET_NOTIFICATION_CFM,
    _CCP_CALL_FRIENDLY_NAME_SET_NOTIFICATION_CFM = CCP_CALL_FRIENDLY_NAME_SET_NOTIFICATION_CFM,
    _CCP_WRITE_SIGNAL_STRENGTH_INTERVAL_CFM = CCP_WRITE_SIGNAL_STRENGTH_INTERVAL_CFM,
    _CCP_WRITE_CALL_CONTROL_POINT_CFM = CCP_WRITE_CALL_CONTROL_POINT_CFM,
    _CCP_PROVIDER_NAME_IND = CCP_PROVIDER_NAME_IND,
    _CCP_BEARER_TECHNOLOGY_IND = CCP_BEARER_TECHNOLOGY_IND,
    _CCP_SIGNAL_STRENGTH_IND = CCP_SIGNAL_STRENGTH_IND,
    _CCP_CURRENT_CALLS_IND = CCP_CURRENT_CALLS_IND,
    _CCP_FLAGS_IND = CCP_FLAGS_IND,
    _CCP_INCOMING_CALL_TARGET_BEARER_URI_IND = CCP_INCOMING_CALL_TARGET_BEARER_URI_IND,
    _CCP_CALL_STATE_IND = CCP_CALL_STATE_IND,
    _CCP_CALL_CONTROL_POINT_IND = CCP_CALL_CONTROL_POINT_IND,
    _CCP_TERMINATION_REASON_IND = CCP_TERMINATION_REASON_IND,
    _CCP_INCOMING_CALL_IND = CCP_INCOMING_CALL_IND,
    _CCP_CALL_FRIENDLY_NAME_IND = CCP_CALL_FRIENDLY_NAME_IND,
    _CCP_MESSAGE_TOP = CCP_MESSAGE_TOP,
};

enum CcpStatus{
    _CCP_STATUS_SUCCESS = CCP_STATUS_SUCCESS,
    _CCP_STATUS_IN_PROGRESS = CCP_STATUS_IN_PROGRESS,
    _CCP_STATUS_INVALID_PARAMETER = CCP_STATUS_INVALID_PARAMETER,
    _CCP_STATUS_DISCOVERY_ERR = CCP_STATUS_DISCOVERY_ERR,
    _CCP_STATUS_FAILED = CCP_STATUS_FAILED,
};

#endif /*CCP_ENUM_DBG_H__*/
