#ifndef GATT_CSIS_SERVER_ENUM_DBG_H__
#define GATT_CSIS_SERVER_ENUM_DBG_H__
/*******************************************************************************
 * File: gatt_csis_server_enum_dbg.h
 * Copyright (c) 2022-2024 Qualcomm Technologies International, Ltd.
 * All Rights Reserved.
 * Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 *
 * DO NOT EDIT
 * Generated from gatt_csis_server.h by enum_generator.py 
 ******************************************************************************/
#include "gatt_csis_server.h"

enum GattCsisServerLocktype{
    _CSIS_SERVER_UNLOCKED = CSIS_SERVER_UNLOCKED,
    _CSIS_SERVER_LOCKED = CSIS_SERVER_LOCKED,
    _CSIS_SERVER_INVALID_LOCK_VALUE = CSIS_SERVER_INVALID_LOCK_VALUE,
};

enum GattCsisServerMessageIdType{
    _GATT_CSIS_LOCK_STATE_IND = GATT_CSIS_LOCK_STATE_IND,
    _GATT_CSIS_SERVER_CONFIG_CHANGE_IND = GATT_CSIS_SERVER_CONFIG_CHANGE_IND,
};

#endif /*GATT_CSIS_SERVER_ENUM_DBG_H__*/
