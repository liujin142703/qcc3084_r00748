#ifndef CSR_BT_HANDOVER_IF_H__
#define CSR_BT_HANDOVER_IF_H__

/****************************************************************************
Copyright (c) 2019-2022 Qualcomm Technologies International, Ltd.


FILE NAME
    csr_bt_handover_if.h

DESCRIPTION
    Header file for the TWS Mirroring Handover interface library.

*/

#include "csr_synergy.h"
#include "csr_types.h"
#include "bluetooth.h"
#include "csr_bt_addr.h"
#include "bdaddr/bdaddr.h"
#include "vm.h"

/* handover interface defined in common library */
#include "handover_if.h"

#endif /* CSR_BT_HANDOVER_IF_H__ */
