/******************************************************************************
 Copyright (c) 2020-2023 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #2 $
******************************************************************************/

#ifndef __GATT_MCS_SERVER_UUIDS_H__
#define __GATT_MCS_SERVER_UUIDS_H__

/* UUIDs for Media Control Service and Characteristics*/
#define GATT_GMCS_UUID_MEDIA_CONTROL_SERVICE            0x1849

/* UUIDS: UUIDS here are according to MCPS assigned number 1-19*/


#define GATT_GMCS_UUID_MEDIA_PLAYER_NAME                0x2B93
#define GATT_GMCS_UUID_MEDIA_PLAYER_ICON_OBJ_ID         0x2B94
#define GATT_GMCS_UUID_MEDIA_PLAYER_ICON_URL            0x2B95
#define GATT_GMCS_UUID_TRACK_CHANGED                    0x2B96
#define GATT_GMCS_UUID_TRACK_TITLE                      0x2B97
#define GATT_GMCS_UUID_TRACK_DURATION                   0x2B98
#define GATT_GMCS_UUID_TRACK_POSITION                   0x2B99
#define GATT_GMCS_UUID_PLAYBACK_SPEED                   0x2B9A
#define GATT_GMCS_UUID_SEEKING_SPEED                    0x2B9B
#define GATT_GMCS_UUID_CURRENT_TRACK_SEGMENTS_OBJ_ID    0x2B9C
#define GATT_GMCS_UUID_CURRENT_TRACK_OBJ_ID             0x2B9D
#define GATT_GMCS_UUID_NEXT_TRACK_OBJ_ID                0x2B9E
#define GATT_GMCS_UUID_CURRENT_GROUP_OBJ_ID             0x2BA0
#define GATT_GMCS_UUID_PARENT_GROUP_OBJ_ID              0x2B9F
#define GATT_GMCS_UUID_PLAYING_ORDER                    0x2BA1
#define GATT_GMCS_UUID_PLAYING_ORDER_SUPPORTED          0x2BA2
#define GATT_GMCS_UUID_MEDIA_STATE                      0x2BA3
#define GATT_GMCS_UUID_MEDIA_CONTROL_POINT              0x2BA4
#define GATT_GMCS_UUID_MEDIA_CONTROL_POINT_OP_SUPP      0x2BA5
#define GATT_GMCS_UUID_SEARCH_RESULTS_OBJ_ID            0x2BA6
#define GATT_GMCS_UUID_SEARCH_CONTROL_POINT             0x2BA7
#define GATT_GMCS_UUID_MEDIA_CONTENT_CONTROL_ID         0x2BBA

#endif /* __GATT_MCS_SERVER_UUIDS_H__ */

