/******************************************************************************
 Copyright (c) 2022 - 2023 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#ifndef __GENERIC_BROADCAST_SCAN_UUIDS_H__
#define __GENERIC_BROADCAST_SCAN_UUIDS_H__

#define UUID_GENERIC_BROADCAST_SCAN_SERVICE                0xbaebd52bd02d4c9c81b5a6c0f7428eed

#define UUID_GENERIC_BROADCAST_SCAN_CONTROL_POINT          0xd3784627ec774043bcb5b94267f760fe
#define UUID_GENERIC_BROADCAST_SCAN_REPORT                 0xaee4ce2a6fe04da3b34c5fcb8e592109
#define UUID_GENERIC_BROADCAST_RECEIVER_STATE              0xb3bb90610e6248d595b84c9c1c742837

#define UUID_GENERIC_BROADCAST_VOLUME_CONTROL_POINT        0x2860b64e0eae45c7b7b7f49cc6258cc9
#define UUID_GENERIC_BROADCAST_VOLUME_STATE                0xd8b167350f414fed9549ee28c05f4f60


#endif /* __GENERIC_BASS_SERVER_UUIDS_H__ */
