/******************************************************************************
 Copyright (c) 2021-2023 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision #2 $
******************************************************************************/

#ifndef __GATT_GMAS_SERVER_UUIDS_H__
#define __GATT_GMAS_SERVER_UUIDS_H__

/* UUIDs for Gaming Audio Service */ 
#define UUID_GAMING_AUDIO_SERVICE       0x1858
#define UUID_GMAS_ROLE                  0x2C00
#define UUID_GMAS_UGG_FEATURES          0x2C01
#define UUID_GMAS_UGT_FEATURES          0x2C02
#define UUID_GMAS_BGS_FEATURES          0x2C03
#define UUID_GMAS_BGR_FEATURES          0x2C04
#endif /* __GATT_GMAS_SERVER_UUIDS_H__ */

