/*!
    \copyright  Copyright (c) 2019 - 2023 Qualcomm Technologies International, Ltd.
                All Rights Reserved.
                Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file
    \defgroup   voice_audio_tuning_mode Voice Audio Tuning Mode
    @{
        \ingroup    voice_ui_layer
        \brief      Voice Audio Tuning VA client interface
*/

#ifndef VOICE_AUDIO_TUNING_H_
#define VOICE_AUDIO_TUNING_H_

/*!   
    \brief Initialise audio tuning VA client.
    \param init_task Unused parameter.
    \return True if initialisation is successful
*/
bool VoiceAudioTuningMode_Init(Task init_task);

#endif /*_VOICE_AUDIO_TUNING_H_ */
/*! @} */