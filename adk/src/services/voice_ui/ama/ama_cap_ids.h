/*!
   \copyright  Copyright (c) 2020 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \addtogroup ama
   @{
   \brief      Configuration related definitions for AMA capabilities support.
*/

#ifndef AMA_CAP_IDS_H_
#define AMA_CAP_IDS_H_

#ifdef INCLUDE_AMA_WUW
#define DOWNLOAD_APVA
#endif

#endif /* AMA_CAP_IDS_H_ */

/*! @} */