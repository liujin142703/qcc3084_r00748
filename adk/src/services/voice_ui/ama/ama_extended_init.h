/*!
   \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \version    
   \file       ama_extended_init.h
   \addtogroup ama
   @{
   \brief      Extended initialisation of AMA.
*/

#ifndef AMA_EXTENDED_INIT_H
#define AMA_EXTENDED_INIT_H

/*! \brief Extended initialisation of AMA.
 */
void Ama_ExtendedInit(void);

#endif // AMA_EXTENDED_INIT_H
/*! @} */