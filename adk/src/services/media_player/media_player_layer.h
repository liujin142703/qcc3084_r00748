/*!
   \copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   media_player_layer Media Player
   @{
      \ingroup services
   @}
*/

/* This exists purely for doxygen directory generation to form the tree structure. */

#ifndef MEDIA_PLAYER_LAYER_
#define MEDIA_PLAYER_LAYER_

#endif /* MEDIA_PLAYER_LAYER_ */
