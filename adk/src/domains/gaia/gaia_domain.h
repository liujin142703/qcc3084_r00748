/*!
   \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file       gaia_domain.h
   \defgroup   gaia_domain GAIA
   @{
      \ingroup domains
      A group of GAIA related components and plugins.

      The GAIA domain provides abstraction and implementation for GAIA features and plugins.



*/

/*
   This exists purely for doxygen directory generation to form the tree structure holding all GAIA related Components.
*/

#ifndef GAIA_DOMAIN_H
#define GAIA_DOMAIN_H

#endif // GAIA_DOMAIN_H

/*! @} */