/*!
   \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file       case_comms_domain.h
   \defgroup   case_comms_domain Case Communications
   \ingroup    domains
   
   A group of case communication components.

   The case communication domain provides collection of components related to case comms communications.



*/

/* 
      This exists purely for doxygen directory generation to form the tree structure holding all Case Communication related Components.
*/

#ifndef CASE_COMMS_DOMAIN_H
#define CASE_COMMS_DOMAIN_H

#endif // CASE_COMMS_DOMAIN_H
