/*!
   \copyright  Copyright (c) 2019 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   audio_domain Audio
   \ingroup    domains
   
   A group of audio related components
   
   The audio domain provides abstraction for audio subsystem implementation.
   It should be possible to add new audio source without modifying existing audio domain code.

*/

/*   
   This exists purely for doxygen directory generation to form the tree structure holding all Audio related Components.
*/

#ifndef AUDIO_DOMAIN_H_
#define AUDIO_DOMAIN_H_

#endif /* AUDIO_DOMAIN_H_ */
