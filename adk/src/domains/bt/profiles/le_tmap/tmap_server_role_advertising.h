/*!
    \copyright  Copyright (c) 2022 - 2023 Qualcomm Technologies International, Ltd.
                All Rights Reserved.
                Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file
    \addtogroup tmap_profile
    \brief
    @{
*/

#ifndef TMAP_SERVER_ROLE_ADVERTISING_H_
#define TMAP_SERVER_ROLE_ADVERTISING_H_

/*! \brief Setup the LE advertising data for TMAP

    \returns TRUE if the advertising data was setup successfully, else FALSE
 */
bool TmapServer_SetupLeAdvertisingData(void);

#endif /* TMAP_SERVER_ROLE_ADVERTISING_H_ */
/*! @} */