/*!
    \copyright  Copyright (c) 2020 - 2023 Qualcomm Technologies International, Ltd.
                All Rights Reserved.
                Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file
    \addtogroup le_bap
    \brief
    @{
*/

#ifndef SCAN_DELEGATOR_ROLE_ADVERTISING_H_
#define SCAN_DELEGATOR_ROLE_ADVERTISING_H_

/*! \brief Setup the LE advertising data for the scan delegator role

    \returns TRUE if the advertising data was setup successfully, else FALSE
 */
bool LeBapScanDelegator_SetupLeAdvertisingData(void);

#endif /* SCAN_DELEGATOR_ROLE_ADVERTISING_H_ */
/*! @} */