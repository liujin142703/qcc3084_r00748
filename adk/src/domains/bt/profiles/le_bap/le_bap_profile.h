/*!
    \copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.
                All Rights Reserved.
                Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file
    \defgroup   le_bap  LE BAP
    @{
    \ingroup    profiles
    \brief      Common defines, structure and functions for BAP profile wrapper
*/

#ifndef LE_BAP_PROFILE_H_
#define LE_BAP_PROFILE_H_

#define ISOC_DATA_PATH_ID_RAW_STREAM_ENDPOINTS_ONLY  0x06

#define ISOC_DATA_PATH_ID_ISO_PATH_SHADOWING_PERIPHERAL  0xFA

#define ISOC_DATA_PATH_DIRECTION_HOST_TO_CONTROLLER  0x00
#define ISOC_DATA_PATH_DIRECTION_CONTROLLER_TO_HOST  0x01

#endif /* LE_BAP_PROFILE_H_ */
/*! @} */