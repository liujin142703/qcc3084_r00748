
#ifndef A2DP_PROFILE_CODEC_LDAC_H
#define A2DP_PROFILE_CODEC_LDAC_H

#ifdef ENABLE_LDAC_SINK

/*************************************************************************
NAME
    getLdacConfigSettings

DESCRIPTION
    Return the codec configuration settings (rate and channel mode) for the physical codec based
    on the A2DP codec negotiated settings.

*/
void getSonyLdacConfigSettings(const uint8 *service_caps, a2dp_codec_settings *codec_settings);


/*************************************************************************
NAME
     selectOptimalLdacCapsSink

DESCRIPTION
    Selects the optimal configuration for LDAC playback by setting a single
    bit in each field of the passed caps structure.

    Note that the priority of selecting features is a
    design decision and not specified by the A2DP profiles.

*/
void selectOptimalSonyLdacCapsSink(const uint8 *local_codec_caps, uint8 *remote_codec_caps);


#endif /* ENABLE_LDAC_SINK */

#endif // A2DP_PROFILE_CODEC_LDAC_H
