/*!
   \copyright  Copyright (c) 2022 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \addtogroup le_cap
   \brief
   @{
*/

#ifndef CAP_SERVER_ROLE_H_
#define CAP_SERVER_ROLE_H_

/*! \brief Initialize the LE CAP Server
 */
void LeCapServer_Init(void);

#endif /* CAP_SERVER_ROLE_H_ */

/*! @} */