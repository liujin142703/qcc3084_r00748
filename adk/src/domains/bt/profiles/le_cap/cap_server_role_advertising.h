/*!
    \copyright  Copyright (c) 2022 - 2023 Qualcomm Technologies International, Ltd.
                All Rights Reserved.
                Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file
    \addtogroup le_cap
    \brief
    @{
*/

#ifndef CAP_SERVER_ROLE_ADVERTISING_H_
#define CAP_SERVER_ROLE_ADVERTISING_H_

/*! \brief Setup the LE advertising data for the CAP server role

    \returns TRUE if the advertising data was setup successfully, else FALSE
 */
bool CapServer_SetupLeAdvertisingData(void);

#endif /* CAP_SERVER_ROLE_ADVERTISING_H_ */
/*! @} */