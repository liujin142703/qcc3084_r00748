/*!
    \copyright Copyright (c) 2024 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \version %%version
    \file 
    \brief The le_audio_volume_sync marshal type definitions. This file is generated by F:/qcc3084/3084_F05_r00784/adk/tools/packages/typegen/typegen.py.
*/

#include <csrtypes.h>
#include <source_param_types.h>
#include <app/marshal/marshal_if.h>
#include <le_audio_volume_sync_typedef.h>
#include <le_audio_volume_sync_marshal_typedef.h>
#include <marshal_common.h>

#include "domain_marshal_types.h"

#ifndef HOSTED_TEST_ENVIRONMENT
COMPILE_TIME_ASSERT(sizeof(source_type_t) == sizeof(uint8), source_type_t_uint8_mismatched);
COMPILE_TIME_ASSERT(sizeof(audio_source_t) == sizeof(uint8), audio_source_t_uint8_mismatched);
COMPILE_TIME_ASSERT(sizeof(voice_source_t) == sizeof(uint8), voice_source_t_uint8_mismatched);
#endif

static const marshal_member_descriptor_t le_audio_volume_sync_update_t_member_descriptors[] =
{
    /*! The source_type_t of the source */
    MAKE_MARSHAL_MEMBER(le_audio_volume_sync_update_t, source_type_t, source_type),
    /*! Either the audio_source_t or voice_source_t of the source */
    MAKE_MARSHAL_MEMBER(le_audio_volume_sync_update_t, uint8, source),
    /*! The absolute volume, in VCP units */
    MAKE_MARSHAL_MEMBER(le_audio_volume_sync_update_t, uint8, volume),
    /*! The mute state of the source */
    MAKE_MARSHAL_MEMBER(le_audio_volume_sync_update_t, uint8, mute),
} ;

const marshal_type_descriptor_t marshal_type_descriptor_le_audio_volume_sync_update_t = MAKE_MARSHAL_TYPE_DEFINITION(le_audio_volume_sync_update_t, le_audio_volume_sync_update_t_member_descriptors);


#define EXPAND_AS_TYPE_DEFINITION(type) (const marshal_type_descriptor_t *)&marshal_type_descriptor_##type,
const marshal_type_descriptor_t * const le_audio_volume_sync_marshal_type_descriptors[] =
{
    MARSHAL_COMMON_TYPES_TABLE(EXPAND_AS_TYPE_DEFINITION)
    LE_AUDIO_VOLUME_SYNC_MARSHAL_TYPES_TABLE(EXPAND_AS_TYPE_DEFINITION)
} ;

#undef EXPAND_AS_TYPE_DEFINITION

