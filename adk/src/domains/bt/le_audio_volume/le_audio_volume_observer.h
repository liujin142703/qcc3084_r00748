/*!
   \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \addtogroup le_audio_volume
   \brief      Audio and voice source volume observers used to update the VCP volume.
   @{
*/

#ifndef LE_AUDIO_VOLUME_OBSERVER_H_
#define LE_AUDIO_VOLUME_OBSERVER_H_

/*!\ brief
*/
void LeAudioVolume_RegisterVolumeObservers(void);

#endif /* LE_AUDIO_VOLUME_OBSERVER_H_ */
/**! @} */