/*!
\copyright  Copyright (c) 2018 - 2023 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\defgroup   le_advertising_manager LE Advertising Manager
@{
\ingroup    bt_domain
\brief      Header file for management of Bluetooth Low Energy advertising settings

Provides control for Bluetooth Low Energy (BLE) advertisements.
*/


#ifndef LE_ADVERTISING_MANAGER_H_
#define LE_ADVERTISING_MANAGER_H_

#ifndef INCLUDE_LEGACY_LE_ADVERTISING_MANAGER
#include "le_advertising_manager_multi_set.h"
#else
#include "le_advertising_manager_legacy.h"
#endif

#endif /* LE_ADVERTISING_MANAGER_H_ */
/*! @} */