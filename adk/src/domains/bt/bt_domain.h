/*!
   \copyright  Copyright (c) 2019 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   bt_domain Bluetooth
   \ingroup    domains

   A group of BREDR and LE related components.
   
   The bt domain provides abstraction for a connection and profiles libraries.
   It should be possible to add new profile without modifying existing bt domain code.
   

*/

/*
   This exists purely for doxygen directory generation to form the tree structure holding all Bluetooth related Components.
*/


#ifndef BT_DOMAIN_H_
#define BT_DOMAIN_H_

#endif /* BT_DOMAIN_H_ */
