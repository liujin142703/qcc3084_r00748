/*!
    \copyright Copyright (c) 2024 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \version %%version
    \file 
    \brief The av c type definitions. This file is generated by F:/qcc3084/3084_F05_r00784/adk/tools/packages/typegen/typegen.py.
*/

#ifndef _AV_TYPEDEF_H__
#define _AV_TYPEDEF_H__

#include <csrtypes.h>
#include <marshal_common.h>
#include <task_list.h>

#include "avrcp_typedef.h"
#include "a2dp_typedef.h"
#include "av_callback_interface.h"

/*! AV Instance task data structure */
typedef struct 
{
    /*! Task/Message information for this instance */
    TaskData av_task;
    /*! Bluetooth Address of remote device */
    bdaddr bd_addr;
    /*! AVRCP task data */
    avrcpTaskData avrcp;
    /*! A2DP task data */
    a2dpTaskData a2dp;
    /*! Flag indicating if the instance is about to be detatched */
    uint8 detach_pending;
    /*! A pointer to the plugin interface */
    const av_callback_interface_t *av_callbacks;
} avInstanceTaskData;

#endif /* _AV_TYPEDEF_H__ */

