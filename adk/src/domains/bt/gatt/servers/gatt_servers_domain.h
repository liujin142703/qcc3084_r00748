/*!
   \copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   gatt_servers_domain Servers
   @{
   \ingroup    gatt
   \brief      GATT Servers.
 
*/

#ifndef GATT_SERVERS_H_
#define GATT_SERVERS_H_

#endif /* GATT_SERVERS_H_ */

/*! @} */