/*!
   \copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   gatt_common_domain Common
   @{
   \ingroup    gatt
   \brief      GATT Clients.
 
*/

#ifndef GATT_COMMON_H_
#define GATT_COMMON_H_

#endif /* GATT_COMMON_H_ */

/*! @} */