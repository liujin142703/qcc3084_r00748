/*!
    \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
    All Rights Reserved.
    Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file
    \ingroup gatt_service_discovery_synergy
    \brief   Gatt Service Discovery implementation.
*/


#include "gatt_service_discovery.h"
#include "gatt_service_discovery_context.h"
#include "gatt_service_discovery_private.h"

/* List of gatt client identifiers (generated by the gatt client create macros in gatt_client.h) */
extern const client_id_t gatt_ams_client_id;
extern const client_id_t gatt_ancs_client_id;

bool gattServiceDiscovery_InitLegacyClients(void)
{
    static const client_id_t* client_priority_discover[] =
    {
        &gatt_ams_client_id,
        &gatt_ancs_client_id,
    };

    GattClientInit(client_priority_discover);

    /* Service IDs corresponding to the gatt clients specified in 'client_priority_discover' */
    static const GattSdSrvcId client_priority_service_id[] =
    {
        GATT_SD_AMS_SRVC,
        GATT_SD_ANCS_SRVC
    };
    gattServiceDiscovery_InitLegacyClientsList(client_priority_discover, client_priority_service_id,
                                               sizeof(client_priority_discover)/sizeof(client_priority_discover[0]));
    return TRUE;
}
