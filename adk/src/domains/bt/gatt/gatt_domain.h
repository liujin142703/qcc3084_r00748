/*!
   \copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   gatt GATT
   @{
   \ingroup    bt_domain
   \brief      A group of GATT related components.
 
*/

#ifndef GATT_H_
#define GATT_H_

#endif /* GATT_H_ */

/*! @} */