/*!
   \copyright  Copyright (c) 2019 - 2023 Qualcomm Technologies International, Ltd.
   All Rights Reserved.
   Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   gatt_client_ancs  GATT ANCs Client
   @{
   \ingroup    gatt_client_domain
   \brief      Application support for GATT ANCS client
*/

#ifndef GATT_CLIENT_ANCS_H
#define GATT_CLIENT_ANCS_H

void GattAncsClientDisableNotificationsByDefault(void);

#endif /* GATT_CLIENT_ANCS_H */
/*! @} */