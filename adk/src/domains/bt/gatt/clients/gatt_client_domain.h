/*!
   \copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   gatt_client_domain Clients
   @{
   \ingroup    gatt
   \brief      GATT Clients.
 
*/

#ifndef GATT_CLIENT_H_
#define GATT_CLIENT_H_

#endif /* GATT_CLIENT_H_ */

/*! @} */