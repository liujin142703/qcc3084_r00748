/*!
\copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      DCF engine configuration
*/

#ifndef DCF_CONFIG_H_
#define DCF_CONFIG_H_

#ifdef INCLUDE_DCF
#define INCLUDE_DCF_OPEN_GARDEN     1
#endif

#endif
