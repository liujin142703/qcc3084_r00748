/*!
\copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      DCF API
*/

#ifndef DCF_H_
#define DCF_H_

/*! \brief Initialises the DCF component and any implementation engines.
*/
bool Dcf_Init(Task init_task);

#endif