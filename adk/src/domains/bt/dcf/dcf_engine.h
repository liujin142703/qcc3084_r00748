/*!
\copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      A DCF engine is an a DCF implementation, and is responsible for defining the data elements, data element sets,
            and identities that make up that implementation.
*/

#ifndef DCF_ENGINE_H_
#define DCF_ENGINE_H_

/*! \brief Initialises the DCF example implementation.
*/
#ifdef INCLUDE_DCF_OPEN_GARDEN
void DcfOpenGarden_Init(void);
#endif

#endif