/*!
    \copyright  Copyright (c) 2022 - 2023  Qualcomm Technologies International, Ltd.\n
                All Rights Reserved.\n
                Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file       
    \addtogroup user_accounts
    \brief      User account types supported by the user_accounts component.
    @{
*/

#ifndef USER_ACCOUNTS_TYPES_H_
#define USER_ACCOUNTS_TYPES_H_

/*! \brief Enum to list out account key types */
typedef enum
{
    user_account_type_invalid,
    user_account_type_fast_pair,
    user_account_type_max
} user_account_type;

#endif /* USER_ACCOUNTS_TYPES_H_ */
/*! @} */