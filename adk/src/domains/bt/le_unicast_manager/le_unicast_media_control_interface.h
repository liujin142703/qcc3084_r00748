/*!
    \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
                All Rights Reserved.
                Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file
    \addtogroup le_unicast_manager
    \brief      Header file for the unicast media control interface
    @{
*/

#ifndef LE_UNICAST_MEDIA_CONTROL_INTERFACE_H_
#define LE_UNICAST_MEDIA_CONTROL_INTERFACE_H_

/*! \brief Initialises the LE Unicast media control interface.
    \return 
 */
void LeUnicastMediaControlInterface_Init(void);

#endif /* LE_UNICAST_MEDIA_CONTROL_INTERFACE_H_ */
/*! @} */