/*!
    \copyright Copyright (c) 2024 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \version %%version
    \file 
    \brief The le_broadcast_manager_sync marshal type declarations. This file is generated by F:/qcc3084/3084_F05_r00784/adk/tools/packages/typegen/typegen.py.
*/

#ifndef _LE_BROADCAST_MANAGER_SYNC_MARSHAL_TYPEDEF_H__
#define _LE_BROADCAST_MANAGER_SYNC_MARSHAL_TYPEDEF_H__

#include <csrtypes.h>
#include <scan_delegator_role.h>
#include <panic.h>
#include <app/marshal/marshal_if.h>
#include <marshal_common.h>


#define LE_BROADCAST_MANAGER_SYNC_MARSHAL_TYPES_TABLE(ENTRY)\
    ENTRY(le_broadcast_manager_sync_add_broadcast_source_t)\
    ENTRY(le_broadcast_manager_sync_modify_broadcast_source_t)\
    ENTRY(le_broadcast_manager_sync_remove_broadcast_source_t)\
    ENTRY(le_broadcast_manager_sync_set_broadcast_code_t)\
    ENTRY(le_broadcast_manager_sync_set_source_match_address_t)\
    ENTRY(le_broadcast_sync_command_t)\
    ENTRY(le_broadcast_manager_sync_ready_to_start_ind_t)\
    ENTRY(le_broadcast_manager_sync_unmute_ind_t)\
    ENTRY(le_broadcast_manager_sync_command_ind_t)\
    ENTRY(le_broadcast_manager_sync_pause_ind_t)\
    ENTRY(le_broadcast_manager_sync_resume_ind_t)\
    ENTRY(le_broadcast_manager_sync_to_source_t)

#define EXPAND_AS_ENUMERATION(type) MARSHAL_TYPE(type),
enum LE_BROADCAST_MANAGER_SYNC_MARSHAL_TYPES
{
    DUMMY = (NUMBER_OF_COMMON_MARSHAL_OBJECT_TYPES-1),
    LE_BROADCAST_MANAGER_SYNC_MARSHAL_TYPES_TABLE(EXPAND_AS_ENUMERATION)
    NUMBER_OF_LE_BROADCAST_MANAGER_SYNC_MARSHAL_TYPES
} ;

#undef EXPAND_AS_ENUMERATION

extern const marshal_type_descriptor_t * const le_broadcast_manager_sync_marshal_type_descriptors[];
#endif /* _LE_BROADCAST_MANAGER_SYNC_MARSHAL_TYPEDEF_H__ */

