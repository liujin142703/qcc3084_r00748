/*!
   \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \addtogroup leabm
   \brief      Media Control implementation for the LE broadcast audio source.
   @{
*/

#ifndef LE_BROADCAST_MEDIA_CONTROL_H_
#define LE_BROADCAST_MEDIA_CONTROL_H_

#include "audio_sources_media_control_interface.h"


const media_control_interface_t * leBroadcastManager_MediaControlGetInterface(void);

#endif /* LE_BROADCAST_MEDIA_CONTROL_H_ */
/*! @} */