/*!
   \copyright  Copyright (c) 2020 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \addtogroup leabm
   \brief
   @{
*/

#ifndef LE_BROADCAST_MUSIC_SOURCE_H_
#define LE_BROADCAST_MUSIC_SOURCE_H_

#include "audio_sources_audio_interface.h"


const audio_source_audio_interface_t * LeBroadcastMusicSource_GetAudioInterface(void);

#endif /* LE_BROADCAST_MUSIC_SOURCE_H_ */
/*! @} */