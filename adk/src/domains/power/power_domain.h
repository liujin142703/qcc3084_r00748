/*!
   \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file       power_domain.h
   \defgroup   power_domain Power
   \ingroup    domains
   
   A group of power components.

   The power domain provides abstraction and implementation a collection of system power related components.

*/

/*   
   This exists purely for doxygen directory generation to form the tree structure holding all Power related Components. 
*/

#ifndef POWER_DOMAIN_H
#define POWER_DOMAIN_H

#endif // POWER_DOMAIN_H
