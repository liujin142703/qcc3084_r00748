/*!
\copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file       
\defgroup   event_bus   Event Bus
\ingroup    domains
\brief      Common types used by the Event Bus API's
@{
*/      

#ifndef EVENT_BUS_TYPES_H_
#define EVENT_BUS_TYPES_H_

#include <csrtypes.h>

typedef unsigned event_bus_channel_t;
typedef unsigned event_bus_event_t;

#endif
/*! @} End of group documentation */