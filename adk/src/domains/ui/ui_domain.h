/*!
   \copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   ui_domain UI
   @{
      \ingroup    domains
      
      
*/

/* This exists purely for doxygen directory generation to form the tree structure holding all UI Components. */

#ifndef UI_DOMAINS_LAYER_H_
#define UI_DOMAINS_LAYER_H_

#endif /* UI_DOMAINS_LAYER_H_ */

/*! @} */