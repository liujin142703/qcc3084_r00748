/*!
    \copyright  Copyright (c) 2022 - 2023 Qualcomm Technologies International, Ltd.\n
                All Rights Reserved.\n
                Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \version    
    \file       motion.h
    \ingroup    motion
    \brief      Vendor specific implementation of motion API
*/

#ifdef INCLUDE_MOTION

#include "motion.h"

bool Motion_IsInMotion(void)
{
    return FALSE;
}

#endif /* INCLUDE_MOTION */
