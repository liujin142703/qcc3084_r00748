/*!
   \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file       sensor_domain.h
   \defgroup   sensor_domain Sensor
   @{
      \ingroup    domains

      A group of sensor components.
      The sensor domain provides collection of available sensors.
*/

/*       
   This exists purely for doxygen directory generation to form the tree structure holding all UI Components.
*/
 
 
#ifndef SENSOR_DOMAIN_H
#define SENSOR_DOMAIN_H
 
#endif // SENSOR_DOMAIN_H

/*! @}  */