/*!
   \copyright  Copyright (c) 2021 - 2023 Qualcomm Technologies International, Ltd.
               All Rights Reserved.
               Qualcomm Technologies International, Ltd. Confidential and Proprietary.
   \file
   \defgroup   device_domain Device
   \ingroup    domains
   A group of device related components.

   The device domain provides abstraction for device and multidevice access.



*/

/* 
   This exists purely for doxygen directory generation to form the tree structure holding all Device Components.
*/


#ifndef DEVICE_DOMAIN_H
#define DEVICE_DOMAIN_H

#endif /* DEVICE_DOMAIN_H */
