/*!
\copyright  Copyright (c) 2019-2023 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\version    
\file       
\brief      
*/

#ifndef TWS_TOPOLOGY_PROC_STANDALONE_PRIMARY_ROLE_H
#define TWS_TOPOLOGY_PROC_STANDALONE_PRIMARY_ROLE_H

#include "script_engine.h"

extern const procedure_script_t standalone_primary_role_script;

#endif /* TWS_TOPOLOGY_PROC_STANDALONE_PRIMARY_ROLE_H */
