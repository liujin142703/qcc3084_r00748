/*!
\copyright  Copyright (c) 2022 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      stereo_topology control of advertising parameters.
*/
#ifndef STEREO_TOPOLOGY_ADVERTISING_H__
#define STEREO_TOPOLOGY_ADVERTISING_H__

/*! \brief This function inspects the system state and decides which advertising
           parameter set to use.
*/
void stereoTopology_UpdateAdvertisingParams(void);

#endif /* STEREO_TOPOLOGY_ADVERTISING_H__ */


