/*!
\copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\version    
\file       
\brief      
*/

#ifndef STEREO_TOPOLOGY_PROC_SEC_CONNECT_PEER_H
#define STEREO_TOPOLOGY_PROC_SEC_CONNECT_PEER_H

#include "stereo_topology_procedures.h"

#include <panic.h>
#include <message.h>

extern const procedure_fns_t stereo_proc_sec_connect_peer_fns;

#endif /* STEREO_TOPOLOGY_PROC_SEC_CONNECT_PEER_H */
