/*!
\copyright  Copyright (c) 2022 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\version    
\file       
\brief      Procedure to stop handset reconnection.
*/

#ifndef STEREO_TOPOLOGY_PROC_STOP_HANDSET_RECONNECT_H
#define STEREO_TOPOLOGY_PROC_STOP_HANDSET_RECONNECT_H

#include "stereo_topology_procedures.h"

extern const procedure_fns_t stereo_proc_stop_handset_reconnect_fns;

#endif /* STEREO_TOPOLOGY_PROC_STOP_HANDSET_RECONNECT_H */
