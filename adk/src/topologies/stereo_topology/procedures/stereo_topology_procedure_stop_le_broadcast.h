/*!
\copyright  Copyright (c) 2022 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\version    
\brief      Interface to procedure to stop LE broadcast.
*/

#ifndef STEREO_TOPOLOGY_PROC_STOP_LE_BROADCAST_H
#define STEREO_TOPOLOGY_PROC_STOP_LE_BROADCAST_H

#include "stereo_topology_procedures.h"

extern const procedure_fns_t stereo_proc_stop_le_broadcast_fns;

#endif /* STEREO_TOPOLOGY_PROC_STOP_LE_BROADCAST_H */
