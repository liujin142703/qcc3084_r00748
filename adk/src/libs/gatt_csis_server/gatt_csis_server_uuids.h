/*******************************************************************************
Copyright (c) 2019 - 2020 Qualcomm Technologies International, Ltd.
 
*******************************************************************************/

#ifndef __GATT_CSIS_SERVER_UUIDS_H__
#define __GATT_CSIS_SERVER_UUIDS_H__

/* UUIDs for Coordinated Set Identification Service and Characteristics*/


#define UUID_COORDINATED_SET_IDENTIFICATION_SERVICE   0x1846

#define UUID_SET_IDENTITY_RESOLVING_KEY     0x2B84
#define UUID_COORDINATED_SET_SIZE           0x2B85
#define UUID_COORDINATED_SET_LOCK           0x2B86
#define UUID_COORDINATED_SET_RANK           0x2B87


#endif /* __GATT_CSIS_SERVER_UUIDS_H__ */

