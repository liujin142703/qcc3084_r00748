/*******************************************************************************
Copyright (c) 2019 Qualcomm Technologies International, Ltd.
 
*******************************************************************************/

#ifndef __GATT_ASCS_SERVER_UUIDS_H__
#define __GATT_ASCS_SERVER_UUIDS_H__

/* UUIDs for Audio Stream Control Service and Characteristics*/


#define UUID_ASCS_SERVICE                     0x8FDA

#define UUID_ASCS_ASE_CHAR                    0x8F9C
#define UUID_ASCS_ASE_CONTROL_POINT_CHAR      0x8F9D



#endif /* __GATT_ASCS_SERVER_UUIDS_H__ */

