/* Copyright (c) 2020 Qualcomm Technologies International, Ltd. */
/*  */

#ifndef GATT_TELEPHONE_BEARER_SERVER_MSG_HANDLER_H_
#define GATT_TELEPHONE_BEARER_SERVER_MSG_HANDLER_H_

/***************************************************************************
NAME
    tbsServerMsgHandler

DESCRIPTION
    Handler for external messages sent to the library in the server role.
*/
void tbsServerMsgHandler(Task task, MessageId id, Message msg);


#endif /* GATT_TELEPHONE_BEARER_SERVER_MSG_HANDLER_H_ */

