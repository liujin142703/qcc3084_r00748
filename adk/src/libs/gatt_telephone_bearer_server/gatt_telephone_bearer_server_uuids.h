/*******************************************************************************
Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 
*******************************************************************************/

#ifndef __GATT_TELEPHONE_BEARER_SERVER_UUIDS_H__
#define __GATT_TELEPHONE_BEARER_SERVER_UUIDS_H__

/* UUIDs for Telephone Bearer Service and Characteristics*/

/* TODO NB: UUIDS here are as July Virtual IOP 2020 */

#define UUID_TELEPHONE_BEARER_SERVICE                        0x8FD5
#define GATT_GTBS_UUID_TELEPHONE_BEARER_SERVICE              0x8FDF


#define GATT_TBS_UUID_BEARER_PROVIDER_NAME                   0x8FEA
#define GATT_TBS_UUID_BEARER_UCI                             0x8FEB
#define GATT_TBS_UUID_BEARER_TECHNOLOGY                      0x8FEC
#define GATT_TBS_UUID_BEARER_URI_PREFIX_LIST                 0x8FED
#define GATT_TBS_UUID_SIGNAL_STRENGTH                        0x8FEF
#define GATT_TBS_UUID_SIGNAL_STRENGTH_REPORTING_INTERVAL     0x8FF0
#define GATT_TBS_UUID_LIST_CURRENT_CALLS                     0x8FF1
#define GATT_TBS_UUID_CONTENT_CONTROL_ID                     0x8FB5
#define GATT_TBS_UUID_STATUS_FLAGS                           0x8FF3
#define GATT_TBS_UUID_INCOMING_CALL_TARGET_BEARER_URI        0x8FF4
#define GATT_TBS_UUID_CALL_STATE                             0x8FF5
#define GATT_TBS_UUID_CALL_CONTROL_POINT                     0x8FF6
#define GATT_TBS_UUID_CALL_CONTROL_POINT_OPCODES             0x8FF7
#define GATT_TBS_UUID_TERMINATION_REASON                     0x8FF8
#define GATT_TBS_UUID_INCOMING_CALL                          0x8FF9
#define GATT_TBS_UUID_REMOTE_FRIENDLY_NAME                   0x8FFA


#endif /* __GATT_TELEPHONE_BEARER_SERVER_UUIDS_H__ */

