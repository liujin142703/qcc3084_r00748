/*******************************************************************************
Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 
*******************************************************************************/
/*! \file
    Definitions of VCS UUIDs for the GATT VCS Client.
 */

#ifndef __GATT_VCS_CLIENT_UUIDS_H__
#define __GATT_VCS_CLIENT_UUIDS_H__

#define GATT_CHARACTERISTIC_UUID_VOLUME_STATE            0x2B7D
#define GATT_CHARACTERISTIC_UUID_VOLUME_CONTROL_POINT    0x2B7E
#define GATT_CHARACTERISTIC_UUID_VOLUME_FLAGS            0x2B7F

#endif /* __GATT_VCS_CLIENT_UUIDS_H__ */
