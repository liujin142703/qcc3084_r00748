/*******************************************************************************
Copyright (c) 2021 Qualcomm Technologies International, Ltd.
 
*******************************************************************************/

#ifndef __GATT_BASS_SERVER_UUIDS_H__
#define __GATT_BASS_SERVER_UUIDS_H__

#define UUID_BASS_SERVICE                            0x184F

#define UUID_BASS_BROADCAST_AUDIO_SCAN_CONTROL_POINT    0x2BC7
#define UUID_BASS_BROADCAST_RECEIVE_STATE               0x2BC8

#endif /* __GATT_BASS_SERVER_UUIDS_H__ */
