/*******************************************************************************
Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 
*******************************************************************************/
/*!
    Definitions of BASS UUIDs for the GATT BASS Client.
 */

#ifndef __GATT_BASS_CLIENT_UUIDS_H__
#define __GATT_BASS_CLIENT_UUIDS_H__

#define GATT_CHARACTERISTIC_UUID_BROADCAST_AUDIO_SCAN_CONTROL_POINT    0x8F9E
#define GATT_CHARACTERISTIC_UUID_BROADCAST_RECEIVE_STATE               0x8F9F

#endif /* __GATT_BASS_CLIENT_UUIDS_H__ */
