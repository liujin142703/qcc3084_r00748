#ifndef _CSR_FRW_CONFIG_PRIVATE_H
#define _CSR_FRW_CONFIG_PRIVATE_H
/****************************************************************************
 *
 *       Copyright (c) 2012-2015 Qualcomm Technologies International, Ltd.
 *
 *
 *       All Rights Reserved. 
 *       
 ****************************************************************************/

/* This is the place where private cmake cache parameters shall be defined. 
    The parameters added here will not be distributed in released software packages. */

/* #undef CSR_FRW_LEGACY_AUTOGEN_TOOL */
	
#endif /* _CSR_FRW_CONFIG_PRIVATE_H */
