#ifndef CSR_BT_CSR_BT_PLATFORM_H__
#define CSR_BT_CSR_BT_PLATFORM_H__
/******************************************************************************
 Copyright (c) 2008-2017 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/
#define CSR_BT_MAX_PATH_LENGTH 128
#endif

