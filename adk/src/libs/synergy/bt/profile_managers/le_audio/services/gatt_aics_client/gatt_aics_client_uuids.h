/*******************************************************************************
Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 %%version
*******************************************************************************/
/*! \file
    Definitions of AICS UUIDs for the GATT AICS Client.
 */

#ifndef __GATT_AICS_CLIENT_UUIDS_H__
#define __GATT_AICS_CLIENT_UUIDS_H__

#define GATT_CHARACTERISTIC_UUID_INPUT_STATE                0x2B77
#define GATT_CHARACTERISTIC_UUID_GAIN_SETTING_PROPERTIES    0x2B78
#define GATT_CHARACTERISTIC_UUID_INPUT_TYPE                 0x2B79
#define GATT_CHARACTERISTIC_UUID_INPUT_STATUS               0x2B7A
#define GATT_CHARACTERISTIC_UUID_AUDIO_INPUT_CONTROL_POINT  0x2B7B
#define GATT_CHARACTERISTIC_UUID_AUDIO_INPUT_DESCRIPTION    0x2B7C

#endif /* __GATT_AICS_CLIENT_UUIDS_H__ */
