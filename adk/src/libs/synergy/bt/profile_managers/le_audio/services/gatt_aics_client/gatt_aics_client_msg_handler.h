/* Copyright (c) 2020 Qualcomm Technologies International, Ltd. */
/* %%version */

#ifndef GATT_AICS_CLIENT_MSG_HANDLER_H_
#define GATT_AICS_CLIENT_MSG_HANDLER_H_

/***************************************************************************
NAME
    gattAicsClientMsgHandler

DESCRIPTION
    Handler for external messages sent to the library in the client role.
*/
void gattAicsClientMsgHandler(void **gash);

#endif
