/******************************************************************************
 Copyright (c) 2021-2022 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 
 REVISION:      $Revision: #56 $
******************************************************************************/
/*! \file
    Definitions of TMAS UUIDs for the GATT TMAS Client.
 */

#ifndef __GATT_TMAS_CLIENT_UUIDS_H__
#define __GATT_TMAS_CLIENT_UUIDS_H__

/* UUID Values as per latest spec */
#define GATT_CHARACTERISTIC_UUID_ROLE                0x2B51

#endif /* __GATT_TMAS_CLIENT_UUIDS_H__ */
