/*******************************************************************************
Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 %%version
*******************************************************************************/
/*! \file
    Definitions of VOCS UUIDs for the GATT VOCS Client.
 */

#ifndef __GATT_VOCS_CLIENT_UUIDS_H__
#define __GATT_VOCS_CLIENT_UUIDS_H__

#define GATT_CHARACTERISTIC_UUID_OFFSET_STATE                 0x2B80
#define GATT_CHARACTERISTIC_UUID_AUDIO_LOCATION               0x2B81
#define GATT_CHARACTERISTIC_UUID_VOLUME_OFFSET_CONTROL_POINT  0x2B82
#define GATT_CHARACTERISTIC_UUID_AUDIO_OUTPUT_DESCRIPTION     0x2B83

#endif /* __GATT_VOCS_CLIENT_UUIDS_H__ */
