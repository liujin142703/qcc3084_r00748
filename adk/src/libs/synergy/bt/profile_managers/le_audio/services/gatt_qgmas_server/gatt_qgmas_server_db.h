/*
 * THIS FILE IS AUTOGENERATED, DO NOT EDIT!
 *
 * generated by gattdbgen from bt/profile_managers/le_audio/services/gatt_qgmas_server/gatt_qgmas_server_db.dbi_
 */
#ifndef __GATT_QGMAS_SERVER_DB_H
#define __GATT_QGMAS_SERVER_DB_H

#include <csrtypes.h>

#define HANDLE_QGAMING_AUDIO_SERVICE    (0x0001)
#define HANDLE_QGAMING_AUDIO_SERVICE_END (0xffff)

typedef enum
{
    QGAMING_AUDIO_SERVICE,
    
    gatt_sdp_last
} gatt_sdp;

uint8 *GattGetServiceRecord(gatt_sdp service, uint16 *len);
uint16 *GattGetDatabase(uint16 *len);
uint16 GattGetDatabaseSize(void);
const uint16 *GattGetConstDatabase(uint16 *len);

#endif

/* End-of-File */
