/******************************************************************************
 Copyright (c) 2020-2022 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#ifndef __GATT_MICS_SERVER_UUIDS_H__
#define __GATT_MICS_SERVER_UUIDS_H__

/* UUIDs for Media Control Service and Characteristics*/

/* TODO NB: UUIDS here are as UPF65 2020 */

/* UUID Values taken from MC_IOP_Test_Plan_0.9.0r08 (with difference from 8FA8 to 8FAD)*/

#define GATT_MICS_UUID_MICROPHONE_CONTROL_SERVICE  0x184D
#define GATT_MICS_UUID_SERVER_MUTE                 0x2BC3

#endif /* __GATT_MICS_SERVER_UUIDS_H__ */

