/******************************************************************************
 Copyright (c) 2022 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision #2 $
******************************************************************************/

#ifndef __GATT_QGMAS_SERVER_UUIDS_H__
#define __GATT_QGMAS_SERVER_UUIDS_H__

/* UUID for Gaming Audio Service of Qualcomm */
#define UUID_QGAMING_AUDIO_SERVICE       0x12994b7e6d4742158c9eaae9a1095ba3

#endif /* __GATT_QGMAS_SERVER_UUIDS_H__ */

