/*
 * THIS FILE IS AUTOGENERATED, DO NOT EDIT!
 *
 * generated by gattdbgen from bt/profile_managers/le_audio/services/gatt_vcs_server/gatt_vcs_server_db.dbi_
 */
#ifndef __GATT_VCS_SERVER_DB_H
#define __GATT_VCS_SERVER_DB_H

#include <csrtypes.h>

#define HANDLE_VOLUME_CONTROL_SERVICE   (0x0001)
#define HANDLE_VOLUME_CONTROL_SERVICE_END (0xffff)
#define HANDLE_VOLUME_STATE             (0x0003)
#define HANDLE_VOLUME_STATE_CLIENT_CONFIG (0x0004)
#define HANDLE_VOLUME_CONTROL_POINT     (0x0006)
#define HANDLE_VOLUME_FLAGS             (0x0008)
#define HANDLE_VOLUME_FLAGS_CLIENT_CONFIG (0x0009)

typedef enum
{
    VOLUME_CONTROL_SERVICE,
    
    gatt_sdp_last
} gatt_sdp;

uint8 *GattGetServiceRecord(gatt_sdp service, uint16 *len);
uint16 *GattGetDatabase(uint16 *len);
uint16 GattGetDatabaseSize(void);
const uint16 *GattGetConstDatabase(uint16 *len);

#endif

/* End-of-File */
