/******************************************************************************
 Copyright (c) 2021 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #56 $
******************************************************************************/

#ifndef __GATT_WMAS_SERVER_UUIDS_H__
#define __GATT_WMAS_SERVER_UUIDS_H__

/* UUIDs for Wireless Microphone Audio Service */
#define UUID_WIRELESS_MICROPHONE_AUDIO_SERVICE        0x2587db3cce704fc9935f777ab4188fd7

#endif /* __GATT_WMAS_SERVER_UUIDS_H__ */

