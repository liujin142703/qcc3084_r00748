/******************************************************************************
 Copyright (c) 2022-2023 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 
 REVISION:      $Revision: #3 $
******************************************************************************/
/*! \file
    Definitions of GMAS UUIDs for the GATT GMAS Client.
 */

#ifndef __GATT_GMAS_CLIENT_UUIDS_H__
#define __GATT_GMAS_CLIENT_UUIDS_H__

/* UUID Values as per latest spec */
#define GATT_CHARACTERISTIC_GMAS_UUID_ROLE                0x2C00
#define GATT_CHARACTERISTIC_GMAS_UUID_UGG_FEATURES        0x2C01
#define GATT_CHARACTERISTIC_GMAS_UUID_UGT_FEATURES        0x2C02
#define GATT_CHARACTERISTIC_GMAS_UUID_BGS_FEATURES        0x2C03
#define GATT_CHARACTERISTIC_GMAS_UUID_BGR_FEATURES        0x2C04

#endif /* __GATT_GMAS_CLIENT_UUIDS_H__ */
