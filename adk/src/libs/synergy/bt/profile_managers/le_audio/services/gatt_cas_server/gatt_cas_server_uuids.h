/*******************************************************************************
Copyright (c) 2021-2022 Qualcomm Technologies International, Ltd.
 %%version
*******************************************************************************/

#ifndef __GATT_CAS_SERVER_UUIDS_H__
#define __GATT_CAS_SERVER_UUIDS_H__

/* UUIDs for Common Audio Service */

#define UUID_COMMON_AUDIO_SERVICE                 0x1853



#endif /* __GATT_CAS_SERVER_UUIDS_H__ */

