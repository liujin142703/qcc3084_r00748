/******************************************************************************
 Copyright (c) 2021 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #57 $
******************************************************************************/

#ifndef __GATT_TELEPHONE_BEARER_SERVER_UUIDS_H__
#define __GATT_TELEPHONE_BEARER_SERVER_UUIDS_H__

/* UUIDs for Telephone Bearer Service and Characteristics*/

#define UUID_TELEPHONE_BEARER_SERVICE                        0x184B
#define GATT_GTBS_UUID_TELEPHONE_BEARER_SERVICE              0x184C


#define GATT_TBS_UUID_BEARER_PROVIDER_NAME                   0x2BB3
#define GATT_TBS_UUID_BEARER_UCI                             0x2BB4
#define GATT_TBS_UUID_BEARER_TECHNOLOGY                      0x2BB5
#define GATT_TBS_UUID_BEARER_URI_PREFIX_LIST                 0x2BB6
#define GATT_TBS_UUID_SIGNAL_STRENGTH                        0x2BB7
#define GATT_TBS_UUID_SIGNAL_STRENGTH_REPORTING_INTERVAL     0x2BB8
#define GATT_TBS_UUID_LIST_CURRENT_CALLS                     0x2BB9
#define GATT_TBS_UUID_CONTENT_CONTROL_ID                     0x2BBA
#define GATT_TBS_UUID_STATUS_FLAGS                           0x2BBB
#define GATT_TBS_UUID_INCOMING_CALL_TARGET_BEARER_URI        0x2BBC
#define GATT_TBS_UUID_CALL_STATE                             0x2BBD
#define GATT_TBS_UUID_CALL_CONTROL_POINT                     0x2BBE
#define GATT_TBS_UUID_CALL_CONTROL_POINT_OPCODES             0x2BBF
#define GATT_TBS_UUID_TERMINATION_REASON                     0x2BC0
#define GATT_TBS_UUID_INCOMING_CALL                          0x2BC1
#define GATT_TBS_UUID_REMOTE_FRIENDLY_NAME                   0x2BC2


#endif /* __GATT_TELEPHONE_BEARER_SERVER_UUIDS_H__ */

