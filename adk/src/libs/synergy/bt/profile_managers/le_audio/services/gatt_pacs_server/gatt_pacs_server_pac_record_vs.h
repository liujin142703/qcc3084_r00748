/******************************************************************************
 Copyright (c) 2021 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#ifndef GATT_PACS_SERVER_PAC_RECORD_VS_H_
#define GATT_PACS_SERVER_PAC_RECORD_VS_H_

/***************************************************************************
NAME
    getGeneratedVSPacsRecord

DESCRIPTION
    Returns generated VS PACS Record for Sink.
*/
uint8* getGeneratedVSPacsRecord(uint8* len, uint16 handle);

#endif
