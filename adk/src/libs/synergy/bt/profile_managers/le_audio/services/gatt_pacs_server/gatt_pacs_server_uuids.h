/******************************************************************************
 Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #57 $
******************************************************************************/
#ifndef __GATT_PACS_SERVER_UUIDS_H__
#define __GATT_PACS_SERVER_UUIDS_H__

/* UUIDs for Published Audio Capabilities Service and Characteristics*/
/* Numbers according to BAP Assigned Numbers v12*/

#define UUID_PUBLISHED_AUDIO_CAPABILITIES_SERVICE           0x1850
#define UUID_SINK_PAC                                       0x2BC9
#define UUID_SINK_AUDIO_LOCATIONS                           0x2BCA
#define UUID_SOURCE_PAC                                     0x2BCB
#define UUID_SOURCE_AUDIO_LOCATIONS                         0x2BCC
#define UUID_AUDIO_CONTEXT_AVAILABILTY                      0x2BCD
#define UUID_SUPPORTED_AUDIO_CONTEXTS                       0x2BCE


#endif /* __GATT_PACS_SERVER_UUIDS_H__ */

