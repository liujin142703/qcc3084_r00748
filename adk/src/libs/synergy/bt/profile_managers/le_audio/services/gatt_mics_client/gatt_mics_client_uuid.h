/****************************************************************************
 Copyright (c) 2022 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

%%version
*******************************************************************************/

/*! \file
    Definitions of MICS UUIDs for the GATT MICS Client.
 */

#ifndef __GATT_MICS_CLIENT_UUIDS_H__
#define __GATT_MICS_CLIENT_UUIDS_H__

#define GATT_CHARACTERISTIC_UUID_MUTE            0x2BC3

#endif /* __GATT_MICS_CLIENT_UUIDS_H__ */
