/*******************************************************************************
Copyright (c) 2020-2021 Qualcomm Technologies International, Ltd.
 %%version
*******************************************************************************/
/*!
    Definitions of BASS UUIDs for the GATT BASS Client.
 */

#ifndef __GATT_BASS_CLIENT_UUIDS_H__
#define __GATT_BASS_CLIENT_UUIDS_H__

#define GATT_CHARACTERISTIC_UUID_BROADCAST_AUDIO_SCAN_CONTROL_POINT    0x2BC7
#define GATT_CHARACTERISTIC_UUID_BROADCAST_RECEIVE_STATE               0x2BC8

#endif /* __GATT_BASS_CLIENT_UUIDS_H__ */
