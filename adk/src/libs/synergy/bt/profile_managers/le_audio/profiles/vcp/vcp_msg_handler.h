/* Copyright (c) 2020 Qualcomm Technologies International, Ltd. */
/* %%version */

#ifndef VCP_MSG_HANDLER_H_
#define VCP_MSG_HANDLER_H_


/***************************************************************************
NAME
    vcpMsgHandler

DESCRIPTION
    Handler for external messages sent to the library in the client role.
*/
void vcpMsgHandler(void **gash);

#endif
