/****************************************************************************
 Copyright (c) 2022 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

%%version
*/

#ifndef MICP_MSG_HANDLER_H_
#define MICP_MSG_HANDLER_H_


/***************************************************************************
NAME
    micpMsgHandler

DESCRIPTION
    Handler for external messages sent to the library in the client role.
*/
void micpMsgHandler(void **gash);

#endif
