/*!
 Copyright (c) 2010-2023 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

\file   bap_client_ase_fsm_constants.c

\brief  FSM constants.
        Contains code generated by fsmgen.pl, version 2D
*/

#include "bap_client_ase_fsm_definitions.h"

#ifdef INSTALL_LEA_UNICAST_CLIENT

/* Action function table */
/*{{__FSM_MARKER(BapClientAse, ACTION_FN_TABLE) */
static const fsm_action_fn_t bapClientAseActionFns[] =
{
    bapClientAseAfSendConfigCodecOp,
    bapClientAseAfCodecConfigureIndReceived,
    bapClientAseAfCodecConfigureCfmReceived,
    bapClientAseAfCodecConfigureCfmFailedReceived,
    bapClientAseAfSendReleaseOp,
    bapClientAseAfReleaseCfmReceived,
    bapClientAseAfSendConfigQosOp,
    bapClientAseAfReleaseIndReceived,
    bapClientAseAfQosConfigureCfmReceived,
    bapClientAseAfQosConfigureCfmFailedReceived,
    bapClientAseAfSendEnableOp,
    bapClientAseAfSendCisDisconnect,
    bapClientAseAfCisDisconnectReceived,
    bapClientAseAfCisDisconnectIndReceived,
    bapClientAseAfCisConnect,
    bapClientAseAfCisConnectCfmSuccessReceived,
    bapClientAseAfCisConnectCfmFailedReceived,
    bapClientAseAfDisableIndReceived,
    bapClientAseAfEnableCfmReceived,
    bapClientAseAfEnableFailedReceived,
    bapClientAseAfSendDisableOp,
    bapClientAseAfSendUpdateMetadataOp,
    bapClientAseAfUpdateMetadataCfmReceived,
    bapClientAseAfUpdateMetadataFailedReceived,
    bapClientAseAfStopReadyCfmReceived,
    bapClientAseAfSendStartReadyOp,
    bapClientAseAfStartReadyReceived,
    bapClientAseAfStartReadyFailedReceived,
    bapClientAseAfStopReadySendOp,
    bapClientAseAfStopReadyCfmFailedReceived,
    bapClientAseAfDisableCfmReceived,
    bapClientAseAfDisableCfmFailedReceived,
    bapClientAseAfReleaseCfmFailedReceived,
    bapClientAseAfReleasedIndReceived,
};
/*}}__FSM_END_MARKER(BapClientAse, ACTION_FN_TABLE) */

/*
 * Actions-per-state initialisation code
 * Format:
 *      Event, Action index, Next state
 */

/*{{__FSM_MARKER(BapClientAse, STATE_TABLES) */
static const FSM_SPARSE16_TRANSITION_T bapClientAseTransitions[] = {
  /* IDLE */
  { CLIENT_ASE_EVENT_CONFIGURE,                      CLIENT_ASE_AC_send_config_codec_op,                 CLIENT_ASE_STATE_WAIT_CODEC_CONFIGURE_CFM },
  { CLIENT_ASE_EVENT_CONFIGURE_IND,                  CLIENT_ASE_AC_codec_configure_ind_received,         CLIENT_ASE_STATE_CODEC_CONFIGURED },
  /* WAIT_CODEC_CONFIGURE_CFM */
  { CLIENT_ASE_EVENT_CODEC_CONFIGURE_CFM_RECEIVED,   CLIENT_ASE_AC_codec_configure_cfm_received,         CLIENT_ASE_STATE_CODEC_CONFIGURED },
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_codec_configure_cfm_failed_received,  CLIENT_ASE_STATE_IDLE },
  /* CODEC_CONFIGURED */
  { CLIENT_ASE_EVENT_CONFIGURE,                      CLIENT_ASE_AC_send_config_codec_op,                 CLIENT_ASE_STATE_CODEC_CONFIGURED },
  { CLIENT_ASE_EVENT_CONFIGURE_IND,                  CLIENT_ASE_AC_codec_configure_ind_received,         CLIENT_ASE_STATE_CODEC_CONFIGURED },
  { CLIENT_ASE_EVENT_CODEC_CONFIGURE_CFM_RECEIVED,   CLIENT_ASE_AC_codec_configure_cfm_received,         CLIENT_ASE_STATE_CODEC_CONFIGURED },
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_codec_configure_cfm_failed_received,  CLIENT_ASE_STATE_CODEC_CONFIGURED },
  { CLIENT_ASE_EVENT_RELEASE,                        CLIENT_ASE_AC_send_release_op,                      CLIENT_ASE_STATE_WAIT_RELEASE_CFM },
  { CLIENT_ASE_EVENT_RELEASE_CFM_RECEIVED,           CLIENT_ASE_AC_release_cfm_received,                 CLIENT_ASE_STATE_RELEASING },
  { CLIENT_ASE_EVENT_QOS_CONFIGURE,                  CLIENT_ASE_AC_send_config_qos_op,                   CLIENT_ASE_STATE_WAIT_QOS_CONFIGURE_CFM },
  { CLIENT_ASE_EVENT_RELEASE_IND,                    CLIENT_ASE_AC_release_ind_received,                 CLIENT_ASE_STATE_RELEASING },
  /* WAIT_QOS_CONFIGURE_CFM */
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_qos_configure_cfm_failed_received,    CLIENT_ASE_STATE_CODEC_CONFIGURED },
  { CLIENT_ASE_EVENT_QOS_CONFIGURE_CFM_RECEIVED,     CLIENT_ASE_AC_qos_configure_cfm_received,           CLIENT_ASE_STATE_QOS_CONFIGURED },
  /* QOS_CONFIGURED */
  { CLIENT_ASE_EVENT_CONFIGURE,                      CLIENT_ASE_AC_send_config_codec_op,                 CLIENT_ASE_STATE_RECONFIGURE_WAIT_CODEC_CONFIGURE_CFM },
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_qos_configure_cfm_failed_received,    CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_RELEASE,                        CLIENT_ASE_AC_send_release_op,                      CLIENT_ASE_STATE_WAIT_RELEASE_CFM },
  { CLIENT_ASE_EVENT_QOS_CONFIGURE,                  CLIENT_ASE_AC_send_config_qos_op,                   CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_RELEASE_IND,                    CLIENT_ASE_AC_release_ind_received,                 CLIENT_ASE_STATE_RELEASING },
  { CLIENT_ASE_EVENT_QOS_CONFIGURE_CFM_RECEIVED,     CLIENT_ASE_AC_qos_configure_cfm_received,           CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_ENABLE,                         CLIENT_ASE_AC_send_enable_op,                       CLIENT_ASE_STATE_WAIT_ENABLE_CFM },
  { CLIENT_ASE_EVENT_CIS_DISCONNECT,                 CLIENT_ASE_AC_send_cis_disconnect,                  CLIENT_ASE_STATE_WAIT_CIS_DISCONNECT_CFM },
  { CLIENT_ASE_EVENT_CIS_DISCONNECT_IND,             CLIENT_ASE_AC_cis_disconnect_ind_received,          CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_CIS_CONNECT,                    CLIENT_ASE_AC_cis_connect,                          CLIENT_ASE_STATE_WAIT_QOS_CIS_CONNECT_CFM },
  /* WAIT_CIS_DISCONNECT_CFM */
  { CLIENT_ASE_EVENT_CIS_DISCONNECT_CFM_RECEIVED,    CLIENT_ASE_AC_cis_disconnect_received,              CLIENT_ASE_STATE_RELEASING },
  /* WAIT_QOS_CIS_CONNECT_CFM */
  { CLIENT_ASE_EVENT_RELEASE_IND,                    CLIENT_ASE_AC_release_ind_received,                 CLIENT_ASE_STATE_RELEASING },
  { CLIENT_ASE_EVENT_CIS_ESTABLISHED,                CLIENT_ASE_AC_cis_connect_cfm_success_received,     CLIENT_ASE_STATE_QOS_CIS_CONNECTED },
  { CLIENT_ASE_EVENT_CIS_FAILED,                     CLIENT_ASE_AC_cis_connect_cfm_failed_received,      CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_DISABLE_IND,                    CLIENT_ASE_AC_disable_ind_received,                 CLIENT_ASE_STATE_DISABLING },
  /* RECONFIGURE_WAIT_CODEC_CONFIGURE_CFM */
  { CLIENT_ASE_EVENT_CODEC_CONFIGURE_CFM_RECEIVED,   CLIENT_ASE_AC_codec_configure_cfm_received,         CLIENT_ASE_STATE_CODEC_CONFIGURED },
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_codec_configure_cfm_failed_received,  CLIENT_ASE_STATE_QOS_CONFIGURED },
  /* WAIT_ENABLE_CFM */
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_enable_failed_received,               CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_ENABLE_CFM_RECEIVED,            CLIENT_ASE_AC_enable_cfm_received,                  CLIENT_ASE_STATE_ENABLING },
  /* QOS_CIS_CONNECTED */
  { CLIENT_ASE_EVENT_ENABLE,                         CLIENT_ASE_AC_send_enable_op,                       CLIENT_ASE_STATE_WAIT_QOS_CIS_ENABLE_CFM },
  { CLIENT_ASE_EVENT_CIS_DISCONNECT,                 CLIENT_ASE_AC_send_cis_disconnect,                  CLIENT_ASE_STATE_WAIT_QOS_CIS_DISCONNECT_CFM },
  { CLIENT_ASE_EVENT_CIS_DISCONNECT_IND,             CLIENT_ASE_AC_cis_disconnect_ind_received,          CLIENT_ASE_STATE_QOS_CONFIGURED },
  /* WAIT_QOS_CIS_ENABLE_CFM */
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_enable_failed_received,               CLIENT_ASE_STATE_QOS_CIS_CONNECTED },
  { CLIENT_ASE_EVENT_ENABLE_CFM_RECEIVED,            CLIENT_ASE_AC_enable_cfm_received,                  CLIENT_ASE_STATE_CIS_CONNECTED },
  /* WAIT_QOS_CIS_DISCONNECT_CFM */
  { CLIENT_ASE_EVENT_CIS_DISCONNECT_CFM_RECEIVED,    CLIENT_ASE_AC_cis_disconnect_received,              CLIENT_ASE_STATE_QOS_CONFIGURED },
  /* ENABLING */
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_update_metadata_failed_received,      CLIENT_ASE_STATE_ENABLING },
  { CLIENT_ASE_EVENT_RELEASE,                        CLIENT_ASE_AC_send_release_op,                      CLIENT_ASE_STATE_WAIT_RELEASE_CFM },
  { CLIENT_ASE_EVENT_RELEASE_IND,                    CLIENT_ASE_AC_release_ind_received,                 CLIENT_ASE_STATE_RELEASING },
  { CLIENT_ASE_EVENT_CIS_CONNECT,                    CLIENT_ASE_AC_cis_connect,                          CLIENT_ASE_STATE_WAIT_CIS_CONNECT_CFM },
  { CLIENT_ASE_EVENT_DISABLE_IND,                    CLIENT_ASE_AC_disable_ind_received,                 CLIENT_ASE_STATE_DISABLING },
  { CLIENT_ASE_EVENT_DISABLE,                        CLIENT_ASE_AC_send_disable_op,                      CLIENT_ASE_STATE_WAIT_DISABLE_CFM },
  { CLIENT_ASE_EVENT_UPDATE_METADATA,                CLIENT_ASE_AC_send_update_metadata_op,              CLIENT_ASE_STATE_ENABLING },
  { CLIENT_ASE_EVENT_METADATA_CFM_RECEIVED,          CLIENT_ASE_AC_update_metadata_cfm_received,         CLIENT_ASE_STATE_ENABLING },
  { CLIENT_ASE_EVENT_STOP_READY_RECEIVED,            CLIENT_ASE_AC_stop_ready_cfm_received,              CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_START_READY,                    CLIENT_ASE_AC_send_start_ready_op,                  CLIENT_ASE_STATE_WAIT_START_READY_CFM },
  { CLIENT_ASE_EVENT_START_READY_RECEIVED,           CLIENT_ASE_AC_start_ready_received,                 CLIENT_ASE_STATE_STREAMING },
  /* WAIT_CIS_CONNECT_CFM */
  { CLIENT_ASE_EVENT_RELEASE_IND,                    CLIENT_ASE_AC_release_ind_received,                 CLIENT_ASE_STATE_RELEASING },
  { CLIENT_ASE_EVENT_CIS_ESTABLISHED,                CLIENT_ASE_AC_cis_connect_cfm_success_received,     CLIENT_ASE_STATE_CIS_CONNECTED },
  { CLIENT_ASE_EVENT_CIS_FAILED,                     CLIENT_ASE_AC_cis_connect_cfm_failed_received,      CLIENT_ASE_STATE_ENABLING },
  { CLIENT_ASE_EVENT_DISABLE_IND,                    CLIENT_ASE_AC_disable_ind_received,                 CLIENT_ASE_STATE_DISABLING },
  /* CIS_CONNECTED */
  { CLIENT_ASE_EVENT_RELEASE,                        CLIENT_ASE_AC_send_release_op,                      CLIENT_ASE_STATE_WAIT_RELEASE_CFM },
  { CLIENT_ASE_EVENT_RELEASE_IND,                    CLIENT_ASE_AC_release_ind_received,                 CLIENT_ASE_STATE_RELEASING },
  { CLIENT_ASE_EVENT_CIS_DISCONNECT,                 CLIENT_ASE_AC_send_cis_disconnect,                  CLIENT_ASE_STATE_WAIT_CIS_DISCONNECT_CFM },
  { CLIENT_ASE_EVENT_CIS_DISCONNECT_IND,             CLIENT_ASE_AC_cis_disconnect_ind_received,          CLIENT_ASE_STATE_ENABLING },
  { CLIENT_ASE_EVENT_DISABLE_IND,                    CLIENT_ASE_AC_disable_ind_received,                 CLIENT_ASE_STATE_DISABLING },
  { CLIENT_ASE_EVENT_DISABLE,                        CLIENT_ASE_AC_send_disable_op,                      CLIENT_ASE_STATE_WAIT_DISABLE_CFM },
  { CLIENT_ASE_EVENT_UPDATE_METADATA,                CLIENT_ASE_AC_send_update_metadata_op,              CLIENT_ASE_STATE_CIS_CONNECTED },
  { CLIENT_ASE_EVENT_METADATA_CFM_RECEIVED,          CLIENT_ASE_AC_update_metadata_cfm_received,         CLIENT_ASE_STATE_CIS_CONNECTED },
  { CLIENT_ASE_EVENT_START_READY,                    CLIENT_ASE_AC_send_start_ready_op,                  CLIENT_ASE_STATE_WAIT_START_READY_CFM },
  { CLIENT_ASE_EVENT_START_READY_RECEIVED,           CLIENT_ASE_AC_start_ready_received,                 CLIENT_ASE_STATE_STREAMING },
  /* WAIT_START_READY_CFM */
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_start_ready_failed_received,          CLIENT_ASE_STATE_CIS_CONNECTED },
  { CLIENT_ASE_EVENT_RELEASE,                        CLIENT_ASE_AC_send_release_op,                      CLIENT_ASE_STATE_WAIT_RELEASE_CFM },
  { CLIENT_ASE_EVENT_DISABLE,                        CLIENT_ASE_AC_send_disable_op,                      CLIENT_ASE_STATE_WAIT_DISABLE_CFM },
  { CLIENT_ASE_EVENT_START_READY_RECEIVED,           CLIENT_ASE_AC_start_ready_received,                 CLIENT_ASE_STATE_STREAMING },
  /* STREAMING */
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_update_metadata_failed_received,      CLIENT_ASE_STATE_STREAMING },
  { CLIENT_ASE_EVENT_RELEASE,                        CLIENT_ASE_AC_send_release_op,                      CLIENT_ASE_STATE_WAIT_RELEASE_CFM },
  { CLIENT_ASE_EVENT_RELEASE_IND,                    CLIENT_ASE_AC_release_ind_received,                 CLIENT_ASE_STATE_RELEASING },
  { CLIENT_ASE_EVENT_DISABLE_IND,                    CLIENT_ASE_AC_disable_ind_received,                 CLIENT_ASE_STATE_DISABLING },
  { CLIENT_ASE_EVENT_DISABLE,                        CLIENT_ASE_AC_send_disable_op,                      CLIENT_ASE_STATE_WAIT_DISABLE_CFM },
  { CLIENT_ASE_EVENT_UPDATE_METADATA,                CLIENT_ASE_AC_send_update_metadata_op,              CLIENT_ASE_STATE_STREAMING },
  { CLIENT_ASE_EVENT_METADATA_CFM_RECEIVED,          CLIENT_ASE_AC_update_metadata_cfm_received,         CLIENT_ASE_STATE_STREAMING },
  { CLIENT_ASE_EVENT_STOP_READY_RECEIVED,            CLIENT_ASE_AC_stop_ready_cfm_received,              CLIENT_ASE_STATE_QOS_CONFIGURED },
  /* DISABLING */
  { CLIENT_ASE_EVENT_RELEASE,                        CLIENT_ASE_AC_send_release_op,                      CLIENT_ASE_STATE_WAIT_RELEASE_CFM },
  { CLIENT_ASE_EVENT_RELEASE_IND,                    CLIENT_ASE_AC_release_ind_received,                 CLIENT_ASE_STATE_RELEASING },
  { CLIENT_ASE_EVENT_STOP_READY_RECEIVED,            CLIENT_ASE_AC_stop_ready_cfm_received,              CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_STOP_READY_SEND,                CLIENT_ASE_AC_stop_ready_send_op,                   CLIENT_ASE_STATE_WAIT_STOP_READY_CFM },
  /* WAIT_STOP_READY_CFM */
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_stop_ready_cfm_failed_received,       CLIENT_ASE_STATE_DISABLING },
  { CLIENT_ASE_EVENT_STOP_READY_RECEIVED,            CLIENT_ASE_AC_stop_ready_cfm_received,              CLIENT_ASE_STATE_QOS_CONFIGURED },
  /* WAIT_DISABLE_CFM */
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_disable_cfm_failed_received,          CLIENT_ASE_STATE_STREAMING },
  { CLIENT_ASE_EVENT_STOP_READY_RECEIVED,            CLIENT_ASE_AC_stop_ready_cfm_received,              CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_DISABLE_CFM_RECEIVED,           CLIENT_ASE_AC_disable_cfm_received,                 CLIENT_ASE_STATE_DISABLING },
  /* WAIT_RELEASE_CFM */
  { CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED,            CLIENT_ASE_AC_release_cfm_failed_received,          CLIENT_ASE_STATE_QOS_CONFIGURED },
  { CLIENT_ASE_EVENT_RELEASE_CFM_RECEIVED,           CLIENT_ASE_AC_release_cfm_received,                 CLIENT_ASE_STATE_RELEASING },
  /* RELEASING */
  { CLIENT_ASE_EVENT_CIS_DISCONNECT,                 CLIENT_ASE_AC_send_cis_disconnect,                  CLIENT_ASE_STATE_WAIT_CIS_DISCONNECT_CFM },
  { CLIENT_ASE_EVENT_CIS_DISCONNECT_IND,             CLIENT_ASE_AC_cis_disconnect_ind_received,          CLIENT_ASE_STATE_RELEASING },
  { CLIENT_ASE_EVENT_RELEASED_IND_RECEIVED,          CLIENT_ASE_AC_released_ind_received,                CLIENT_ASE_STATE_CODEC_CONFIGURED },
  { CLIENT_ASE_EVENT_RELEASED_RECEIVED,              CLIENT_ASE_AC_released_ind_received,                CLIENT_ASE_STATE_IDLE }
};
/*}}__FSM_END_MARKER(BapClientAse, STATE_TABLES) */

/* State array declarations */
/*{{__FSM_MARKER(BapClientAse, STATE_ARRAY) */
static const FSM_SPARSE16_STATES_T bapClientAseStates[CLIENT_ASE_STATE_ST_MAX+1] = {
    { &bapClientAseTransitions[0] },
    { &bapClientAseTransitions[2] },
    { &bapClientAseTransitions[4] },
    { &bapClientAseTransitions[12] },
    { &bapClientAseTransitions[14] },
    { &bapClientAseTransitions[24] },
    { &bapClientAseTransitions[25] },
    { &bapClientAseTransitions[29] },
    { &bapClientAseTransitions[31] },
    { &bapClientAseTransitions[33] },
    { &bapClientAseTransitions[36] },
    { &bapClientAseTransitions[38] },
    { &bapClientAseTransitions[39] },
    { &bapClientAseTransitions[50] },
    { &bapClientAseTransitions[54] },
    { &bapClientAseTransitions[64] },
    { &bapClientAseTransitions[68] },
    { &bapClientAseTransitions[76] },
    { &bapClientAseTransitions[80] },
    { &bapClientAseTransitions[82] },
    { &bapClientAseTransitions[85] },
    { &bapClientAseTransitions[87] },
    { &bapClientAseTransitions[91] },
};
/*}}__FSM_END_MARKER(BapClientAse, STATE_ARRAY) */

/* FSM logging definitions */
#if defined(FSM_DEBUG_NAMES)
/*{{__FSM_MARKER(BapClientAse, FSM_LOGGING) */
/* State names */
const char *const bapClientAseStateNames[] =
{
    "CLIENT_ASE_STATE_IDLE",
    "CLIENT_ASE_STATE_WAIT_CODEC_CONFIGURE_CFM",
    "CLIENT_ASE_STATE_CODEC_CONFIGURED",
    "CLIENT_ASE_STATE_WAIT_QOS_CONFIGURE_CFM",
    "CLIENT_ASE_STATE_QOS_CONFIGURED",
    "CLIENT_ASE_STATE_WAIT_CIS_DISCONNECT_CFM",
    "CLIENT_ASE_STATE_WAIT_QOS_CIS_CONNECT_CFM",
    "CLIENT_ASE_STATE_RECONFIGURE_WAIT_CODEC_CONFIGURE_CFM",
    "CLIENT_ASE_STATE_WAIT_ENABLE_CFM",
    "CLIENT_ASE_STATE_QOS_CIS_CONNECTED",
    "CLIENT_ASE_STATE_WAIT_QOS_CIS_ENABLE_CFM",
    "CLIENT_ASE_STATE_WAIT_QOS_CIS_DISCONNECT_CFM",
    "CLIENT_ASE_STATE_ENABLING",
    "CLIENT_ASE_STATE_WAIT_CIS_CONNECT_CFM",
    "CLIENT_ASE_STATE_CIS_CONNECTED",
    "CLIENT_ASE_STATE_WAIT_START_READY_CFM",
    "CLIENT_ASE_STATE_STREAMING",
    "CLIENT_ASE_STATE_DISABLING",
    "CLIENT_ASE_STATE_WAIT_STOP_READY_CFM",
    "CLIENT_ASE_STATE_WAIT_DISABLE_CFM",
    "CLIENT_ASE_STATE_WAIT_RELEASE_CFM",
    "CLIENT_ASE_STATE_RELEASING",
};
#define BAP_CLIENT_ASE_STATE_NAMES_PTR (bapClientAseStateNames)

/* Event names */
const char *const bapClientAseEventNames[] =
{
    "CLIENT_ASE_EVENT_CONFIGURE",
    "CLIENT_ASE_EVENT_CONFIGURE_IND",
    "CLIENT_ASE_EVENT_CODEC_CONFIGURE_CFM_RECEIVED",
    "CLIENT_ASE_EVENT_FAILED_CFM_RECEIVED",
    "CLIENT_ASE_EVENT_RELEASE",
    "CLIENT_ASE_EVENT_RELEASE_CFM_RECEIVED",
    "CLIENT_ASE_EVENT_QOS_CONFIGURE",
    "CLIENT_ASE_EVENT_RELEASE_IND",
    "CLIENT_ASE_EVENT_QOS_CONFIGURE_CFM_RECEIVED",
    "CLIENT_ASE_EVENT_ENABLE",
    "CLIENT_ASE_EVENT_CIS_DISCONNECT",
    "CLIENT_ASE_EVENT_CIS_DISCONNECT_CFM_RECEIVED",
    "CLIENT_ASE_EVENT_CIS_DISCONNECT_IND",
    "CLIENT_ASE_EVENT_CIS_CONNECT",
    "CLIENT_ASE_EVENT_CIS_ESTABLISHED",
    "CLIENT_ASE_EVENT_CIS_FAILED",
    "CLIENT_ASE_EVENT_DISABLE_IND",
    "CLIENT_ASE_EVENT_ENABLE_CFM_RECEIVED",
    "CLIENT_ASE_EVENT_DISABLE",
    "CLIENT_ASE_EVENT_UPDATE_METADATA",
    "CLIENT_ASE_EVENT_METADATA_CFM_RECEIVED",
    "CLIENT_ASE_EVENT_STOP_READY_RECEIVED",
    "CLIENT_ASE_EVENT_START_READY",
    "CLIENT_ASE_EVENT_START_READY_RECEIVED",
    "CLIENT_ASE_EVENT_STOP_READY_SEND",
    "CLIENT_ASE_EVENT_DISABLE_CFM_RECEIVED",
    "CLIENT_ASE_EVENT_RELEASED_IND_RECEIVED",
    "CLIENT_ASE_EVENT_RELEASED_RECEIVED",
};
#define BAP_CLIENT_ASE_EVENT_NAMES_PTR (bapClientAseEventNames)

/*}}__FSM_END_MARKER(BapClientAse, FSM_LOGGING) */
#else
#define BAP_CLIENT_ASE_STATE_NAMES_PTR (NULL)
#define BAP_CLIENT_ASE_EVENT_NAMES_PTR (NULL)
#endif

/* State machine definition */
/*{{__FSM_MARKER(BapClientAse, FSM_DEFINITION) */
const FSM_SPARSE16_DEFINITION_T bapClientAseFsm =
{
    (const FSM_SPARSE16_STATES_T *) bapClientAseStates,
    bapClientAseActionFns
    FSM_LOG_INFO("BapClientAse",
    BAP_CLIENT_ASE_STATE_NAMES_PTR,
    BAP_CLIENT_ASE_EVENT_NAMES_PTR)
};
/*}}__FSM_END_MARKER(BapClientAse, FSM_DEFINITION) */

#endif /* INSTALL_LEA_UNICAST_CLIENT */
