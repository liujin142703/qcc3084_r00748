/******************************************************************************
 Copyright (c) 2021-2023 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #56 $
******************************************************************************/

#ifndef CAP_CLIENT_MSG_HANDLER_H
#define CAP_CLIENT_MSG_HANDLER_H

#include "cap_client_private.h"
#endif
