#ifndef CSR_BT_GATT_DM_SEF_H__
#define CSR_BT_GATT_DM_SEF_H__
/******************************************************************************
 Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#include "csr_synergy.h"
#include "csr_bt_gatt_main.h"

#ifdef __cplusplus
extern "C" {
#endif

void CsrBtGattDispatchDm(GattMainInst *inst);

#ifdef __cplusplus
}
#endif

#endif /* CSR_BT_GATT_DM_SEF_H__ */
