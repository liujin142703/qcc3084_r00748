#ifndef CSR_BT_AV_SIGNAL_ENUM_DBG_H__
#define CSR_BT_AV_SIGNAL_ENUM_DBG_H__
/*******************************************************************************
 * File: csr_bt_av_signal_enum_dbg.h
 * Copyright (c) 2022-2024 Qualcomm Technologies International, Ltd.
 * All Rights Reserved.
 * Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 *
 * DO NOT EDIT
 * Generated from csr_bt_av_signal.h by enum_generator.py 
 ******************************************************************************/
#include "csr_bt_av_signal.h"

enum AvdtpSignalId{
    _AVDTP_DISCOVER_SID = AVDTP_DISCOVER_SID,
    _AVDTP_GET_CAPABILITIES_SID = AVDTP_GET_CAPABILITIES_SID,
    _AVDTP_SET_CONFIGURATION_SID = AVDTP_SET_CONFIGURATION_SID,
    _AVDTP_GET_CONFIGURATION_SID = AVDTP_GET_CONFIGURATION_SID,
    _AVDTP_RECONFIGURE_SID = AVDTP_RECONFIGURE_SID,
    _AVDTP_OPEN_SID = AVDTP_OPEN_SID,
    _AVDTP_START_SID = AVDTP_START_SID,
    _AVDTP_CLOSE_SID = AVDTP_CLOSE_SID,
    _AVDTP_SUSPEND_SID = AVDTP_SUSPEND_SID,
    _AVDTP_ABORT_SID = AVDTP_ABORT_SID,
    _AVDTP_SECURITY_CONTROL_SID = AVDTP_SECURITY_CONTROL_SID,
    _AVDTP_GET_ALL_CAPABILITIES_SID = AVDTP_GET_ALL_CAPABILITIES_SID,
    _AVDTP_DELAYREPORT_SID = AVDTP_DELAYREPORT_SID,
    _SIGNAL_ID_MASK = SIGNAL_ID_MASK,
};

#endif /*CSR_BT_AV_SIGNAL_ENUM_DBG_H__*/
