/*!
        Copyright (C) 2010 - 2017 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.

\file

\brief  Private header for tbdaddr
*/

#ifndef __BREDRLE_ADDR_PRIVATE_H__
#define __BREDRLE_ADDR_PRIVATE_H__

#include "tbdaddr.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif  /* __BREDRLE_ADDR_PRIVATE_H__ */
