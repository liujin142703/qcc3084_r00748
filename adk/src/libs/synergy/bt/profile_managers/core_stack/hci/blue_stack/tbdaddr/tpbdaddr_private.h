/*!
        Copyright (C) 2015 - 2017 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.

\file   tbdaddr_private.h

\brief  Private header for tbdaddr
*/

#ifndef __BREDRLE_TP_ADDR_PRIVATE_H__
#define __BREDRLE_TP_ADDR_PRIVATE_H__

#include "tpbdaddr.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif  /* __BREDRLE_TP_ADDR_PRIVATE_H__ */
