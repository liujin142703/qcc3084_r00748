#ifndef QBL_FAULT_H__
#define QBL_FAULT_H__
/******************************************************************************
 Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#define FAULT_BLUESTACK_DM_UNEXPECTED_CMD_CMPLT_EVENT 0x2004

#define fault(x)

#ifdef __cplusplus
}
#endif

#endif
