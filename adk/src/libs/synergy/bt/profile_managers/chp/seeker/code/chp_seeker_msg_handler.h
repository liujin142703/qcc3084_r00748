/******************************************************************************
 Copyright (c) 2021 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 
 REVISION:      $Revision: #1 $
******************************************************************************/

#ifndef CHP_SEEKER_MSG_HANDLER_H_
#define CHP_SEEKER_MSG_HANDLER_H_


/***************************************************************************
NAME
    chpSeekerMsgHandler

DESCRIPTION
    Handler for external messages sent to the library in the client role.
*/
void chpSeekerMsgHandler(Task task, MessageId id, Message msg);

#endif
