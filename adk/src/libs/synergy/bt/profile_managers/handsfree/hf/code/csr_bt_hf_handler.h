#ifndef CSR_BT_HF_HANDLER_H__
#define CSR_BT_HF_HANDLER_H__
/******************************************************************************
 Copyright (c) 2008-2017 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #1 $
******************************************************************************/

#include "csr_synergy.h"

#include "csr_bt_hf_main.h"
#include "csr_bt_cm_prim.h"
#include "csr_bt_hf_util.h"

#ifdef __cplusplus
extern "C" {
#endif

void CsrBtHfpHandler(HfMainInstanceData_t * instData);
#ifdef __cplusplus
}
#endif

#endif
