/*******************************************************************************

Copyright (C) 2023 Qualcomm Technologies International, Ltd.
All Rights Reserved.
Qualcomm Technologies International, Ltd. Confidential and Proprietary.

*******************************************************************************/

#ifndef GATT_HIDS_SERVER_MSG_HANDLER_H_
#define GATT_HIDS_SERVER_MSG_HANDLER_H_

#include "gatt_hids_server_private.h"

void hidsServerMsgHandler(void* task, MsgId id, Msg msg);
#endif

