/*******************************************************************************
Copyright (c) 2021 Qualcomm Technologies International, Ltd.
 %%version
*******************************************************************************/
/*! \file
    Definitions of TDS UUIDs for the GATT TDS Client.
 */

#ifndef __GATT_TDS_CLIENT_UUIDS_H__
#define __GATT_TDS_CLIENT_UUIDS_H__

/* UUID Values as per latest spec */
#define UUID_TRANSPORT_DISCOVERY_CONTROL_POINT                  0x2ABC
#define UUID_TRANSPORT_DISCOVERY_BREDR_HANDOVER_DATA            0x2B38
#define UUID_TRANSPORT_DISCOVERY_SIG_DATA                       0x2B39
#define UUID_TRANSPORT_DISCOVERY_BREDR_TRANSPORT_BLOCK_DATA     0x290F

#endif /* __GATT_TDS_CLIENT_UUIDS_H__ */
