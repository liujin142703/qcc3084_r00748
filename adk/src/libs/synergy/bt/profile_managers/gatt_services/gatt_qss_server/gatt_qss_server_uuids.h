/******************************************************************************
 Copyright (c) 2023 Qualcomm Technologies International, Ltd.
 All Rights Reserved.
 Qualcomm Technologies International, Ltd. Confidential and Proprietary.

 REVISION:      $Revision: #2 $
******************************************************************************/

#ifndef __GATT_QSS_SERVER_UUIDS_H__
#define __GATT_QSS_SERVER_UUIDS_H__

#define UUID_QUALCOMM_SNAPDRAGON_SOUND_SERVICE     0xaf377309c0344bf0817ad9c3af214a8f
#define UUID_QUALCOMM_SNAPDRAGON_SOUND_SUPPORT     0x66ee066384954c60a81ab587c2518d54
#define UUID_USER_DESCRIPTION                      0x2901
#define UUID_LOSSLESS_AUDIO                        0x71543f8dafd8472fae43953a0b82f9e4

#endif /* __GATT_QSS_SERVER_UUIDS_H__ */
