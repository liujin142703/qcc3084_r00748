#ifndef VCP_ENUM_DBG_H__
#define VCP_ENUM_DBG_H__
/*******************************************************************************
 * File: vcp_enum_dbg.h
 * Copyright (c) 2022-2024 Qualcomm Technologies International, Ltd.
 * All Rights Reserved.
 * Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 *
 * DO NOT EDIT
 * Generated from vcp.h by enum_generator.py 
 ******************************************************************************/
#include "vcp.h"

enum VcpStatus{
    _VCP_STATUS_SUCCESS = VCP_STATUS_SUCCESS,
    _VCP_STATUS_IN_PROGRESS = VCP_STATUS_IN_PROGRESS,
    _VCP_STATUS_INVALID_PARAMETER = VCP_STATUS_INVALID_PARAMETER,
    _VCP_STATUS_DISCOVERY_ERR = VCP_STATUS_DISCOVERY_ERR,
    _VCP_STATUS_FAILED = VCP_STATUS_FAILED,
    _VCP_STATUS_VOL_PERSISTED = VCP_STATUS_VOL_PERSISTED,
};

enum VcpMessageId{
    _VCP_INIT_CFM = VCP_INIT_CFM,
    _VCP_DESTROY_CFM = VCP_DESTROY_CFM,
    _VCP_VCS_TERMINATE_CFM = VCP_VCS_TERMINATE_CFM,
    _VCP_VOCS_TERMINATE_CFM = VCP_VOCS_TERMINATE_CFM,
    _VCP_AICS_TERMINATE_CFM = VCP_AICS_TERMINATE_CFM,
    _VCP_VOLUME_STATE_SET_NTF_CFM = VCP_VOLUME_STATE_SET_NTF_CFM,
    _VCP_VOLUME_FLAG_SET_NTF_CFM = VCP_VOLUME_FLAG_SET_NTF_CFM,
    _VCP_READ_VOLUME_STATE_CCC_CFM = VCP_READ_VOLUME_STATE_CCC_CFM,
    _VCP_READ_VOLUME_FLAG_CCC_CFM = VCP_READ_VOLUME_FLAG_CCC_CFM,
    _VCP_VOLUME_STATE_IND = VCP_VOLUME_STATE_IND,
    _VCP_VOLUME_FLAG_IND = VCP_VOLUME_FLAG_IND,
    _VCP_READ_VOLUME_STATE_CFM = VCP_READ_VOLUME_STATE_CFM,
    _VCP_READ_VOLUME_FLAG_CFM = VCP_READ_VOLUME_FLAG_CFM,
    _VCP_REL_VOL_DOWN_CFM = VCP_REL_VOL_DOWN_CFM,
    _VCP_REL_VOL_UP_CFM = VCP_REL_VOL_UP_CFM,
    _VCP_UNMUTE_REL_VOL_DOWN_CFM = VCP_UNMUTE_REL_VOL_DOWN_CFM,
    _VCP_UNMUTE_REL_VOL_UP_CFM = VCP_UNMUTE_REL_VOL_UP_CFM,
    _VCP_ABS_VOL_CFM = VCP_ABS_VOL_CFM,
    _VCP_UNMUTE_CFM = VCP_UNMUTE_CFM,
    _VCP_MUTE_CFM = VCP_MUTE_CFM,
    _VCP_OFFSET_STATE_SET_NTF_CFM = VCP_OFFSET_STATE_SET_NTF_CFM,
    _VCP_AUDIO_LOCATION_SET_NTF_CFM = VCP_AUDIO_LOCATION_SET_NTF_CFM,
    _VCP_AUDIO_OUTPUT_DESC_SET_NTF_CFM = VCP_AUDIO_OUTPUT_DESC_SET_NTF_CFM,
    _VCP_READ_OFFSET_STATE_CCC_CFM = VCP_READ_OFFSET_STATE_CCC_CFM,
    _VCP_READ_AUDIO_LOCATION_CCC_CFM = VCP_READ_AUDIO_LOCATION_CCC_CFM,
    _VCP_READ_AUDIO_OUTPUT_DESC_CCC_CFM = VCP_READ_AUDIO_OUTPUT_DESC_CCC_CFM,
    _VCP_READ_OFFSET_STATE_CFM = VCP_READ_OFFSET_STATE_CFM,
    _VCP_READ_AUDIO_LOCATION_CFM = VCP_READ_AUDIO_LOCATION_CFM,
    _VCP_READ_AUDIO_OUTPUT_DESC_CFM = VCP_READ_AUDIO_OUTPUT_DESC_CFM,
    _VCP_SET_VOLUME_OFFSET_CFM = VCP_SET_VOLUME_OFFSET_CFM,
    _VCP_SET_AUDIO_LOCATION_CFM = VCP_SET_AUDIO_LOCATION_CFM,
    _VCP_SET_AUDIO_OUTPUT_DESC_CFM = VCP_SET_AUDIO_OUTPUT_DESC_CFM,
    _VCP_OFFSET_STATE_IND = VCP_OFFSET_STATE_IND,
    _VCP_AUDIO_LOCATION_IND = VCP_AUDIO_LOCATION_IND,
    _VCP_AUDIO_OUTPUT_DESC_IND = VCP_AUDIO_OUTPUT_DESC_IND,
    _VCP_INPUT_STATE_SET_NTF_CFM = VCP_INPUT_STATE_SET_NTF_CFM,
    _VCP_INPUT_STATUS_SET_NTF_CFM = VCP_INPUT_STATUS_SET_NTF_CFM,
    _VCP_AUDIO_INPUT_DESC_SET_NTF_CFM = VCP_AUDIO_INPUT_DESC_SET_NTF_CFM,
    _VCP_READ_INPUT_STATE_CCC_CFM = VCP_READ_INPUT_STATE_CCC_CFM,
    _VCP_READ_INPUT_STATUS_CCC_CFM = VCP_READ_INPUT_STATUS_CCC_CFM,
    _VCP_READ_AUDIO_INPUT_DESC_CCC_CFM = VCP_READ_AUDIO_INPUT_DESC_CCC_CFM,
    _VCP_READ_INPUT_STATE_CFM = VCP_READ_INPUT_STATE_CFM,
    _VCP_READ_GAIN_SET_PROPERTIES_CFM = VCP_READ_GAIN_SET_PROPERTIES_CFM,
    _VCP_READ_INPUT_TYPE_CFM = VCP_READ_INPUT_TYPE_CFM,
    _VCP_READ_INPUT_STATUS_CFM = VCP_READ_INPUT_STATUS_CFM,
    _VCP_READ_AUDIO_INPUT_DESC_CFM = VCP_READ_AUDIO_INPUT_DESC_CFM,
    _VCP_SET_GAIN_SETTING_CFM = VCP_SET_GAIN_SETTING_CFM,
    _VCP_AICS_UNMUTE_CFM = VCP_AICS_UNMUTE_CFM,
    _VCP_AICS_MUTE_CFM = VCP_AICS_MUTE_CFM,
    _VCP_AICS_SET_MANUAL_GAIN_MODE_CFM = VCP_AICS_SET_MANUAL_GAIN_MODE_CFM,
    _VCP_AICS_SET_AUTOMATIC_GAIN_MODE_CFM = VCP_AICS_SET_AUTOMATIC_GAIN_MODE_CFM,
    _VCP_SET_AUDIO_INPUT_DESC_CFM = VCP_SET_AUDIO_INPUT_DESC_CFM,
    _VCP_INPUT_STATE_IND = VCP_INPUT_STATE_IND,
    _VCP_INPUT_STATUS_IND = VCP_INPUT_STATUS_IND,
    _VCP_AUDIO_INPUT_DESC_IND = VCP_AUDIO_INPUT_DESC_IND,
    _VCP_SET_INITIAL_VOL_CFM = VCP_SET_INITIAL_VOL_CFM,
    _VCP_MESSAGE_TOP = VCP_MESSAGE_TOP,
};

#endif /*VCP_ENUM_DBG_H__*/
