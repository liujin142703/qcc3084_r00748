#ifndef CSR_BT_HIDD_PRIM_ENUM_DBG_H__
#define CSR_BT_HIDD_PRIM_ENUM_DBG_H__
/*******************************************************************************
 * File: csr_bt_hidd_prim_enum_dbg.h
 * Copyright (c) 2022-2024 Qualcomm Technologies International, Ltd.
 * All Rights Reserved.
 * Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 *
 * DO NOT EDIT
 * Generated from csr_bt_hidd_prim.h by enum_generator.py 
 ******************************************************************************/
#include "csr_bt_hidd_prim.h"

enum CsrBtHiddCtrlType{
    _CSR_BT_HIDD_NOP = CSR_BT_HIDD_NOP,
    _CSR_BT_HIDD_HARD_RESET = CSR_BT_HIDD_HARD_RESET,
    _CSR_BT_HIDD_SOFT_RESET = CSR_BT_HIDD_SOFT_RESET,
    _CSR_BT_HIDD_SUSPEND = CSR_BT_HIDD_SUSPEND,
    _CSR_BT_HIDD_EXIT_SUSPEND = CSR_BT_HIDD_EXIT_SUSPEND,
    _CSR_BT_HIDD_VC_UNPLUG = CSR_BT_HIDD_VC_UNPLUG,
};

enum CsrBtHiddPowerModeType{
    _CSR_BT_HIDD_ACTIVE_MODE = CSR_BT_HIDD_ACTIVE_MODE,
    _CSR_BT_HIDD_SNIFF_MODE = CSR_BT_HIDD_SNIFF_MODE,
    _CSR_BT_HIDD_DISCONNECT_MODE = CSR_BT_HIDD_DISCONNECT_MODE,
};

enum CsrBtHiddProtocolType{
    _CSR_BT_HIDD_BOOT_PROTOCOL = CSR_BT_HIDD_BOOT_PROTOCOL,
    _CSR_BT_HIDD_REPORT_PROTOCOL = CSR_BT_HIDD_REPORT_PROTOCOL,
};

enum CsrBtHiddStatusType{
    _CSR_BT_HIDD_DISCONNECTED = CSR_BT_HIDD_DISCONNECTED,
    _CSR_BT_HIDD_CONNECTED = CSR_BT_HIDD_CONNECTED,
    _CSR_BT_HIDD_CONNECT_FAILED = CSR_BT_HIDD_CONNECT_FAILED,
    _CSR_BT_HIDD_UNREGISTER_FAILED = CSR_BT_HIDD_UNREGISTER_FAILED,
    _CSR_BT_HIDD_RECONNECTING = CSR_BT_HIDD_RECONNECTING,
};

enum CsrBtHiddTransactionType{
    _CSR_BT_HIDD_HANDSHAKE = CSR_BT_HIDD_HANDSHAKE,
    _CSR_BT_HIDD_CONTROL = CSR_BT_HIDD_CONTROL,
    _CSR_BT_HIDD_GET_REPORT = CSR_BT_HIDD_GET_REPORT,
    _CSR_BT_HIDD_SET_REPORT = CSR_BT_HIDD_SET_REPORT,
    _CSR_BT_HIDD_GET_PROTOCOL = CSR_BT_HIDD_GET_PROTOCOL,
    _CSR_BT_HIDD_SET_PROTOCOL = CSR_BT_HIDD_SET_PROTOCOL,
    _CSR_BT_HIDD_GET_IDLE = CSR_BT_HIDD_GET_IDLE,
    _CSR_BT_HIDD_SET_IDLE = CSR_BT_HIDD_SET_IDLE,
    _CSR_BT_HIDD_DATA = CSR_BT_HIDD_DATA,
    _CSR_BT_HIDD_DATC = CSR_BT_HIDD_DATC,
};

enum CsrBtHiddReportType{
    _CSR_BT_HIDD_OTHER = CSR_BT_HIDD_OTHER,
    _CSR_BT_HIDD_INPUT_REPORT = CSR_BT_HIDD_INPUT_REPORT,
    _CSR_BT_HIDD_OUTPUT_REPORT = CSR_BT_HIDD_OUTPUT_REPORT,
    _CSR_BT_HIDD_FEATURE_REPORT = CSR_BT_HIDD_FEATURE_REPORT,
};

enum CsrBtHiddHandshakeType{
    _CSR_BT_HIDD_SUCCESS = CSR_BT_HIDD_SUCCESS,
    _CSR_BT_HIDD_NOT_READY = CSR_BT_HIDD_NOT_READY,
    _CSR_BT_HIDD_ERR_INVALID_REPORT_ID = CSR_BT_HIDD_ERR_INVALID_REPORT_ID,
    _CSR_BT_HIDD_ERR_UNSUPPORTED = CSR_BT_HIDD_ERR_UNSUPPORTED,
    _CSR_BT_HIDD_ERR_INVALID_PARAMETER = CSR_BT_HIDD_ERR_INVALID_PARAMETER,
    _CSR_BT_HIDD_ERR_UNKNOWN = CSR_BT_HIDD_ERR_UNKNOWN,
    _CSR_BT_HIDD_ERR_FATAL = CSR_BT_HIDD_ERR_FATAL,
};

enum CsrBtHiddResultCode{
    _CSR_BT_RESULT_CODE_HIDD_SUCCESS = CSR_BT_RESULT_CODE_HIDD_SUCCESS,
    _CSR_BT_RESULT_CODE_HIDD_UNACCEPTABLE_PARAMETER = CSR_BT_RESULT_CODE_HIDD_UNACCEPTABLE_PARAMETER,
    _CSR_BT_RESULT_CODE_HIDD_SDS_REGISTER_FAILED = CSR_BT_RESULT_CODE_HIDD_SDS_REGISTER_FAILED,
    _CSR_BT_RESULT_CODE_HIDD_SDS_UNREGISTER_FAILED = CSR_BT_RESULT_CODE_HIDD_SDS_UNREGISTER_FAILED,
    _CSR_BT_RESULT_CODE_HIDD_TIMEOUT = CSR_BT_RESULT_CODE_HIDD_TIMEOUT,
    _CSR_BT_RESULT_CODE_HIDD_CONNECTION_TERM_BY_REMOTE_HOST = CSR_BT_RESULT_CODE_HIDD_CONNECTION_TERM_BY_REMOTE_HOST,
    _CSR_BT_RESULT_CODE_HIDD_CONNECTION_TERM_BY_LOCAL_HOST = CSR_BT_RESULT_CODE_HIDD_CONNECTION_TERM_BY_LOCAL_HOST,
};

enum CsrBtHiddPrim{
    _CSR_BT_HIDD_ACTIVATE_REQ = CSR_BT_HIDD_ACTIVATE_REQ,
    _CSR_BT_HIDD_DEACTIVATE_REQ = CSR_BT_HIDD_DEACTIVATE_REQ,
    _CSR_BT_HIDD_CONTROL_RES = CSR_BT_HIDD_CONTROL_RES,
    _CSR_BT_HIDD_DATA_REQ = CSR_BT_HIDD_DATA_REQ,
    _CSR_BT_HIDD_UNPLUG_REQ = CSR_BT_HIDD_UNPLUG_REQ,
    _CSR_BT_HIDD_MODE_CHANGE_REQ = CSR_BT_HIDD_MODE_CHANGE_REQ,
    _CSR_BT_HIDD_SECURITY_IN_REQ = CSR_BT_HIDD_SECURITY_IN_REQ,
    _CSR_BT_HIDD_SECURITY_OUT_REQ = CSR_BT_HIDD_SECURITY_OUT_REQ,
    _HIDD_CONNECT_REQ = HIDD_CONNECT_REQ,
    _HIDD_DISCONNECT_REQ = HIDD_DISCONNECT_REQ,
    _CSR_BT_HIDD_RESTORE_IND = CSR_BT_HIDD_RESTORE_IND,
    _CSR_BT_HIDD_REACTIVATE_IND = CSR_BT_HIDD_REACTIVATE_IND,
    _CSR_BT_HIDD_ACTIVATE_CFM = CSR_BT_HIDD_ACTIVATE_CFM,
    _CSR_BT_HIDD_DEACTIVATE_CFM = CSR_BT_HIDD_DEACTIVATE_CFM,
    _CSR_BT_HIDD_STATUS_IND = CSR_BT_HIDD_STATUS_IND,
    _CSR_BT_HIDD_CONTROL_IND = CSR_BT_HIDD_CONTROL_IND,
    _CSR_BT_HIDD_DATA_CFM = CSR_BT_HIDD_DATA_CFM,
    _CSR_BT_HIDD_DATA_IND = CSR_BT_HIDD_DATA_IND,
    _CSR_BT_HIDD_UNPLUG_CFM = CSR_BT_HIDD_UNPLUG_CFM,
    _CSR_BT_HIDD_UNPLUG_IND = CSR_BT_HIDD_UNPLUG_IND,
    _CSR_BT_HIDD_MODE_CHANGE_IND = CSR_BT_HIDD_MODE_CHANGE_IND,
    _CSR_BT_HIDD_SECURITY_IN_CFM = CSR_BT_HIDD_SECURITY_IN_CFM,
    _CSR_BT_HIDD_SECURITY_OUT_CFM = CSR_BT_HIDD_SECURITY_OUT_CFM,
};

#endif /*CSR_BT_HIDD_PRIM_ENUM_DBG_H__*/
