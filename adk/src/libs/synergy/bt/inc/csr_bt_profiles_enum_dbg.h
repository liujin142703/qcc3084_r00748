#ifndef CSR_BT_PROFILES_ENUM_DBG_H__
#define CSR_BT_PROFILES_ENUM_DBG_H__
/*******************************************************************************
 * File: csr_bt_profiles_enum_dbg.h
 * Copyright (c) 2022-2024 Qualcomm Technologies International, Ltd.
 * All Rights Reserved.
 * Qualcomm Technologies International, Ltd. Confidential and Proprietary.
 *
 * DO NOT EDIT
 * Generated from csr_bt_profiles.h by enum_generator.py 
 ******************************************************************************/
#include "csr_bt_profiles.h"

enum CsrBtUuidSize{
    _CSR_BT_UUID16_SIZE = CSR_BT_UUID16_SIZE,
    _CSR_BT_UUID32_SIZE = CSR_BT_UUID32_SIZE,
    _CSR_BT_UUID128_SIZE = CSR_BT_UUID128_SIZE,
};

enum uuid16_t{
    _CSR_BT_PROTOCOL_AVCTP_UUID = CSR_BT_PROTOCOL_AVCTP_UUID,
    _CSR_BT_PROTOCOL_AVDTP_UUID = CSR_BT_PROTOCOL_AVDTP_UUID,
    _CSR_BT_PROTOCOL_MCAP_CONTROL_UUID = CSR_BT_PROTOCOL_MCAP_CONTROL_UUID,
    _CSR_BT_PROTOCOL_MCAP_DATA_UUID = CSR_BT_PROTOCOL_MCAP_DATA_UUID,
    _CSR_BT_OBEX_SYNCML_TRANSFER_UUID = CSR_BT_OBEX_SYNCML_TRANSFER_UUID,
    _CSR_BT_SPP_PROFILE_UUID = CSR_BT_SPP_PROFILE_UUID,
    _CSR_BT_LAP_PROFILE_UUID = CSR_BT_LAP_PROFILE_UUID,
    _CSR_BT_DUN_PROFILE_UUID = CSR_BT_DUN_PROFILE_UUID,
    _CSR_BT_OBEX_IR_MC_SYNC_SERVICE_UUID = CSR_BT_OBEX_IR_MC_SYNC_SERVICE_UUID,
    _CSR_BT_OBEX_OBJECT_PUSH_SERVICE_UUID = CSR_BT_OBEX_OBJECT_PUSH_SERVICE_UUID,
    _CSR_BT_OBEX_FILE_TRANSFER_UUID = CSR_BT_OBEX_FILE_TRANSFER_UUID,
    _CSR_BT_HS_PROFILE_UUID = CSR_BT_HS_PROFILE_UUID,
    _CSR_BT_CTP_PROFILE_UUID = CSR_BT_CTP_PROFILE_UUID,
    _CSR_BT_AUDIO_SOURCE_UUID = CSR_BT_AUDIO_SOURCE_UUID,
    _CSR_BT_AUDIO_SINK_UUID = CSR_BT_AUDIO_SINK_UUID,
    _CSR_BT_AV_REMOTE_CONTROL_TARGET_UUID = CSR_BT_AV_REMOTE_CONTROL_TARGET_UUID,
    _CSR_BT_ADVANCED_AUDIO_PROFILE_UUID = CSR_BT_ADVANCED_AUDIO_PROFILE_UUID,
    _CSR_BT_AV_REMOTE_CONTROL_UUID = CSR_BT_AV_REMOTE_CONTROL_UUID,
    _CSR_BT_AV_REMOTE_CONTROL_CONTROLLER_UUID = CSR_BT_AV_REMOTE_CONTROL_CONTROLLER_UUID,
    _CSR_BT_ICP_PROFILE_UUID = CSR_BT_ICP_PROFILE_UUID,
    _CSR_BT_FAX_PROFILE_UUID = CSR_BT_FAX_PROFILE_UUID,
    _CSR_BT_HEADSET_AG_SERVICE_UUID = CSR_BT_HEADSET_AG_SERVICE_UUID,
    _CSR_BT_PAN_PANU_PROFILE_UUID = CSR_BT_PAN_PANU_PROFILE_UUID,
    _CSR_BT_PAN_NAP_PROFILE_UUID = CSR_BT_PAN_NAP_PROFILE_UUID,
    _CSR_BT_PAN_GN_PROFILE_UUID = CSR_BT_PAN_GN_PROFILE_UUID,
    _CSR_BT_DIRECT_PRINTING_UUID = CSR_BT_DIRECT_PRINTING_UUID,
    _CSR_BT_REFERENCE_PRINTING_UUID = CSR_BT_REFERENCE_PRINTING_UUID,
    _CSR_BT_OBEX_IMAGING_UUID = CSR_BT_OBEX_IMAGING_UUID,
    _CSR_BT_OBEX_IMAGING_RESPONDER_UUID = CSR_BT_OBEX_IMAGING_RESPONDER_UUID,
    _CSR_BT_OBEX_IMAGING_AUTOMATIC_ARCHIVE_UUID = CSR_BT_OBEX_IMAGING_AUTOMATIC_ARCHIVE_UUID,
    _CSR_BT_HF_PROFILE_UUID = CSR_BT_HF_PROFILE_UUID,
    _CSR_BT_HFG_PROFILE_UUID = CSR_BT_HFG_PROFILE_UUID,
    _CSR_BT_DIRECT_PRINTING_REFERENCE_OBJ_UUID = CSR_BT_DIRECT_PRINTING_REFERENCE_OBJ_UUID,
    _CSR_BT_BASIC_PRINTING_UUID = CSR_BT_BASIC_PRINTING_UUID,
    _CSR_BT_PRINTING_STATUS_UUID = CSR_BT_PRINTING_STATUS_UUID,
    _CSR_BT_HID_PROFILE_UUID = CSR_BT_HID_PROFILE_UUID,
    _CSR_BT_HCR_PROFILE_UUID = CSR_BT_HCR_PROFILE_UUID,
    _CSR_BT_SIM_ACCESS_PROFILE_UUID = CSR_BT_SIM_ACCESS_PROFILE_UUID,
    _CSR_BT_OBEX_PBA_CLIENT_PROFILE_UUID = CSR_BT_OBEX_PBA_CLIENT_PROFILE_UUID,
    _CSR_BT_OBEX_PBA_SERVER_PROFILE_UUID = CSR_BT_OBEX_PBA_SERVER_PROFILE_UUID,
    _CSR_BT_OBEX_PBAP_PROFILE_UUID = CSR_BT_OBEX_PBAP_PROFILE_UUID,
    _CSR_BT_HS_12_PROFILE_UUID = CSR_BT_HS_12_PROFILE_UUID,
    _CSR_BT_OBEX_MESSAGE_ACCESS_SERVER_UUID = CSR_BT_OBEX_MESSAGE_ACCESS_SERVER_UUID,
    _CSR_BT_OBEX_MESSAGE_NOTIFICATION_SERVER_UUID = CSR_BT_OBEX_MESSAGE_NOTIFICATION_SERVER_UUID,
    _CSR_BT_OBEX_MESSAGE_ACCESS_PROFILE_UUID = CSR_BT_OBEX_MESSAGE_ACCESS_PROFILE_UUID,
    _CSR_BT_PNP_INFORMATION_UUID = CSR_BT_PNP_INFORMATION_UUID,
    _CSR_BT_VIDEO_SOURCE_UUID = CSR_BT_VIDEO_SOURCE_UUID,
    _CSR_BT_VIDEO_SINK_UUID = CSR_BT_VIDEO_SINK_UUID,
    _CSR_BT_VIDEO_DISTRIBUTION_UUID = CSR_BT_VIDEO_DISTRIBUTION_UUID,
    _CSR_BT_HDP_PROFILE_UUID = CSR_BT_HDP_PROFILE_UUID,
    _CSR_BT_HDP_SOURCE_UUID = CSR_BT_HDP_SOURCE_UUID,
    _CSR_BT_HDP_SINK_UUID = CSR_BT_HDP_SINK_UUID,
    _CSR_BT_PUBLIC_BROWSE_GROUP_UUID = CSR_BT_PUBLIC_BROWSE_GROUP_UUID,
    _CSR_BT_GENERIC_ACCESS_PROFILE_UUID = CSR_BT_GENERIC_ACCESS_PROFILE_UUID,
    _CSR_BT_GENERIC_ATTRIBUTE_PROFILE_UUID = CSR_BT_GENERIC_ATTRIBUTE_PROFILE_UUID,
    _CSR_BT_GNSS_PROFILE_UUID = CSR_BT_GNSS_PROFILE_UUID,
    _CSR_BT_GNSS_SERVER_UUID = CSR_BT_GNSS_SERVER_UUID,
};

enum psm_t{
    _CSR_BT_ICP_PSM = CSR_BT_ICP_PSM,
    _CSR_BT_CTP_PSM = CSR_BT_CTP_PSM,
    _CSR_BT_PAN_BNEP_PSM = CSR_BT_PAN_BNEP_PSM,
    _CSR_BT_HID_CTRL_PSM = CSR_BT_HID_CTRL_PSM,
    _CSR_BT_HID_INTR_PSM = CSR_BT_HID_INTR_PSM,
    _CSR_BT_AVCTP_PSM = CSR_BT_AVCTP_PSM,
    _CSR_BT_AVDTP_PSM = CSR_BT_AVDTP_PSM,
    _CSR_BT_AVCTP_BROWSING_PSM = CSR_BT_AVCTP_BROWSING_PSM,
    _CSR_BT_GATT_PSM = CSR_BT_GATT_PSM,
};

enum link_policy_settings_t{
    _CSR_BT_DEFAULT_LOW_POWER_MODES = CSR_BT_DEFAULT_LOW_POWER_MODES,
};

enum CsrBtBslPanRole{
    _CSR_BT_BSL_NO_ROLE = CSR_BT_BSL_NO_ROLE,
    _CSR_BT_BSL_NAP_ROLE = CSR_BT_BSL_NAP_ROLE,
    _CSR_BT_BSL_GN_ROLE = CSR_BT_BSL_GN_ROLE,
    _CSR_BT_BSL_PANU_ROLE = CSR_BT_BSL_PANU_ROLE,
};

enum CsrPrim{
    _CSR_BT_DG_PRIM = CSR_BT_DG_PRIM,
    _CSR_BT_DUNC_PRIM = CSR_BT_DUNC_PRIM,
    _CSR_BT_SC_PRIM = CSR_BT_SC_PRIM,
    _CSR_BT_CM_PRIM = CSR_BT_CM_PRIM,
    _CSR_BT_SDS_PRIM = CSR_BT_SDS_PRIM,
    _CSR_BT_OPS_PRIM = CSR_BT_OPS_PRIM,
    _CSR_BT_OPC_PRIM = CSR_BT_OPC_PRIM,
    _CSR_BT_AT_PRIM = CSR_BT_AT_PRIM,
    _CSR_BT_IWU_PRIM = CSR_BT_IWU_PRIM,
    _CSR_BT_SYNCS_PRIM = CSR_BT_SYNCS_PRIM,
    _CSR_BT_SPP_PRIM = CSR_BT_SPP_PRIM,
    _CSR_BT_FAX_PRIM = CSR_BT_FAX_PRIM,
    _CSR_BT_HFG_PRIM = CSR_BT_HFG_PRIM,
    _CSR_BT_HF_PRIM = CSR_BT_HF_PRIM,
    _CSR_BT_FTS_PRIM = CSR_BT_FTS_PRIM,
    _CSR_BT_BNEP_PRIM = CSR_BT_BNEP_PRIM,
    _CSR_BT_BSL_PRIM = CSR_BT_BSL_PRIM,
    _CSR_BT_BIPS_PRIM = CSR_BT_BIPS_PRIM,
    _CSR_BT_BIPC_PRIM = CSR_BT_BIPC_PRIM,
    _CSR_BT_FTC_PRIM = CSR_BT_FTC_PRIM,
    _CSR_BT_SMLC_PRIM = CSR_BT_SMLC_PRIM,
    _CSR_BT_SMLS_PRIM = CSR_BT_SMLS_PRIM,
    _CSR_BT_PPP_PRIM = CSR_BT_PPP_PRIM,
    _CSR_BT_BPPC_PRIM = CSR_BT_BPPC_PRIM,
    _CSR_BT_AV_PRIM = CSR_BT_AV_PRIM,
    _CSR_BT_AVRCP_PRIM = CSR_BT_AVRCP_PRIM,
    _CSR_BT_SAPS_PRIM = CSR_BT_SAPS_PRIM,
    _CSR_BT_SAPC_PRIM = CSR_BT_SAPC_PRIM,
    _CSR_BT_SD_PRIM = CSR_BT_SD_PRIM,
    _CSR_BT_HIDH_PRIM = CSR_BT_HIDH_PRIM,
    _CSR_BT_HIDD_PRIM = CSR_BT_HIDD_PRIM,
    _CSR_BT_PAS_PRIM = CSR_BT_PAS_PRIM,
    _CSR_BT_PAC_PRIM = CSR_BT_PAC_PRIM,
    _CSR_BT_BPPS_PRIM = CSR_BT_BPPS_PRIM,
    _CSR_BT_LSL_PRIM = CSR_BT_LSL_PRIM,
    _CSR_BT_HCRP_PRIM = CSR_BT_HCRP_PRIM,
    _CSR_BT_JSR82_PRIM = CSR_BT_JSR82_PRIM,
    _CSR_BT_SDC_PRIM = CSR_BT_SDC_PRIM,
    _CSR_BT_SYNCC_PRIM = CSR_BT_SYNCC_PRIM,
    _CSR_BT_MCAP_PRIM = CSR_BT_MCAP_PRIM,
    _CSR_BT_HDP_PRIM = CSR_BT_HDP_PRIM,
    _CSR_BT_MAPC_PRIM = CSR_BT_MAPC_PRIM,
    _CSR_BT_MAPS_PRIM = CSR_BT_MAPS_PRIM,
    _CSR_BT_AMPM_PRIM = CSR_BT_AMPM_PRIM,
    _CSR_BT_PHDC_MGR_PRIM = CSR_BT_PHDC_MGR_PRIM,
    _CSR_BT_PHDC_AG_PRIM = CSR_BT_PHDC_AG_PRIM,
    _CSR_BT_GATT_PRIM = CSR_BT_GATT_PRIM,
    _CSR_BT_PROX_SRV_PRIM = CSR_BT_PROX_SRV_PRIM,
    _CSR_BT_THERM_SRV_PRIM = CSR_BT_THERM_SRV_PRIM,
    _CSR_BT_GENERIC_SRV_PRIM = CSR_BT_GENERIC_SRV_PRIM,
    _CSR_BT_GNSS_CLIENT_PRIM = CSR_BT_GNSS_CLIENT_PRIM,
    _CSR_BT_GNSS_SERVER_PRIM = CSR_BT_GNSS_SERVER_PRIM,
    _CSR_BT_ASM_PRIM = CSR_BT_ASM_PRIM,
    _CSR_BT_AVRCP_IMAGING_PRIM = CSR_BT_AVRCP_IMAGING_PRIM,
    _CSR_BT_HOGH_PRIM = CSR_BT_HOGH_PRIM,
    _CSR_BT_PXPM_PRIM = CSR_BT_PXPM_PRIM,
    _CSR_BT_LE_SRV_PRIM = CSR_BT_LE_SRV_PRIM,
    _CSR_BT_LE_SVC_PRIM = CSR_BT_LE_SVC_PRIM,
    _CSR_BT_LPM_PRIM = CSR_BT_LPM_PRIM,
    _CSR_BT_TPM_PRIM = CSR_BT_TPM_PRIM,
    _CSR_BT_GGPROXY_PRIM = CSR_BT_GGPROXY_PRIM,
    _BAP_PRIM = BAP_PRIM,
    _ASCS_CLIENT_PRIM = ASCS_CLIENT_PRIM,
    _PACS_CLIENT_PRIM = PACS_CLIENT_PRIM,
    _GATT_SRVC_DISC_PRIM = GATT_SRVC_DISC_PRIM,
    _CSR_BT_SYNERGY_AG = CSR_BT_SYNERGY_AG,
    _VCP_PRIM = VCP_PRIM,
    _VCS_CLIENT_PRIM = VCS_CLIENT_PRIM,
    _VOCS_CLIENT_PRIM = VOCS_CLIENT_PRIM,
    _AICS_CLIENT_PRIM = AICS_CLIENT_PRIM,
    _PACS_SERVER_PRIM = PACS_SERVER_PRIM,
    _VCS_SERVER_PRIM = VCS_SERVER_PRIM,
    _ASCS_SERVER_PRIM = ASCS_SERVER_PRIM,
    _CSIP_PRIM = CSIP_PRIM,
    _CSIS_CLIENT_PRIM = CSIS_CLIENT_PRIM,
    _TBS_SERVER_PRIM = TBS_SERVER_PRIM,
    _BASS_CLIENT_PRIM = BASS_CLIENT_PRIM,
    _TBS_CLIENT_PRIM = TBS_CLIENT_PRIM,
    _CSIS_SERVER_PRIM = CSIS_SERVER_PRIM,
    _BASS_SERVER_PRIM = BASS_SERVER_PRIM,
    _MCS_CLIENT_PRIM = MCS_CLIENT_PRIM,
    _MCP_PRIM = MCP_PRIM,
    _MCS_SERVER_PRIM = MCS_SERVER_PRIM,
    _CTM_PRIM = CTM_PRIM,
    _MICS_SERVER_PRIM = MICS_SERVER_PRIM,
    _BAP_SERVER_PRIM = BAP_SERVER_PRIM,
    _WMAS_SERVER_PRIM = WMAS_SERVER_PRIM,
    _GMAS_SERVER_PRIM = GMAS_SERVER_PRIM,
    _CAS_SERVER_PRIM = CAS_SERVER_PRIM,
    _CCP_PRIM = CCP_PRIM,
    _BAS_SERVER_PRIM = BAS_SERVER_PRIM,
    _DIS_SERVER_PRIM = DIS_SERVER_PRIM,
    _TPS_SERVER_PRIM = TPS_SERVER_PRIM,
    _TDS_SERVER_PRIM = TDS_SERVER_PRIM,
    _TDS_CLIENT_PRIM = TDS_CLIENT_PRIM,
    _CHP_SEEKER_PRIM = CHP_SEEKER_PRIM,
    _TMAS_CLIENT_PRIM = TMAS_CLIENT_PRIM,
    _TMAP_CLIENT_PRIM = TMAP_CLIENT_PRIM,
    _TMAS_SERVER_PRIM = TMAS_SERVER_PRIM,
    _CAP_CLIENT_PRIM = CAP_CLIENT_PRIM,
    _BT_VSDM_PRIM = BT_VSDM_PRIM,
    _MICP_PRIM = MICP_PRIM,
    _MICS_CLIENT_PRIM = MICS_CLIENT_PRIM,
    _PBP_PRIM = PBP_PRIM,
    _GMAS_CLIENT_PRIM = GMAS_CLIENT_PRIM,
    _GMAP_CLIENT_PRIM = GMAP_CLIENT_PRIM,
    _HIDS_SERVER_PRIM = HIDS_SERVER_PRIM,
    _QSS_SERVER_PRIM = QSS_SERVER_PRIM,
};

#endif /*CSR_BT_PROFILES_ENUM_DBG_H__*/
