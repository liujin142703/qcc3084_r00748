/*!
\copyright  Copyright (c) 2023 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Synergy's Version
*/
#include "csr_synergy.h"


_Pragma("datasection LIB_VERSIONS") const char* synergy_si_version = "21.2.0-SYNERGY.SRC.1.0-00079-PT_GEN_PACK-1";

