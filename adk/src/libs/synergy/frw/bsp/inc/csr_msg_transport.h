#ifndef CSR_MSG_TRANSPORT_H__
#define CSR_MSG_TRANSPORT_H__
/*****************************************************************************
 Copyright (c) 2008-2018, The Linux Foundation.
 All rights reserved.
*****************************************************************************/

#include "csr_synergy.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef CsrMsgTransport
#define CsrMsgTransport CsrSchedMessagePut
#endif

#ifdef __cplusplus
}
#endif

#endif /* CSR_MSG_TRANSPORT */
