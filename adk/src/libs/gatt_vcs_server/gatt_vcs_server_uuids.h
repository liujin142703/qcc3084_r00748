/*******************************************************************************
Copyright (c) 2020 Qualcomm Technologies International, Ltd.
 
*******************************************************************************/

#ifndef __GATT_VCS_SERVER_UUIDS_H__
#define __GATT_VCS_SERVER_UUIDS_H__

/* UUIDs for Volume Control Service and Characteristics*/

#define UUID_VOLUME_CONTROL_SERVICE                 0x1844

#define UUID_VOLUME_STATE                           0x2B7D
#define UUID_VOLUME_CONTROL_POINT                   0x2B7E
#define UUID_VOLUME_FLAGS                           0x2B7F

#endif /* __GATT_VCS_SERVER_UUIDS_H__ */

