#ifndef BUTTON_CONFIG_H
#define BUTTON_CONFIG_H

#include "domain_message.h"
#include "input_event_manager.h"
extern const InputEventConfig_t input_event_config;

extern const InputActionMessage_t default_message_group[22];

#define SYS_CTRL             (1UL <<  0)
#define VOL_PLUS             (1UL <<  2)
#define VOL_MINUS            (1UL <<  1)
#define ANC                  (1UL <<  3)

#define MIN_INPUT_ACTION_MESSAGE_ID (LOGICAL_INPUT_MESSAGE_BASE-1)
#define MAX_INPUT_ACTION_MESSAGE_ID (LOGICAL_INPUT_MESSAGE_BASE+20)

#define LI_MFB_BUTTON_HELD_FACTORY_RESET_DS      (LOGICAL_INPUT_MESSAGE_BASE- 0x1)
#define LI_MFB_BUTTON_HELD_3SEC                  (LOGICAL_INPUT_MESSAGE_BASE+ 0x0)
#define LI_MFB_BUTTON_SINGLE_PRESS               (LOGICAL_INPUT_MESSAGE_BASE+ 0x1)
#define VOL_MINUS_RELEASE_1S                     (LOGICAL_INPUT_MESSAGE_BASE+ 0x2)
#define LI_MFB_BUTTON_HELD_12SEC                 (LOGICAL_INPUT_MESSAGE_BASE+ 0x3)
#define LI_MFB_BUTTON_HELD_6SEC                  (LOGICAL_INPUT_MESSAGE_BASE+ 0x4)
#define LI_MFB_BUTTON_MULTI_CLICK                (LOGICAL_INPUT_MESSAGE_BASE+ 0x5)
#define APP_BUTTON_VOLUME_UP                     (LOGICAL_INPUT_MESSAGE_BASE+ 0x6)
#define LI_MFB_BUTTON_HELD_2SEC                  (LOGICAL_INPUT_MESSAGE_BASE+ 0x7)
#define APP_BUTTON_ANC_TUNNING                   (LOGICAL_INPUT_MESSAGE_BASE+ 0x8)
#define APP_BUTTON_VOLUME_DOWN                   (LOGICAL_INPUT_MESSAGE_BASE+ 0x9)
#define APP_BUTTON_ANC_MULTI                     (LOGICAL_INPUT_MESSAGE_BASE+ 0xa)
#define VOL_PLUS_RELEASE_1S                      (LOGICAL_INPUT_MESSAGE_BASE+ 0xb)
#define RESET_PAIRED_DEVICE_LIST                 (LOGICAL_INPUT_MESSAGE_BASE+ 0xc)
#define LI_MFB_BUTTON_DOUBLE_PRESS               (LOGICAL_INPUT_MESSAGE_BASE+ 0xd)
#define LI_MFB_BUTTON_RELEASE_1SEC               (LOGICAL_INPUT_MESSAGE_BASE+ 0xe)
#define ENTER_DUT_MODE                           (LOGICAL_INPUT_MESSAGE_BASE+ 0xf)
#define LI_MFB_BUTTON_HELD_1SEC                  (LOGICAL_INPUT_MESSAGE_BASE+0x10)
#define APP_BUTTON_ANC                           (LOGICAL_INPUT_MESSAGE_BASE+0x11)
#define APP_BUTTON_FORWARD                       (LOGICAL_INPUT_MESSAGE_BASE+0x12)
#define APP_BUTTON_ANC_DOUBLE                    (LOGICAL_INPUT_MESSAGE_BASE+0x13)
#define APP_BUTTON_BACKWARD                      (LOGICAL_INPUT_MESSAGE_BASE+0x14)

#endif

