#include "input_event_manager.h"
#include "6_buttons.h"

const InputEventConfig_t input_event_config = 
{
	/* Table to convert from PIO to input event ID*/
	{
		 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
		-1, -1, -1, -1, -1, -1,  3, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
		-1, -1,  1,  2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
	},

	/* Masks for each PIO bank to configure as inputs */
	{ 0x00400001UL, 0x0000000cUL },
	/* PIO debounce settings */
	4, 5
};


const InputActionMessage_t default_message_group[22] = 
{
	{
		SYS_CTRL,                               /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		MULTI_CLICK,                            /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		3,                                      /* Count */
		LI_MFB_BUTTON_MULTI_CLICK,              /* Message */
	},
	{
		SYS_CTRL,                               /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		3000,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		LI_MFB_BUTTON_HELD_3SEC,                /* Message */
	},
	{
		SYS_CTRL | VOL_PLUS | VOL_MINUS | ANC,  /* Input event bits */
		SYS_CTRL | VOL_PLUS | VOL_MINUS | ANC,  /* Input event mask */
		HELD,                                   /* Action */
		5000,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		LI_MFB_BUTTON_HELD_6SEC,                /* Message */
	},
	{
		0,                                      /* Input event bits */
		VOL_PLUS,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		10,                                     /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		APP_BUTTON_VOLUME_UP,                   /* Message */
	},
	{
		SYS_CTRL,                               /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		15000,                                  /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		LI_MFB_BUTTON_HELD_FACTORY_RESET_DS,    /* Message */
	},
	{
		0,                                      /* Input event bits */
		VOL_PLUS,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		1000,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		VOL_PLUS_RELEASE_1S,                    /* Message */
	},
	{
		SYS_CTRL,                               /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		MULTI_CLICK,                            /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		2,                                      /* Count */
		LI_MFB_BUTTON_DOUBLE_PRESS,             /* Message */
	},
	{
		SYS_CTRL,                               /* Input event bits */
		SYS_CTRL | VOL_PLUS,                    /* Input event mask */
		HELD,                                   /* Action */
		6000,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		ENTER_DUT_MODE,                         /* Message */
	},
	{
		SYS_CTRL,                               /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		1200,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		LI_MFB_BUTTON_HELD_1SEC,                /* Message */
	},
	{
		0,                                      /* Input event bits */
		VOL_MINUS,                              /* Input event mask */
		HELD,                                   /* Action */
		1100,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		APP_BUTTON_BACKWARD,                    /* Message */
	},
	{
		SYS_CTRL,                               /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		MULTI_CLICK,                            /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		1,                                      /* Count */
		LI_MFB_BUTTON_SINGLE_PRESS,             /* Message */
	},
	{
		0,                                      /* Input event bits */
		VOL_MINUS | SYS_CTRL,                   /* Input event mask */
		HELD_RELEASE,                           /* Action */
		1000,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		VOL_MINUS_RELEASE_1S,                   /* Message */
	},
	{
		SYS_CTRL | VOL_PLUS | VOL_MINUS | ANC,  /* Input event bits */
		SYS_CTRL | VOL_PLUS | VOL_MINUS | ANC,  /* Input event mask */
		HELD,                                   /* Action */
		2000,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		LI_MFB_BUTTON_HELD_2SEC,                /* Message */
	},
	{
		0,                                      /* Input event bits */
		VOL_MINUS,                              /* Input event mask */
		HELD_RELEASE,                           /* Action */
		10,                                     /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		APP_BUTTON_VOLUME_DOWN,                 /* Message */
	},
	{
		0,                                      /* Input event bits */
		ANC,                                    /* Input event mask */
		MULTI_CLICK,                            /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		3,                                      /* Count */
		APP_BUTTON_ANC_MULTI,                   /* Message */
	},
	{
		SYS_CTRL,                               /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		1200,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		LI_MFB_BUTTON_RELEASE_1SEC,             /* Message */
	},
	{
		0,                                      /* Input event bits */
		ANC,                                    /* Input event mask */
		MULTI_CLICK,                            /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		2,                                      /* Count */
		APP_BUTTON_ANC_DOUBLE,                  /* Message */
	},
	{
		SYS_CTRL | VOL_PLUS,                    /* Input event bits */
		SYS_CTRL | VOL_PLUS,                    /* Input event mask */
		HELD,                                   /* Action */
		12000,                                  /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		LI_MFB_BUTTON_HELD_12SEC,               /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL | VOL_MINUS | VOL_PLUS,        /* Input event mask */
		HELD,                                   /* Action */
		5000,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		RESET_PAIRED_DEVICE_LIST,               /* Message */
	},
	{
		0,                                      /* Input event bits */
		ANC,                                    /* Input event mask */
		HELD,                                   /* Action */
		4000,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		APP_BUTTON_ANC_TUNNING,                 /* Message */
	},
	{
		0,                                      /* Input event bits */
		ANC,                                    /* Input event mask */
		MULTI_CLICK,                            /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		1,                                      /* Count */
		APP_BUTTON_ANC,                         /* Message */
	},
	{
		0,                                      /* Input event bits */
		VOL_PLUS | SYS_CTRL,                    /* Input event mask */
		HELD,                                   /* Action */
		1100,                                   /* Timeout */
		0,                                      /* Repeat */
		0,                                      /* Count */
		APP_BUTTON_FORWARD,                     /* Message */
	},
};


ASSERT_MESSAGE_GROUP_NOT_OVERFLOWED(LOGICAL_INPUT,MAX_INPUT_ACTION_MESSAGE_ID)

