#ifndef _SINK_AT_H_
#define _SINK_AT_H_
 
bool hfpAtCmdInit(Task init_task);
void HfpAtCmdSendBatt(uint16 voltage_mv);
void HfpEnableBattReport(void);
#endif