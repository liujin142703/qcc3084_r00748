#ifndef ZY_FUNC_DEF_H
#define ZY_FUNC_DEF_H

/*代码BANK默认为0，暂时只能用pio0-31*/

/* 佩戴检测传感器 */
#define WEAR_DETECT_USE_HY 0
#define WEAR_DETECT_USE_STK33532 1


#if WEAR_DETECT_USE_HY
#define WEAR_DETECT_PIN         (1 << 5)
#define WEAR_RESULT_PIN         (1 << 4)
#elif WEAR_DETECT_USE_STK33532
#define RET_INT_PIN             (1 << 5)
#define STK33532_I2C_ADDR       0x46
#endif


/* 外部LDO控制，F05控制光感下电 */
#define Enable_Extern_DCDC
#define Extern_DCDC_EN_PIN      (1 << 2)


/* 产线UART测试指令 */
// #define UART_FOR_FACTORY

/* BP1 外置mic检测 */
// #define Enable_EXTERN_MIC_DETECT
#define EXTERN_MIC_DETECT_PIN         (1 << 2)
#define EXTERN_MIC_LED_PIN            5

#endif // ZY_FUNC_DEF_H
