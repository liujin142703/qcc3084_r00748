#ifndef ZY_GAIA_H
#define ZY_GAIA_H

#include <gaia.h>
#include "logging.h"

extern uint16 eq_ucid;
extern uint32 my_peq_gains[10];
void zy_set_user_eq_ucid(uint16 eq_ucid);
void gaia_set_transport(GAIA_TRANSPORT* transport);
GAIA_TRANSPORT* gaia_get_transport(void);
void gaiaFrameworkCommand_GetZyCommand(GAIA_TRANSPORT *transport, uint16 vendor_id, uint16 command_id, uint16 payload_size, const uint8 *payload);

#endif // ZY_GAIA_H
