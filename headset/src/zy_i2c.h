#ifndef ZY_I2C_H
#define ZY_I2C_H

void i2cPioInit(void);
bool i2c_write_byte_for1787(uint16 addr, uint8 *value, uint16 size);
bool i2c_read_byte_for1787(uint16 addr, uint8 *value, uint16 size);
bool i2c_write_byte(uint16 devAddress, uint8 address, uint8 value);
bool i2c_read_byte(uint8 devAddress, uint8 address, uint8 *pData, int size);

#endif // ZY_I2C_H
