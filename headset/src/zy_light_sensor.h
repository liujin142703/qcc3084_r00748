#ifndef ZY_LIGHT_SENSOR_H
#define ZY_LIGHT_SENSOR_H

#include <message.h>
#include <pio.h>
#include "zy_func_def.h"

typedef enum
{
    EventLSDetectOFFLED,
    EventLSDetectONLED,
    EventLSDetectOFFLEDRecheck,
    EventSTK33532Start,
    EventLSWear,
    EventLSNoWear,
    EventLSMusicPause,
    EventLSMusicPlay
} LS_Events;

typedef struct
{
    TaskData task;
} LightSensorTaskData;
extern LightSensorTaskData light_sensor_tast;

#define LightSensorGetTaskData() (&light_sensor_tast)

#define LightSensorGetTask() (&light_sensor_tast.task)

void Light_Sensor_Init(Task init_task);
void Light_Sensor_DeInit(void);
void Light_Sensor_Detect_OFFLED(void);
void Light_Sensor_Detect_ONLED(void);
void Light_Sensor_Detect_OFFLED_Recheck(void);
void Wear_State_Operate(void);
void No_Wear_State_Operate(void);
void set_light_sensor_enabled(bool mode);
bool get_light_sensor_enabled(void);

#if WEAR_DETECT_USE_STK33532
void STK33532_Reg_Init(void);
void STK33532_Wear_Detect(void);
#endif

#endif // ZY_LIGHT_SENSOR_H
