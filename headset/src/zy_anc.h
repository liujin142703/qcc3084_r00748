#ifndef ZY_ANC_H
#define ZY_ANC_H

void Extern_DCDC_Init(void);
void Extern_DCDC_DeInit(void);
void ADI1787_PD_Init(void);
void ADI1787_PD_DeInit(void);
void AUX_Mic_Switch_Init(void);
void AUX_Mic_Switch_DeInit(void);

void ADI1787_Init(Task init_task, const uint8 *adi1787_init_data_p0, uint16 size_p0, const uint8 *adi1787_init_data_p1, uint16 size_p1);
void anc_state_circle(void);

extern bool usb_state_voice;

#endif // ZY_ANC_H
