#ifndef ZY_ENCORDER_H
#define ZY_ENCORDER_H

#include <message.h>
#include <pio.h>

// #define ENABLE_ENCODER
#ifdef ENABLE_ENCODER

#define NUMBER_OF_PIOS_PER_BANK    (32U)
#define PIO_NONE                   (0UL)
#define PIO_SUCCESS                PIO_NONE
#define PIO_BANK(pio)   (uint16)((pio)/NUMBER_OF_PIOS_PER_BANK)

#define ENCODER_PIN_A         10    //音量+ pio
#define ENCODER_PIN_B         20    //音量- pio

#define ENCODER_PIN_A_MASK    (1 << (ENCODER_PIN_A & 0x1F))
#define ENCODER_PIN_B_MASK    (1 << (ENCODER_PIN_B & 0x1F))
#define GET_PIN_A             (PioGet32Bank(PIO_BANK(ENCODER_PIN_A)) & ENCODER_PIN_A_MASK)
#define GET_PIN_B             (PioGet32Bank(PIO_BANK(ENCODER_PIN_B)) & ENCODER_PIN_B_MASK)
#define ENCODER_SCAN_PERIOD_MS         2
#define ENCODER_VOL_ADJUST_PERIOD_MS   100

typedef enum
{
    ENCODER_START_SCAN,
    ENCODER_STOP_SCAN,
    ENCODER_ACCEPT_VOL_ADJUST,
} Encoder_Events;

typedef enum
{
    encoder_idle,
    encoder_left,
    encoder_right,
} Encoder_Output;

typedef struct
{
    TaskData task;
} EncoderTaskData;
extern EncoderTaskData encoder_tast;

typedef struct
{
    int8 encoder_tpye;
    uint8 encoder_a_last;
    uint8 encoder_b_last;
} EncoderData;
extern EncoderData encoder_data;

#define EncoderGetTaskData() (&encoder_tast)

#define EncoderGetTask() (&encoder_tast.task)
#endif

#ifdef ENABLE_ENCODER
void Encoder_Init(void);
#else
#define Encoder_Init(x) ((void)(0))
#endif

#ifdef ENABLE_ENCODER
void Encoder_DeInit(void);
#else
#define Encoder_DeInit() ((void)(0))
#endif

#endif // ZY_ENCORDER_H
