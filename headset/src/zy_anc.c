
#include <i2c.h>
#include <ps.h>
#include <library.h>
#include <print.h>
#include <led.h>
#include <pio.h>
#include "zy_anc.h"
#include "zy_func_def.h"
#include "adi1787.h"
#include "zy_i2c.h"
#include "zy_gaia.h"
#include "logging.h"
#include "headset_test.h"
#include "headset_setup_audio.h"
#include "headset_sm.h"
#include "headset_sm_private.h"
#include "headset_sm_config.h"
#include "audio_sources.h"
#include "voice_sources.h"
#include "ui.h"
#include "bt_device.h"
#include "headset_setup_audio.h"
#include "kymera_usb_voice.h"
#include "ui_indicator_tones.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static Task hs_task;

void Extern_DCDC_Init(void)
{
#ifdef Enable_Extern_DCDC
    PioSetMapPins32Bank(0, Extern_DCDC_EN_PIN, Extern_DCDC_EN_PIN);
    PioSetDir32Bank(0, Extern_DCDC_EN_PIN, Extern_DCDC_EN_PIN); //设置GPIO输出模式
    PioSet32Bank(0, Extern_DCDC_EN_PIN, Extern_DCDC_EN_PIN);    //拉高使能GPIO
#endif
}

void Extern_DCDC_DeInit(void)
{
#ifdef Enable_Extern_DCDC
    PioSetDir32Bank(0, Extern_DCDC_EN_PIN, Extern_DCDC_EN_PIN); //设置GPIO输出模式
    PioSet32Bank(0, Extern_DCDC_EN_PIN, 0);                     //拉低关闭
    PioSetStrongBias32Bank(0, Extern_DCDC_EN_PIN, Extern_DCDC_EN_PIN);
#endif
}

#ifdef Enable_EXTERN_MIC_DETECT
static void mic_mute_led_init(uint8 led_num)
{
    LedConfigure(led_num, LED_DUTY_CYCLE, 0x3ff);  /* 亮度控制 */
    LedConfigure(led_num, LED_FLASH_ENABLE, 0);  /* 闪烁开关 */
    LedConfigure(led_num, LED_ENABLE, 0);  /* 开关 */
}
#endif

void AUX_Mic_Switch_Init(void)
{
#ifdef Enable_EXTERN_MIC_DETECT
    // PioSetMapPins32Bank(0, EXTERN_MIC_DETECT_PIN, EXTERN_MIC_DETECT_PIN);
    // PioSetDir32Bank(0, EXTERN_MIC_DETECT_PIN, 0);        //设置EXTERN_MIC_DETECT_PIN输入模式
    // PioSet32Bank(0, EXTERN_MIC_DETECT_PIN, EXTERN_MIC_DETECT_PIN);           //设置上拉
    // PioSetStrongBias32Bank(0, EXTERN_MIC_DETECT_PIN, EXTERN_MIC_DETECT_PIN); //设置高阻
    mic_mute_led_init(EXTERN_MIC_LED_PIN); //外置MIC LED采用LED4控制

    // hfpCheckVoiceNumbersConfig();
#endif
}

void AUX_Mic_Switch_DeInit(void)
{
#ifdef Enable_EXTERN_MIC_DETECT
    // PioSetMapPins32Bank(0, EXTERN_MIC_DETECT_PIN, EXTERN_MIC_DETECT_PIN);
    // PioSetDir32Bank(0, EXTERN_MIC_DETECT_PIN, 0);        //设置EXTERN_MIC_DETECT_PIN输入模式
    // PioSet32Bank(0, EXTERN_MIC_DETECT_PIN, 0);
    // PioSetStrongBias32Bank(0, EXTERN_MIC_DETECT_PIN, 0); //设置高阻
    mic_mute_led_init(EXTERN_MIC_LED_PIN); //外置MIC LED采用LED5控制
#endif
}

static void user_peq_ucid_init(void)
{
    uint16 ret_len;
    uint16 requested_words;
    uint16 peq_gain_buffer[20] = {0};
    ret_len = PsRetrieve(anc_state_key, NULL, 0);
    requested_words = PS_SIZE_ADJ(sizeof(uint16));
    if (!ret_len){
        eq_ucid = EQ_BANK_USER;
        PsStore(eq_ucid_key, (const uint16 *)&eq_ucid, requested_words);
        PsStore(my_peq_gain_key, (const uint16 *)peq_gain_buffer, 20);
        zy_set_user_eq_ucid(eq_ucid);
    }else
    {
        PsRetrieve(eq_ucid_key, &eq_ucid, requested_words);
        PsRetrieve(my_peq_gain_key, peq_gain_buffer, 20);
        for (int i = 0; i < 10; i++)
        {
            my_peq_gains[i] = (uint32)(peq_gain_buffer[i*2]<<16 | peq_gain_buffer[i*2+1]);
        }
        zy_set_user_eq_ucid(eq_ucid);
    }    
}

void ADI1787_PD_Init(void)
{
    // MessageCancelAll(AncGetTask(), EventMyAdiPdSetDown);  /* 防止下电未完成时用户又开机 */
    PioSetMapPins32Bank(0, ADI1787_PD_PIN, ADI1787_PD_PIN);
    PioSetDir32Bank(0, ADI1787_PD_PIN, ADI1787_PD_PIN); //设置GPIO输出模式
    PioSet32Bank(0, ADI1787_PD_PIN, ADI1787_PD_PIN);    //拉高使能GPIO
}

void ADI1787_PD_DeInit(void)
{
    // MessageCancelAll(AncGetTask(), EventMyAdiPdSetDown);
    // MessageSendLater(AncGetTask(), EventMyAdiPdSetDown, 0, 8000);
    my_battery_low_prompt_indcation_stop();
    save_power_off_ANC_state();
    ANC_Adaptive_Task_DeInit();
    spp_server_started = FALSE;
}

void ADI1787_Init(Task init_task, const uint8 *adi1787_init_data_p0, uint16 size_p0, const uint8 *adi1787_init_data_p1, uint16 size_p1)
{
    hs_task = init_task;
    ANC_task_init(init_task);
    ANC_Adaptive_Task_Init();
    ANC_led_init(ANC_LED_PIN);
    Parst_Adi1787_init_Data(adi1787_init_data_p0, size_p0);
    Parst_Adi1787_init_Data(adi1787_init_data_p1, size_p1);
    Set_Dafault_ANC_State();
    user_peq_ucid_init();
    Init_ADC_Value();
    // uint8 spt0_master_value = 0x11;
    // i2c_reg_write(i2c_addr_1787_8bit, ADI1787_SPT0_CTRL2, 1, &spt0_master_value); /* SPT0设置48k master模式 */
    i2c_write_byte_for1787(0xC0B6, ADC_UNMUTE, 1); //1787 i2s暂时需配置为slave模式
    my_battery_low_prompt_indcation_start();
}

void restore_volume(void)
{
//    updateVolume(&volume_corrent, audio_output_group_main);
}


void anc_state_circle(void)
{
   uint8 response[3] = {0x0b, 0x0a, 0x00}; /* GAIA发送ANC实时状态 0：OFF,1：ON, 2：ambient sound */
    if (anc_is_changing)
    {
        /* ANC未切换完成，不进行操作, DAC_Fade_Out函数会把此变量置零*/
    }
    else
    {
         /* 播放TONE期间调整音乐音量 */
//        if ((lState == deviceConnected) || (lState == deviceA2DPStreaming))
//        {
//            volume_info volume;
//            getVolumeInfoFromAudioSource(sinkAudioGetRoutedAudioSource(), &volume_corrent);
//            if ((volume_corrent.main_volume >3) || (volume_corrent.aux_volume >3))
//            {
//                volume.main_volume = 3;
//                volume.aux_volume = 3;
//                updateVolume(&volume, audio_output_group_main);
//                MessageSendLater(AncGetTask(), EventMyRestoreVolume, 0, anc_prompt_time);
//            }
//        }
        if (appTestIsHandsetHfpScoActive())
        {
            play_tone_on_anc_changing = 0;
        }else
        {
            play_tone_on_anc_changing = 1;
        }
        anc_is_changing = 1;
        if (anc_state == state_anc_off) /* 默认降噪关，跳转降噪开 */
        {
            MessageSend(AncGetTask(), EventMyAncOn, 0);
           response[2] = 1;
        }
        else if (anc_state == state_anc_on) /* 降噪开，跳转环境音 */
        {
            MessageSend(AncGetTask(), EventMyMonitorOn, 0);
           response[2] = 2;
        }
        else /*环境音，调整降噪关 */
        {
            MessageSend(AncGetTask(), EventMyAncOff, 0);
           response[2] = 0;
        }
        /* APP连接状态下发送状态更改提示 */
        if (appDeviceIsHandsetConnected())
        {
            GaiaSendPacket(gaia_get_transport(), GAIA_VENDOR_QTIL, GAIA_EVENT_NOTIFICATION, GAIA_STATUS_NONE, 3, response);
        }
        

        if(SmGetTaskData()->state == HEADSET_STATE_IDLE)
        {
            if(headsetConfigIdleTimeoutMs())
            {
                MessageCancelAll(headsetSmGetTask(), SM_INTERNAL_TIMEOUT_IDLE);
                MessageSendLater(headsetSmGetTask(), SM_INTERNAL_TIMEOUT_IDLE, NULL, headsetConfigIdleTimeoutMs()); 
            }
        }
    }
}
