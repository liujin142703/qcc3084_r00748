#include "zy_at_commands.h"
#include <string.h>
#include <byte_utils.h>
#include <stdlib.h>
#include <bt_device.h>
#include "battery_monitor.h"
#include "state_of_charge.h"
#include "hfp_profile.h"
#include "logging.h"
#include "battery_monitor_config.h"
#include "hfp.h"
 
typedef struct
{
    TaskData task;
    batteryRegistrationForm Client; 
} atCmdTaskData;
 
atCmdTaskData atCmdTask; 
 
/* vender id - product id - version- report bit */
//static const char batt_enable_string[]   = "AT+XAPL=05AC-1702-0100,9\r";
static const char batt_level_string[]   = "AT+IPHONEACCEV=1,1,0\r";
uint8 last_battery_percent = 11;
#define BATT_POS   19

void HfpEnableBattReport(void)
{
    // HfpAtCmdRequest(hfp_primary_link, batt_enable_string);
}
 
void HfpAtCmdSendBatt(uint16 voltage_mv)
{
    char *at_command_data;
    uint8 percent = Soc_ConvertLevelToPercentage(voltage_mv);
    at_command_data = malloc(sizeof(batt_level_string));
    if (at_command_data)
    {
        DEBUG_LOG_VERBOSE("atcmd battery event: %d",percent);
        memcpy(at_command_data, batt_level_string, sizeof(batt_level_string));
        if (percent > 94)
            at_command_data[BATT_POS] = '9';
        else if (percent > 84)
            at_command_data[BATT_POS] = '8';
        else if (percent > 74)
            at_command_data[BATT_POS] = '7';
        else if (percent > 64)
            at_command_data[BATT_POS] = '6';
        else if (percent > 54)
            at_command_data[BATT_POS] = '5';
        else if (percent > 44)
            at_command_data[BATT_POS] = '4';
        else if (percent > 34)
            at_command_data[BATT_POS] = '3';
        else if (percent > 24)
            at_command_data[BATT_POS] = '2';
        else if (percent > 14)
            at_command_data[BATT_POS] = '1';
        else
            at_command_data[BATT_POS] = '0';
        if (last_battery_percent != (percent/10))
        {
            DEBUG_LOG_DEBUG("HfpAtCmdSendBatt: %d",percent);
            last_battery_percent = percent/10;
            // HfpAtCmdRequest(hfp_primary_link, at_command_data);
        }
    }
    free(at_command_data);
}
 
static void hfpAtCmdHandleMessage(Task task, MessageId id, Message message)
{
    UNUSED(task);
 
    switch (id)
    {
        case MESSAGE_BATTERY_LEVEL_UPDATE_VOLTAGE:
            if(appHfpIsConnected())
                HfpAtCmdSendBatt(((MESSAGE_BATTERY_LEVEL_UPDATE_VOLTAGE_T *)message)->voltage_mv);
            break;
            
        default:
            break;
    }
}
 
bool hfpAtCmdInit(Task init_task)
{
    UNUSED(init_task);
 
    atCmdTask.task.handler = hfpAtCmdHandleMessage;
 
    atCmdTask.Client.task = &atCmdTask.task;
    atCmdTask.Client.hysteresis = 1;
    atCmdTask.Client.representation = battery_level_repres_voltage;
    last_battery_percent = 11;
    appBatteryRegister(&atCmdTask.Client);
 
    return TRUE;
}
