/*!
\copyright  Copyright (c) 2020-2023 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file       headset_ui_config.c
\brief      ui configuration table

    This file contains ui configuration table which maps different logical inputs to
    corresponding ui inputs based upon ui provider contexts.
*/

#include "headset_ui_config.h"
#include "ui.h"
#include "audio_curation.h"

#include "app_buttons.h"

/* Needed for UI contexts - transitional; when table is code generated these can be anonymised
 * unsigned ints and these includes can be removed. */
#include "av.h"
#include "hfp_profile.h"
#include "bt_device.h"
#include "headset_sm.h"
#include "media_player.h"
#include "voice_ui.h"

#include <focus_select.h>
#include <telephony_service.h>
#include <handset_service.h>

#ifdef INCLUDE_ACCESSORY_TRACKING
#include "accessory_tracking.h"
#endif

const focus_select_audio_tie_break_t audio_source_focus_tie_break_order[] =
{
    FOCUS_SELECT_AUDIO_LINE_IN,
    FOCUS_SELECT_AUDIO_USB,
    FOCUS_SELECT_AUDIO_A2DP
};

const focus_select_voice_tie_break_t voice_source_focus_tie_break_order[] =
{
    FOCUS_SELECT_VOICE_HFP,
    FOCUS_SELECT_VOICE_USB
};

/*! \brief ui config table*/
const ui_config_table_content_t app_ui_config_table[] =
{    
    {LI_MFB_BUTTON_HELD_2SEC,          ui_provider_app_sm,          context_app_sm_inactive,              ui_input_sm_power_on                  },
    {LI_MFB_BUTTON_HELD_3SEC,          ui_provider_app_sm,          context_app_sm_active,                ui_input_sm_power_off                 },
    {LI_MFB_BUTTON_HELD_6SEC,          ui_provider_app_sm,          context_app_sm_active,                ui_input_sm_pair_handset              },
    {APP_BUTTON_ANC,                   ui_provider_app_sm,          context_app_sm_active,                ui_input_my_anc_evt                   },
    {APP_BUTTON_ANC_TUNNING,           ui_provider_app_sm,          context_app_sm_active,                ui_input_my_anc_enter_tunning         },
    {APP_BUTTON_ANC_MULTI,             ui_provider_app_sm,          context_app_sm_active,                ui_input_dts_mode_dut                 },//    配对状态下进DUT，否则切换光感
    {RESET_PAIRED_DEVICE_LIST,         ui_provider_app_sm,          context_app_sm_active,                ui_input_factory_reset_request        },

    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_telephony,       context_voice_in_call,                ui_input_voice_call_hang_up           },//    单击挂断
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_telephony,       context_voice_ringing_outgoing,       ui_input_voice_call_hang_up           },//    单击取消
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_telephony,       context_voice_ringing_incoming,       ui_input_voice_call_accept            },//    单击接听
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_telephony,       context_voice_in_call_with_incoming,  ui_input_voice_call_cycle             },//    单击接听 + 保持
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_telephony,       context_voice_in_call_with_held,      ui_input_voice_call_hang_up           },//    单击挂断 + 返回保持
    {LI_MFB_BUTTON_DOUBLE_PRESS,       ui_provider_telephony,       context_voice_in_call_with_held,      ui_input_voice_call_cycle              },//   双击循环切换
    {LI_MFB_BUTTON_DOUBLE_PRESS,       ui_provider_telephony,       context_voice_in_call,                ui_input_voice_call_cycle             },//    双击循环切换

    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_telephony,       context_voice_in_call_with_outgoing,  ui_input_voice_call_hang_up           },
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_telephony,       context_voice_in_multiparty_call,     ui_input_voice_call_hang_up           },
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_media_player,    context_media_player_streaming,       ui_input_toggle_play_pause            },
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_media_player,    context_media_player_idle,            ui_input_toggle_play_pause            },
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_handset,         context_handset_not_connected,        ui_input_connect_handset              },
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_handset,         context_handset_connected,            ui_input_connect_handset              },
    {LI_MFB_BUTTON_DOUBLE_PRESS,       ui_provider_app_sm,          context_app_sm_active,                ui_input_my_enter_pairing             }, //空闲连接进入可配对，连接状态末号重播

    {LI_MFB_BUTTON_HELD_2SEC,          ui_provider_telephony,       context_voice_in_call,                ui_input_voice_transfer               }, //    长按转移
    {LI_MFB_BUTTON_HELD_2SEC,          ui_provider_telephony,       context_voice_in_call_with_outgoing,  ui_input_voice_transfer               },
    {LI_MFB_BUTTON_HELD_2SEC,          ui_provider_telephony,       context_voice_in_call_with_held,      ui_input_voice_transfer               },
    {LI_MFB_BUTTON_HELD_2SEC,          ui_provider_telephony,       context_voice_call_held,              ui_input_voice_transfer               },
    {LI_MFB_BUTTON_HELD_2SEC,          ui_provider_telephony,       context_voice_in_multiparty_call,     ui_input_voice_transfer               },

    {LI_MFB_BUTTON_RELEASE_1SEC,       ui_provider_telephony,       context_voice_ringing_incoming,       ui_input_voice_call_reject            }, //    长按拒听
    {LI_MFB_BUTTON_RELEASE_1SEC,       ui_provider_telephony,       context_voice_in_call_with_incoming,  ui_input_voice_call_reject            },

    {LI_MFB_BUTTON_RELEASE_1SEC,       ui_provider_app_sm,          context_app_sm_active,                ui_input_voice_dial                   },
    {APP_BUTTON_ANC_DOUBLE,            ui_provider_handset,         context_handset_connected,            ui_input_gaming_mode_toggle           },
    {LI_MFB_BUTTON_HELD_FACTORY_RESET_DS, ui_provider_app_sm,       context_app_sm_active,                ui_input_factory_reset_request        },
#if defined(INCLUDE_ACCESSORY_TRACKING)
    {LI_MFB_MFB_BUTTON_4_CLICKS,       ui_provider_accessory_tracking,   context_at_active,               ui_input_at_serial_number_lookup      },
#endif

#ifdef INCLUDE_VOICE_UI
    {APP_VA_BUTTON_DOWN,               ui_provider_voice_ui,        context_voice_ui_default,             ui_input_va_1                         },
    {APP_VA_BUTTON_HELD_RELEASE,       ui_provider_voice_ui,        context_voice_ui_default,             ui_input_va_2                         },
    {APP_VA_BUTTON_SINGLE_CLICK,       ui_provider_voice_ui,        context_voice_ui_default,             ui_input_va_3                         },
    {APP_VA_BUTTON_DOUBLE_CLICK,       ui_provider_voice_ui,        context_voice_ui_default,             ui_input_va_4                         },
    {APP_VA_BUTTON_HELD_1SEC,          ui_provider_voice_ui,        context_voice_ui_default,             ui_input_va_5                         },
    {APP_VA_BUTTON_RELEASE,            ui_provider_voice_ui,        context_voice_ui_default,             ui_input_va_6                         },
#endif

#if defined(HAVE_4_BUTTONS) || defined(HAVE_6_BUTTONS) || defined(HAVE_7_BUTTONS) || defined(HAVE_9_BUTTONS)
    {APP_BUTTON_VOLUME_DOWN,           ui_provider_app_sm,          context_app_sm_active,                ui_input_volume_down                  },
    {APP_BUTTON_VOLUME_UP,             ui_provider_app_sm,          context_app_sm_active,                ui_input_volume_up                    },
    {APP_BUTTON_FORWARD,               ui_provider_media_player,    context_media_player_streaming,       ui_input_av_forward                   },
    {APP_BUTTON_FORWARD,               ui_provider_media_player,    context_media_player_idle,            ui_input_av_forward                   },
    {APP_BUTTON_BACKWARD,              ui_provider_media_player,    context_media_player_streaming,       ui_input_av_backward                  },
    {APP_BUTTON_BACKWARD,              ui_provider_media_player,    context_media_player_idle,            ui_input_av_backward                  },
    {LI_MFB_BUTTON_SINGLE_PRESS,       ui_provider_media_player,    context_media_player_no_media,        ui_input_usb_toggle_play_pause        },
    {APP_BUTTON_FORWARD,               ui_provider_media_player,    context_media_player_no_media,        ui_input_usb_forward                  },
    {APP_BUTTON_BACKWARD,              ui_provider_media_player,    context_media_player_no_media,        ui_input_usb_backward                 },

#ifdef ENABLE_TWM_SPEAKER
    {APP_BUTTON_HELD_4SEC,             ui_provider_app_sm,          context_app_sm_active,                ui_input_app_peer_pair                },
    {APP_BUTTON_HELD_6SEC,             ui_provider_app_sm,          context_app_sm_active,                ui_input_app_toggle_twm_standalone    },
    {APP_BUTTON_HELD_8SEC,             ui_provider_app_sm,          context_app_sm_active,                ui_input_app_toggle_party_mode        },
#endif /* ENABLE_TWM_SPEAKER */
    /* events if speaker supports broadcast source configuration */
#ifdef INCLUDE_LE_AUDIO_BROADCAST_SOURCE
    {APP_BCAST_SENDER_TOGGLE_START_STOP, ui_provider_app_sm,         context_app_sm_active,                ui_input_app_toggle_broadcast_media_sender  },
#endif

#endif /* HAVE_4_BUTTONS || HAVE_6_BUTTONS || HAVE_7_BUTTONS || HAVE_9_BUTTONS */

#if defined(HAVE_6_BUTTONS) || defined(HAVE_7_BUTTONS) || defined(HAVE_9_BUTTONS)
    // {APP_BUTTON_FORWARD,              ui_provider_media_player,     context_media_player_streaming,       ui_input_av_forward                   },
    // {APP_BUTTON_FORWARD_HELD,         ui_provider_media_player,     context_media_player_streaming,       ui_input_av_fast_forward_start        },
    // {APP_BUTTON_FORWARD_HELD_RELEASE, ui_provider_media_player,     context_media_player_streaming,       ui_input_fast_forward_stop            },
    // {APP_BUTTON_BACKWARD,             ui_provider_media_player,     context_media_player_streaming,       ui_input_av_backward                  },
    // {APP_BUTTON_BACKWARD_HELD,        ui_provider_media_player,     context_media_player_streaming,       ui_input_av_rewind_start              },
    // {APP_BUTTON_BACKWARD_HELD_RELEASE,ui_provider_media_player,     context_media_player_streaming,       ui_input_rewind_stop                  },
#endif /* HAVE_6_BUTTONS || HAVE_7_BUTTONS || HAVE_9_BUTTONS */

#if defined(HAVE_7_BUTTONS) || defined(HAVE_9_BUTTONS)
    {APP_BUTTON_PLAY_PAUSE_TOGGLE,    ui_provider_media_player,     context_media_player_streaming,       ui_input_toggle_play_pause            },
    {APP_BUTTON_PLAY_PAUSE_TOGGLE,    ui_provider_media_player,     context_media_player_idle,            ui_input_toggle_play_pause            },
#endif /* HAVE_7_BUTTONS || HAVE_9_BUTTONS */
};


const ui_config_table_content_t* AppUi_GetConfigTable(unsigned* table_length)
{
    *table_length = ARRAY_DIM(app_ui_config_table);
    return app_ui_config_table;
}

void AppUi_ConfigureFocusSelection(void)
{
    FocusSelect_ConfigureAudioSourceTieBreakOrder(audio_source_focus_tie_break_order);
    FocusSelect_ConfigureVoiceSourceTieBreakOrder(voice_source_focus_tie_break_order);
}

bool AppUi_IsLogicalInputScreenedInLimboState(unsigned logical_input)
{
    switch (logical_input)
    {
    case LI_MFB_BUTTON_HELD_2SEC:
        /* Power On button press is not screened. */
        return FALSE;
    default:
        return TRUE;
    }
}
